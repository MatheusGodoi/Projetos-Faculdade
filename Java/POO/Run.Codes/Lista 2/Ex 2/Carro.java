class Carro extends Veiculo {

	public Carro(float velocidadeAtual, double aceleracaoBase, float distanciaPercorrida){
		super(velocidadeAtual, (3*aceleracaoBase), distanciaPercorrida);
	}
}