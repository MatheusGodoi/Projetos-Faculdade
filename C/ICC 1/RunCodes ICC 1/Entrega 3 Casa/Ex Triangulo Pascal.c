#include <stdio.h>
#include <stdlib.h>
#define max 500

void pascal(int linha, int*numeros, double*soma, double*produto);

int main(){
    int linha, num[max];
    double soma, produto=1;

    scanf("%d", &linha);

    pascal(linha, num, &soma, &produto);

    printf("\n%.0f %.0f", soma, produto);

return 0;
}

void pascal(int linha, int*numeros, double*soma, double*produto){
   int i,j,k=0, acima, lado;

   numeros[0]= 1;
   numeros[1]= 1;
   numeros[2]= 1;

   if (linha>2){
       *soma = 3;
        for (i=3; i<=linha;i++){
            for (j=0; j<i; j++){
                if (j==0 || j==i-1){
                    numeros[k+i]= 1;
                }else{
                    acima= numeros[k-1];
                    lado = numeros[k];
                    numeros[k+i] = acima + lado;
                }

                if (i==linha){
                    printf("%d ", numeros[k+i]);
                }
                *soma += numeros[k+i];
                *produto *= numeros[k+i];
                k++;
            }
        }
   }else{
       *soma = 0;
       for (i=0; i<=linha; i++){
           *soma += numeros[i];
           *produto *= numeros[i];
       }
       for (i=0; i<linha; i++){
            printf("%d ", 1);
       }
   }
}


