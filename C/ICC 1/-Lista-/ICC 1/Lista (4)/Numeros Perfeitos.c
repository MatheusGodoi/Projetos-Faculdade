#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main () {

int N, divisor, resto, soma;

printf ("Insira N: ");
scanf("%d",&N);

while (N<=0) {
printf ("\nErro! Numero Positivo!\n");
printf ("\nInsira N: ");
scanf("%d",&N);
}

printf ("1 ");

divisor = 2;
soma = 1;
for(;divisor!=N;divisor++){
    resto=N%divisor;

    if (resto==0){
        soma = soma+divisor;
        printf ("+ %d ",divisor);
    }
}

printf ("= %d",soma);

if (soma == N){
    printf ("\nSeu numero e perfeito!\n\n");
}else
printf ("\nSeu numero nao e perfeito!\n\n");

system ("pause");
return 0;
}
