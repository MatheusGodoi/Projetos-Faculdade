#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 15

void calculaterra(int sini,int *d,int *h,int *m,int *s);
void calculavenus(int sini,int *d,int *h,int *m,int *s);
void calculamerc(int sini,int *d,int *h,int *m,int *s);
void calculajupi(int sini,int *d,int *h,int *m,int *s);

int main () {
char planini[max], plan;
int segini, sini;
int dia=0, hora=0, min=0, seg=0;
int *d, *h, *m, *s;

scanf("%d",&segini);
scanf("%s",planini);

plan = planini[0];
sini = segini;
d = &dia;
h = &hora;
m = &min;
s = &seg;

switch(plan){
    case 'T': calculaterra(sini, d, h, m ,s);break;
    case 'V': calculavenus(sini, d, h, m, s);break;
    case 'M': calculamerc(sini, d, h, m, s);break;
    case 'J': calculajupi(sini, d, h, m, s);break;
}

printf("%d segundos no planeta %s equivalem a:\n",segini,planini);
printf("%d dias, %d horas, %d minutos e %d segundos\n",dia,hora,min,seg);
return 0;
}

/*
O dia em J�piter equivale a 9 horas e 56 minutos terrestres (Cuidado, esse vai ser um pouco mais dif�cil calcular!).
*/

void calculaterra(int sini,int *d,int *h,int *m,int *s){
    int dia=0;
    *s = sini%60;
    sini /= 60;
    *m = sini%60;
    sini /= 60;
    *h = sini;

    while(*h > 24){
        *h = *h - 24;
        dia++;
    }
    *d = dia;
}

void calculavenus(int sini,int *d,int *h,int *m,int *s){
    int dia=0;
    *s = sini%60;
    sini /= 60;
    *m = sini%60;
    sini /= 60;
    *h = sini;

    while(*h > 5832){
        *h = *h  - 5832;
        dia++;
    }
    *d = dia;
}

void calculamerc(int sini,int *d,int *h,int *m,int *s){
      int dia=0;
    *s = sini%60;
    sini /= 60;
    *m = sini%60;
    sini /= 60;
    *h = sini;

    while(*h > 1408){
        *h = *h  - 1408;
        dia++;
    }
    *d = dia;
}

void calculajupi(int sini,int *d,int *h,int *m,int *s){

    *d=sini/(9*3600+56*60);
    *h=(sini%(9*3600+56*60))/3600;
    *m=((sini%(9*3600+56*60))%3600)/60;
    *s=((sini%(9*3600+56*60))%3600)%60;

}
