#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct para montar uma lista com todos casos de teste
typedef struct{
    int personAmount;
    int countAmount;
}caseStruct;

// Struct para ser a pessoa da Lista circular
// Contém um link para a próxima pessoa e a posição
typedef struct temp{
    struct temp *prox;
    int posicao;
}pessoaStruct;

// Função de inserção no final da lista circular
void insereFimListaCircular(pessoaStruct **lista, int pos){
    pessoaStruct *tempNo;

    tempNo = (pessoaStruct *) malloc(sizeof(pessoaStruct));
    tempNo->posicao = pos;

    if(*lista == NULL){
        tempNo->prox = tempNo;
    }else{
        tempNo->prox = (*lista)->prox;
        (*lista)->prox = tempNo;
    }

    *lista = tempNo;
}

// Função para imprimir os itens da lista
void imprimeLista(pessoaStruct *lista){
    pessoaStruct *tempNo;

    if (lista != NULL){
        tempNo = lista;
        do{
            printf("%d\n", tempNo->posicao);
            tempNo = tempNo->prox;
        }while(tempNo != lista);
    }
}

// Função para limpar todos nós da lista
void cleanLista(pessoaStruct *lista){
    pessoaStruct *tempHead;
    pessoaStruct *tempNo;
    pessoaStruct *tempAux;

    tempHead = lista;
    tempNo = lista->prox;
    
    while(tempNo != tempHead){
        tempAux = tempNo;
        tempNo = tempNo->prox;
        tempAux->prox = NULL;
        free(tempAux);
    }

    tempNo = NULL;
    lista = NULL;

}

// Função responsável pelo "jogo". Inicia pegando o próximo elemento da lista (o primeiro) e começa
// a contagem até K para eliminar o nó em questão.
int jogoDoJosefo(pessoaStruct *lista, int n, int k){
    pessoaStruct *inicio;
    pessoaStruct *tempNo;
    pessoaStruct *tempAtras;

    // Posição inicial dos ponteiros
    inicio = lista->prox;   // Primeiro elemento da lista cirular
    tempNo = inicio;        // tempNo sendo o primeiro elemento
    tempAtras = lista;      // tempAtras começa sendo o ultimo elemento
    
    while(n > 1){
        // Contando a posição iniical ( i = 1 ), anda na lista K vezes
        for(int i = 1; i < k; i++){
            tempAtras = tempNo;
            tempNo = tempNo->prox;
        }

        // Retira o ponteiro do tempNo e aponta para o próximo
        tempAtras->prox = tempNo->prox;

        // Reseta a posição inicial da contagem
        inicio = tempNo->prox;

        // Liberação da lista
        free(tempNo);

        // Reseta a posição inicial da contagem
        tempNo = inicio;

        // Diminui uma pessoa viva
        n -= 1;
    }

    // Retorna o sobrevivente
    return inicio->posicao;
}


// Variavel "testes" ira armazenar o número T de testes.
// Ponteiro "cases" irá ser alocado para conter todos os casos
int main(){
    int numTestes;
    caseStruct *cases;
    pessoaStruct *lista = NULL;
    int winner;

    printf("Quantos casos de teste?\n");
    scanf("%d", &numTestes);

    // Alocação do vetor
    cases = (caseStruct *)malloc(numTestes * sizeof(caseStruct));

    // Leitura de todos casos de teste e alocação no vetor
    for(int i = 0; i < numTestes; i++) {
        scanf("%d %d", &cases[i].personAmount, &cases[i].countAmount);
    }


    //Inicio do jogo
    for(int i = 0; i < numTestes; i++){
        // Criação da lista circular
        for(int j = 1; j <= cases[i].personAmount; j++) {
            insereFimListaCircular(&lista, j);
        }

        // Função responsável pelo jogo
        winner = jogoDoJosefo(lista, cases[i].personAmount, cases[i].countAmount);
        printf("Caso %d: %d\n",i+1, winner);

        // Limpeza da lista para criar a próxima
        cleanLista(lista);
    }

    return 0;
}