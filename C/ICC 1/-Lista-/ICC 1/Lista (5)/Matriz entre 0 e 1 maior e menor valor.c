#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
float matriz[max][max], maior, menor; //contadores i,j   linha (m) x colunas (n)
int i, j, m, n;
int imaior=0, jmaior=0, imenor=0, jmenor=0;

printf("Insira Linhas x Colunas: ");
scanf("%d %d",&m,&n);

//valores aleatorios na matriz
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matriz[i][j]=(float)(rand())/(float)(RAND_MAX);
    }
}//fim da atribuišao

maior = matriz[0][0];
for(i=0;i<m;i++){ //achar maior valor
    for(j=0;j<n;j++){
        if(maior<matriz[i][j]){
            maior = matriz [i][j];
            imaior = i; jmaior = j;
        }
    }
}

menor = matriz[0][0];
for(i=0;i<m;i++){ // achar menor valor
    for(j=0;j<n;j++){
        if(menor>matriz[i][j]){
            menor = matriz [i][j];
            imenor = i; jmenor = j;
        }
    }
}

printf("\nMatriz:\n");
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %.2f ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}

printf("\n");
printf("Maior valor:%.2f Coordenadas: [%d][%d]\n",maior,imaior+1,jmaior+1);
printf("Menor valor:%.2f Coordenadas: [%d][%d]\n",menor,imenor+1,jmenor+1);
printf("\n");
system ("pause");
return 0;
}
