#include <stdio.h>
#include <stdlib.h>
#define max 100001

typedef int elem;

typedef struct temp{
    elem info;
    struct temp *esq, *dir;
}no;

typedef struct{
    no *raiz;
}BST;

void inicializa(BST *Arvore){
     Arvore->raiz=NULL;
}

void insereArvore(no **p, elem *x){

    if(*p==NULL){
        *p=(no*) malloc(sizeof(no));

        (*p)->info=*x;
        (*p)->esq=NULL;
        (*p)->dir=NULL;
        return;
     }else if(*x<(*p)->info){
          return(insereArvore(&(*p)->esq,x));
     }else if(*x>=(*p)->info){
          return(insereArvore(&(*p)->dir,x));
     }
}

elem buscaMaior(no **p) {
     no *aux;
     aux = *p;
     while(aux->dir!=NULL){
           aux = aux->dir;
     }
     return(aux->info);
}

void removeArvore(no **p, elem *x) {
    no *aux;
    if(*p==NULL){
       return;
    }else if(*x<(*p)->info){
         return(removeArvore(&(*p)->esq,x));
    }
    else if(*x>(*p)->info){
         return(removeArvore(&(*p)->dir,x));
    }else{
            //CASOS

         //N� folha
         if(((*p)->esq==NULL)&&((*p)->dir==NULL)){
            free(*p);
            *p=NULL;
            return;
         }
         //Filho Direito
         else if((*p)->esq==NULL){
              aux=*p;
              *p=(*p)->dir;
              free(aux);
              return;
         }
         //Filho Esquerdo
         else if((*p)->dir==NULL){
              aux=*p;
              *p=(*p)->esq;
              free(aux);
              return;
         }
         //Dois filhos
         else{
              (*p)->info = (*p)->dir->info;
              return(removeArvore(&(*p)->dir,&(*p)->info));
         }
    }
}

void printaArvore(no **p){
     if (*p!=NULL) {
        printf("%d ",(*p)->info);
        printaArvore(&(*p)->esq);
        printaArvore(&(*p)->dir);
     }
}

void finalizaArvore(no **p){
     if(*p!=NULL){
        finalizaArvore(&(*p)->esq);
        finalizaArvore(&(*p)->dir);
        free(*p);
     }
}


int main(){
    int tamInsere, i, auxInsere[max];
    int tamRemove, auxRemove[max];
    BST Arvore;

    inicializa(&Arvore);

    scanf("%d",&tamInsere);

    for(i=0;i<tamInsere;i++){
        scanf("%d", &auxInsere[i]);
    }
    for(i=0;i<tamInsere;i++){
        insereArvore(&Arvore.raiz,&auxInsere[i]);
    }

    scanf("%d",&tamRemove);
    for(i=0;i<tamRemove;i++){
        scanf("%d", &auxRemove[i]);
    }
    for(i=0;i<tamRemove;i++){
        removeArvore(&Arvore.raiz,&auxRemove[i]);
    }

    printaArvore(&Arvore.raiz);
    printf("\n");

    finalizaArvore(&Arvore.raiz);

    return 0;
}
