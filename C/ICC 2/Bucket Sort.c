#include <stdio.h>
#include <stdlib.h>

typedef struct temp{
    float info;
    struct temp *prox;
}no;

typedef struct{
    no *inicio;
} Lista;

void inicializa(Lista *L) {
    L->inicio=NULL;
}

void finaliza(Lista *L) {
    no *p;
    p=L->inicio;
    while (p!=NULL) {
          L->inicio=L->inicio->prox;
          free(p);
          p=L->inicio;
    }
}

void inserirElem(Lista *L, float *X){
    no *lAtual, *lAnt, *eAux;

    eAux =(no*) malloc(sizeof(no));
    if(eAux==NULL){
        printf("ERRO MALLOC");
        return;
    }

    eAux->info=*X;

    if((L->inicio==NULL)||(*X<L->inicio->info)){
        eAux->prox=L->inicio;
        L->inicio=eAux;
    }else{
        lAnt=NULL;
        lAtual=L->inicio;

    while((lAtual!=NULL)&&(lAtual->info<=*X)){
        lAnt=lAtual;
        lAtual=lAtual->prox;
    }

    eAux->prox=lAtual;
    lAnt->prox=eAux;
    }

    return;
}

void removerElem(Lista *L, float *X){
    no *eAux;

    eAux = L->inicio;
    L->inicio = L->inicio->prox;
    *X = eAux->info;
    free(eAux);

    return;
}

void imprimir(Lista *L){
    no *eAux;

    eAux=L->inicio;
    while(eAux!=NULL){
          printf("%.4f ",eAux->info);
          eAux = eAux->prox;
    }

    return;
}

int estaVazia(Lista *L){
    if(L->inicio == NULL){
        return 1;
    }
    return 0;
}

void Bucket(float *pAux, int tam){
    int i, j, indice;
    float aux;
    Lista *L;

    L = (Lista *) malloc((tam+1)*sizeof(Lista));
    if(L == NULL){
        printf("ERRO MALLOC LISTA");
        return;
    }

    for(i=0;i<tam+1;i++){
        inicializa(&L[i]);
    }

    for(i=0;i<tam;i++){
        aux = pAux[i];
        indice = pAux[i] *tam;
        inserirElem(&L[indice], &aux);
    }

    j=0;
    for(i=0;i<tam+1;i++){
        while(!estaVazia(&L[i])){
            removerElem(&L[i],&aux);
            pAux[j] = aux;
            j++;
        }
    }
}

int main(){
    int tam, i;
    float *pAux;

    scanf("%d", &tam);

    pAux = (float *)malloc(tam*sizeof(float));

    for(i=0;i<tam;i++){
        scanf(" %f", &pAux[i]);
    }

    Bucket(pAux, tam);

    for(i=0;i<tam;i++){
        printf("%.4f ", pAux[i]);
    }

    return 0;
}
