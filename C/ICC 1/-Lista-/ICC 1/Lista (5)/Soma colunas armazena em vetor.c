#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
int matriz[max][max], i, j, m,n; //contadores i,j   linha (m) x colunas (n)
int vetor[max], soma=0;

printf("Insira Linhas x Colunas: ");
scanf("%d %d",&m,&n);

//valores aleatorios na matriz
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matriz[i][j]=rand()%101;
    }
}

for(j=0;j<n;j++){
    for(i=0;i<m;i++){
        soma=soma+matriz[i][j];
    }
    vetor[j]=soma;
    soma=0;
}

printf("\n");
printf("\nMatriz Final:\n");
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\nVetor Final:\n");
    for(j=0;j<n;j++){
        printf("[%d]=%d ",j,vetor[j]);
    }

printf("\n");
system ("pause");
return 0;
}
