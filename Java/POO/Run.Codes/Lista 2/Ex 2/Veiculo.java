class Veiculo {
	private float velocidadeAtual;
	private double aceleracaoBase;
	private float distanciaPercorrida;	

	public Veiculo(float velocidadeAtual, double aceleracaoBase, float distanciaPercorrida){
		this.velocidadeAtual = velocidadeAtual;
		this.aceleracaoBase = aceleracaoBase;
		this.distanciaPercorrida = distanciaPercorrida;
	}

	public float getVelocidadeAtual(){
		return this.velocidadeAtual;
	}

	public void setVelocidadeAtual(float velocidade){
		this.velocidadeAtual = velocidade;
	}

	public double getAceleracaoBase(){
		return this.aceleracaoBase;
	}

    public void setAceleracaoBase(double aceleracao){
        this.aceleracaoBase = aceleracao;
    }

	public float getDistanciaPercorrida(){
		return this.distanciaPercorrida;
	}

	public void setDistanciaPercorrida(float dist){
		this.distanciaPercorrida = dist;
	}

	public void calculoDistancia(String acao, int tempo){
		if("C".equals(acao)){
			this.setDistanciaPercorrida(this.getDistanciaPercorrida() + (this.getVelocidadeAtual()*tempo));
		}else if("A".equals(acao)){
			this.setDistanciaPercorrida((float) (this.getDistanciaPercorrida()+ (this.getVelocidadeAtual()*tempo)  +((this.getAceleracaoBase()*tempo*tempo)/2)));
			this.setVelocidadeAtual((float) ((float) (this.getVelocidadeAtual()) + (this.getAceleracaoBase()*tempo)));
		}else if("F".equals(acao)){
			this.setDistanciaPercorrida((float) (this.getDistanciaPercorrida() + (this.getVelocidadeAtual()*tempo) - ((this.getAceleracaoBase()*tempo*tempo)/2)));
			this.setVelocidadeAtual((float) ((float) this.getVelocidadeAtual() - (this.getAceleracaoBase()*tempo)));
		}else{
			System.out.println("ERRO ACAO INVALIDA");
		}
	}

	public String printDistanciaPercorrida(){
			return Float.toString(this.getDistanciaPercorrida());
	}
}

