#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int pontuacao(int jogador,char dardo[3]);

int main(){
    int cont=0,joga1=301,joga2=301, resultado[2];
    char dardo[3];

    do{
        cont ++;
        scanf("%s",dardo);
        if (((cont-1)/3)%2){
            joga2 = pontuacao(joga2,dardo);
        }else{
            joga1 = pontuacao(joga1,dardo);
        }
    }while(joga1>0 && joga2>0);

    resultado[0] = ((cont-1)/3)%2+1;

    if (joga1 == 0){
        resultado [1] = joga2;
    }else if (joga2 == 0){
        resultado [1] = joga1;
    }

    printf("%d %d", resultado[0], resultado[1]);
    return 0;
}

int pontuacao(int jogador,char dardo[3]){
    int resul, multi=0, valor=0;
    char letra;

    letra = dardo[0];
    switch(letra){
        case 'S': multi=1; break;
        case 'D': multi=2; break;
        case 'T': multi=3; break;
        case 'M': multi=25; break;
        case 'X': multi=0; break;
    }

    valor = (int)(dardo[1]-'0')*10+(int)(dardo[2]-'0');
    resul = multi*valor;

   if(jogador >= resul){
        return jogador-resul;
    }else{
        return jogador;
    }
}
