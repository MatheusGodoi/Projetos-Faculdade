#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {
float lab,prova,exame, nota1,nota2,nota3, soma, media;

inicio :

printf("Insira nota de laboratorio:");
scanf ("%f",&lab);
printf("\nInsira nota da avaliacao semanal:");
scanf ("%f",&prova);
printf("\nInsira nota do exame final:");
scanf ("%f",&exame);

nota1 = lab*2;
nota2 = prova*3;
nota3 = exame*5;
soma = nota1+nota2+nota3;
media = soma/10;


if ((media>8)&&(media<=10)){
    printf ("Seu conceito: A (Nota: %.2f)\n",media);
    getchar();
    system ("pause");
    return 0;
}
else if ((media>7)&&(media<=8)){
    printf ("Seu conceito: B (Nota: %.2f)\n",media);
    getchar();
    system ("pause");
    return 0;

}
else if ((media>6)&&(media<=7)){
    printf ("Seu conceito: C (Nota: %.2f)\n",media);
    getchar();
    system ("pause");
    return 0;

}
else if ((media>=5)&&(media<=6)){
    printf ("Seu conceito: D (Nota: %.2f)\n",media);
    getchar();
    system ("pause");
    return 0;

}
else if (media<5){
    printf ("REPROVADO (Conceito: E, Nota: %.2f)\n",media);
    getchar();
    system ("pause");
    return 0;
}
else {
    printf ("\nInsira valores validos!\n");
    goto inicio;
}
}
