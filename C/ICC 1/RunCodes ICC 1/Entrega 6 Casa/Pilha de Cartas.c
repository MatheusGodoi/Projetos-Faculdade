#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    int topo;
    int capacidade;
    char* elem;
}pStruct;

void criaPilha(pStruct* pilha,int capac);
void push(pStruct* pilha,char valor);
void pop(pStruct* pilha);
void mostraMao(pStruct* pilha,int coringa,int tam);

int main() {
    int qtd, i = 0, tam = 0;
    char carta;
    int contaCoringa = 0, contaCarta = 0;
    pStruct pilha;

    scanf(" %d",&qtd);

    criaPilha(&pilha,qtd);

    for(i=0;i<qtd;i++){
        scanf(" %c",&carta);
        if((carta == 'C')&&(contaCarta < 5)){
            contaCoringa++;
        }else if(carta == 'C'){
            tam --;
        }else{
            contaCarta++;
            push(&pilha,carta);
        }
    }

    tam += (qtd - contaCoringa);

    mostraMao(&pilha,contaCoringa,tam);

    return 0;
}

void criaPilha(pStruct* pilha,int capac){
    pilha->topo = -1;
    pilha->capacidade = capac;
    pilha->elem = (char*) malloc(capac);

}

void push(pStruct* pilha,char valor){
        pilha->topo++;
        pilha->elem[pilha->topo] = valor;
}

void pop(pStruct* pilha){
    pilha->topo--;
}

void mostraMao(pStruct* pilha,int coringa,int tam){
    int i = 0, soma = 0;

    printf("Coringas: %d\n",coringa);
    printf("Cartas na mao:\n");

    for(i=tam-1;i>=tam-5;i--){
        printf("%c ", pilha->elem[i]);
        switch(pilha->elem[i]){
            case 65: soma+= 1; break;
            case 50: soma+= 2; break;
            case 51: soma+= 3; break;
            case 52: soma+= 4; break;
            case 53: soma+= 5; break;
            case 54: soma+= 6; break;
            case 55: soma+= 7; break;
            case 74: soma+= 8; break;
            case 81: soma+= 9; break;
            case 75: soma+= 10; break;
        }
    }

    printf("\n");
    printf("Soma: %d",soma);
}

