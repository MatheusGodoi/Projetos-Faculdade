#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct{
    int idade;
    char*nome;
}registro;

struct Node{
    registro dados;
    struct Node *prox;
};

typedef struct{
    struct Node *prim;
}encLista;

char* leNome();
void criarLista(encLista *lista);
int estado(encLista* lista);
void inserirInicio(encLista* lista, int idade, char* nome);
void removerInicio(encLista* lista);
void sortLista (encLista* lista);
void inserirOrdem(encLista* lista,int idade, char* nome);
void imprimirLista(encLista* lista);

int main(){

    char*nome;
    int idade, qnt, i;
    encLista lista;

    scanf("%d",&qnt);

    criarLista(&lista);

    for (i=0; i<qnt; i++){
        scanf("%d ", &idade);
        nome = leNome();
        inserirInicio(&lista, idade, nome);
    }

    sortLista(&lista);


    if (!estado(&lista)){
        imprimirLista(&lista);
    }

    return 0;
}

char* leNome(){
    char*nome= NULL;
    int i=0;

    do{
        nome = (char*)realloc(nome,(i+1)*sizeof(char));
        scanf("%c", &nome[i]);
        i++;
    }while(nome[i-1]!='\n');

    nome[i-1]='\0';

    return nome;
}

void criarLista(encLista *lista){
    lista->prim = NULL;
}

int estado(encLista* lista){
    return (lista->prim==NULL);
}

void inserirInicio(encLista* lista, int idade, char*nome){
    struct Node* novo;

    novo = (struct Node*)malloc(sizeof(struct Node));

    novo->dados.idade = idade;

    novo->dados.nome = (char*)realloc(novo->dados.nome, strlen(nome)*sizeof(char));
    assert(novo->dados.nome!=NULL);

    novo->dados.nome = nome;

    novo->prox = lista->prim;
    lista->prim = novo;
}

void removerInicio(encLista*lista){
    struct Node* aux = lista->prim;
    lista->prim = lista->prim->prox;
    free(aux);
}

void sortLista (encLista* lista){
    char temp[50];
    int temp2;
    struct Node* aux = lista->prim;
    struct Node* temp3;

    while(aux!=NULL){
        temp3 = aux->prox;
        while(temp3!=NULL){
            if(aux->dados.idade > temp3->dados.idade || (aux->dados.idade == temp3->dados.idade &&  strcmp(aux->dados.nome,temp3->dados.nome)<0)){
                strcpy(temp,aux->dados.nome);
                strcpy(aux->dados.nome,temp3->dados.nome);
                strcpy(temp3->dados.nome,temp);

                temp2 = aux->dados.idade;
                aux->dados.idade = temp3->dados.idade;
                temp3->dados.idade = temp2;

            }
            temp3 = temp3->prox;
        }
        aux = aux->prox;
    }
}

void inserirOrdem (encLista*lista,int idade, char* nome){
    struct Node* novo;

    novo = (struct Node*)malloc(sizeof(struct Node));
    assert(novo!=NULL);

    novo->dados.idade = idade;
    novo->dados.nome = (char*)realloc(novo->dados.nome, strlen(nome)*sizeof(char));
    novo->dados.nome = nome;

    struct Node *pAnter, *pAtual;
    pAnter = NULL;
    pAtual = lista->prim;

    while(pAtual!=NULL && pAtual->dados.idade<idade){
        pAnter = pAtual;
        pAtual = pAtual->prox;
    }

    novo->prox = pAtual-> prox;
    pAnter ->prox = novo;
}

void imprimirLista(encLista* lista){
    struct Node* aux;

    for (aux = lista->prim; aux != NULL; aux = aux->prox){
        printf("%s\n",aux->dados.nome);
    }
}


