#include <stdio.h>
#include <stdlib.h>
#define MAX 20

typedef struct bloco{
	int info;
	struct bloco *prox;
}no;

typedef struct{
	no* top;
}pilha;


void criaPilha(pilha *P){

	P->top = NULL;

return;
}

void push(pilha *P , int* X){
	no *aux;
	
	aux = (no*) malloc(sizeof(no));
	if(aux==NULL){
		printf("Problema malloc");
		return;
	}
	
	aux->info = *X;
	aux->prox = P->top;
	P->top = aux;	
	
}

int empty(pilha *P){
	if(P->top == NULL){
		return 1;
	}
	return 0;
}

int pop(pilha *P, int *X){
	
	no* aux;
	
	if(empty(P)){
		return 0;
	}
	
	*X = P->top->info;
	aux = P->top;
	P->top = P->top->prox;
	free(aux);
	
	return 1;
}


void trocaPilha(pilha *pInvertido, pilha *pCerto){
	int aux;

	while(pop(pInvertido,&aux)){
		push(pCerto,&aux);
	}

	return;
}

int fazConta(pilha *pOperacao,pilha *pNum){
	int auxNum = 0, auxOp = 0, auxResul = 0;
	int resulFinal = 0;

	while(1){
		pop(pNum,&auxNum);
		pop(pOperacao,&auxOp);

		auxResul = 0;
		switch(auxOp){
			//43 == caracter + em ascii
			case 43: auxResul = auxNum; pop(pNum,&auxNum); auxResul = (auxNum+auxResul); break;
			//45 == caracter - em ascii
			case 45: auxResul = auxNum; pop(pNum,&auxNum); auxResul = (auxNum-auxResul); break;
			//42 == caracter * em ascii
			case 42: auxResul = auxNum; pop(pNum,&auxNum); auxResul = (auxNum*auxResul); break;
			//47 == caracter / em ascii
			case 47: auxResul = auxNum; pop(pNum,&auxNum); auxResul = (auxNum/auxResul); break;
			//Caso de erro
			default: printf("ERRO NAS OPERACOES!\n"); return -2;
		}

		resulFinal = auxResul;
		if(empty(pNum)){
			return resulFinal;
		}

		push(pNum,&auxResul);
	}

	return -1;
}

int main (){

	pilha pOperacao, pNumInvetido, pNum;
	char entrada[MAX];
	int numFinal = 0;
	int i, aux;

	criaPilha(&pOperacao);
	criaPilha(&pNumInvetido);
	criaPilha(&pNum);
	
	scanf("%[^\n]s",entrada);
	for(i = 0; i < MAX; i++){
		if(entrada[i]=='\0'){
			break;
		}
		switch(entrada[i]){
			//43 == caracter + em ascii
			case '+': aux = 43; push(&pOperacao,&aux); break;
			//45 == caracter - em ascii
			case '-': aux = 45; push(&pOperacao,&aux); break;
			//42 == caracter * em ascii
			case '*': aux = 42; push(&pOperacao,&aux); break;
			//47 == caracter / em ascii
			case '/': aux = 47; push(&pOperacao,&aux); break;
			//Ignorar espaços
			case ' ': break;
			//Inserir os números na pilha de números
			default: aux = (int)entrada[i]-48; push(&pNumInvetido,&aux); break;
		}
	}

	trocaPilha(&pNumInvetido,&pNum);
	numFinal = fazConta(&pOperacao,&pNum);

	printf("Resultado: %d\n",numFinal);

	return 0;
}
