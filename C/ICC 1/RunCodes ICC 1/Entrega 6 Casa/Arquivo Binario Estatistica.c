#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    char nome[50];
    int idade;
    char sexo;
    float salario;
}strFunc;

strFunc* leArquivo(int* cont);

int main () {
    strFunc* registro;
    int cont = 0, i=0;
    int masc = 0, fem = 0;
    float salmedmasc = 0, salmedfem = 0, salmedtotal = 0;

    registro = leArquivo(&cont);

    printf("%d ",cont); //total de registros

    for(;i<cont;i++){
        if((registro[i].sexo == 'm')||(registro[i].sexo == 'M')){
            masc++;
            salmedmasc += registro[i].salario;
        }else{
            fem++;
            salmedfem += registro[i].salario;
        }
        salmedtotal += registro[i].salario;
    }

    printf("%d %d ",masc, fem);

    salmedtotal = salmedtotal / cont;
    salmedmasc = salmedmasc / masc;
    salmedfem = salmedfem / fem;

    printf("%.2f %.2f %.2f",salmedtotal,salmedmasc,salmedfem);

    return 0;
}

strFunc* leArquivo(int* cont){
    strFunc* reg;
    int i=0;

    FILE*arq = fopen("dados-funcionario.bin","rb");
    if (arq == NULL) {
       printf ("Houve um erro ao abrir o arquivo\n");
       exit(0);
    }

    reg = (strFunc*) malloc(sizeof(strFunc));
    assert(reg!=NULL);

    while(1){
        reg[i].idade = -1;

        fread(reg[i].nome,sizeof(char),52,arq);
        fread(&reg[i].idade,sizeof(int),1,arq);
        fread(&reg[i].sexo,sizeof(char),4,arq);
        fread(&reg[i].salario,sizeof(float),1,arq);

//        printf("%s %d %c %.2f\n",reg[i].nome,reg[i].idade,reg[i].sexo,reg[i].salario);

        if(reg[i].idade == -1){
            break;
        }

        i++;
        reg = (strFunc*) realloc(reg, (i+1) * sizeof(strFunc));
        assert(reg!=NULL);
    }

    (*cont) = i;

    fclose(arq);

    return reg;
}

