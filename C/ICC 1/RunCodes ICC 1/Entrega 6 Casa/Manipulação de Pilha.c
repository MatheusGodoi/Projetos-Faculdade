#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    int topo;
    int capacidade;
    int* elem;
}pStruct;

void criaPilha(pStruct* pilha,int capac);
int checaEstado(pStruct* pilha);    /* 1 = Vazia, 2 = Cheia, 3 = Nenhum dos dois */
void push(pStruct* pilha,int valor);
void pop(pStruct* pilha);
void mostraPilha(pStruct* pilha);

int main() {
    int qtd, i=0, valor;
    char opc;
    pStruct pilha;

    scanf("%d",&qtd);

    criaPilha(&pilha,qtd);

    for(;i<qtd;i++){

        scanf(" %c %d",&opc,&valor);
        switch(opc){
            case 'i':
                if(checaEstado(&pilha)==2){
                    break;
                }
                push(&pilha,valor);
                break;
            case 'r':
                if(checaEstado(&pilha)==1){
                    break;
                }
                pop(&pilha);
                break;
            default:
                printf("\nErro\n");
                break;
        }
    }

    mostraPilha(&pilha);

    return 0;
}

void criaPilha(pStruct* pilha,int capac){
    pilha->topo = -1;
    pilha->capacidade = capac;
    pilha->elem = (int*) malloc(2 * sizeof(int));

}

int checaEstado(pStruct* pilha){

    if(pilha->topo==-1){
        return 1;
    }else if(pilha->topo==pilha->capacidade-1){
        return 2;
    }else{
        return 3;
    }
/* 1 = Vazia, 2 = Cheia, 3 = Nenhum dos dois */
}

void push(pStruct* pilha,int valor){

    if(checaEstado(pilha) != 2){
        pilha->topo++;
        pilha->elem[pilha->topo] = valor;
    }
}

void pop(pStruct* pilha){
    if(checaEstado(pilha) != 1){
        pilha->topo--;
    }
}

void mostraPilha(pStruct* pilha){
    int i = 0;

    for(i=pilha->topo;i>=0;i--){
        printf("%d ", pilha->elem[i]);
    }
}

