#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {

int primo, divisor, resto, numero;

resto = 0;
primo = 5000;

printf ("Numeros primos entre 5000 e 7000: \n\n");

while(primo<=7000){
	numero=0;
        for(divisor=1;divisor<=primo;divisor++){
            if((primo%divisor)==0) numero++;
        }
        if(numero==2&&primo<divisor){
            printf("%d | ", primo);
	}
	primo++;
}

printf("\n\n");
system("pause");
return 0;
}
