README
Matheus Vinicius G de Godoi 10295217
Hugo A Vitulli 10295221

Pré requisitos:

    Ter Python 3.x+ instalado;
    Ter o Gensim instalado;
    Ter uma base de dados;

Uso do código:
    Primeiramente, uma base de dados deve ser incluída na pasta "base".
    Para os testes, utilizamos a base de dados fornecidas no tidia, o único requisitos é que os documentos estejam no formato ".utf8" para serem lidos automaticamente.
    Devido ao peso da base fornecida, enviamos o projeto sem a mesma.

    Basta executar "main.py" no terminal, o programa irá exibir um prompt perguntando qual método a ser utilizado, sendo eles:
    1-Apenas Indexação
    2-Indexação + Remoção de Stop-words
    3-Indexação + Radicalização
    4-Indexação + Remoção de Stop-Words + Radicalização

    Após escolher uma opção, o código irá ler todos os arquivos que estejam no formato ".utf8" dentro da basta "base".
    Os documentos lidos serão escritos em um arquivo .txt dentro da pasta "info", chamado de "readDocuments.txt".

    Quando o software terminar o processamento, ele irá pedir uma query a ser buscada dentro dos documentos lidos

    No final da busca, o código irá mostrar os 30 primeiros resultados (com maior similaridade), com o número do documento (O nome do mesmo se encontra no arquivo 
    readDocuments, contendo o indíce e o caminho até o mesmo), e o tempo total de execução.

Alteração da base:

    Para trocar os arquivos a serem buscados, basta colocar arquivos novos / remover arquivos da pasta "base".
    O único requerimento é que arquivos novos estejam no formato ".utf8", e ele será lido automaticamente pelo código.