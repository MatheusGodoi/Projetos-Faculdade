#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct{
    char letra;
    int qtd;
}strLetra;


int main(){
    int posi, aux, i = 0;
    strLetra* vet;
    char letra;

    FILE* arq = fopen("texto.txt","r");
    if(arq==NULL){
        printf("Erro ao abrir o arquivo.\n");
        exit(0);
    }

    vet = (strLetra*)calloc(26,sizeof(strLetra));

    while(!feof(arq)){
        letra = fgetc(arq);
        letra = tolower(letra);

        if (letra>='a' && letra<='z'){
               aux = letra;
               posi = aux - 97;
               vet[posi].letra = letra;
               vet[posi].qtd++;
        }
    }

    for(;i<26; i++){
        if(vet[i].qtd==0){
                if(vet[i-1].qtd!=0){
                    vet[i].letra = vet[i-1].letra +1;
                }else if(vet[i+1].qtd!=0){
                    vet[i].letra = vet[i+1].letra -1;
                }
            }
        printf("%c = %d\n", vet[i].letra, vet[i].qtd);
    }

    fclose(arq);

    return 0;
}

