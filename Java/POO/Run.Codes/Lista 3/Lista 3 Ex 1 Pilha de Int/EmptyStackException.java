//package PilhaInt;

public class EmptyStackException extends StackException{
    public int modo;
    
    EmptyStackException(int modo){
        this.modo = modo;
    }
    
    @Override
    public String getMessage(){
        if(this.modo == 1){
            return "Remocao Invalida!";
        }
        return "Operacao Invalida!";
    }
}
