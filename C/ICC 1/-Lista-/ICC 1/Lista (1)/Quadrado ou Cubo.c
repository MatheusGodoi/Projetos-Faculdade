#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main ()
{
    int recebido;
    float quad, cubo;
    recebido = 0;
    quad = 0;
    cubo = 0;

    printf ("Insira numero inteiro positivo:");
    scanf ("%d",&recebido);

    quad = pow(recebido,2);
    cubo = pow (recebido,3);

    printf ("\nNumero ao quadrado: %.0f\nNumero ao cubo: %.0f\n",quad,cubo);

    system("pause");
    return 0;
}
