import java.util.Scanner;

class Date {
	int dia;
	int mes;
	int ano;

	Date(int nDia,int nMes,int nAno){
		if((nDia <= 0)||(nMes <= 0)||(nAno <=0)){
			setDia(-1);
			setMes(-1);
			setAno(-1);
			return;
		}
		if((nDia > 31)||(nMes > 12)){
			setDia(-1);
			setMes(-1);
			setAno(-1);
			return;
		}


		if((nMes == 2)&&(nDia > 28)){
			setDia(-1);
			setMes(-1);
			setAno(-1);
			return;
		}else if((nMes == 2)&&(nDia <= 28)){
			setDia(nDia);
			setMes(nMes);
			setAno(nAno);
			return;
		}

		if(nDia == 31){
			if((nMes == 4)||(nMes == 6)||(nMes == 7)||(nMes == 11)){
				setDia(-1);
				setMes(-1);
				setAno(-1);
				return;
			}else{
				setDia(nDia);
				setMes(nMes);
				setAno(nAno);
				return;
			}
		}else{
			setDia(nDia);
			setMes(nMes);
			setAno(nAno);
			return;
		}
	}

	void setDia(int nDia){
		dia = nDia;
		return;
	}
	void setMes(int nMes){
		mes = nMes;
		return;
	}
	void setAno(int nAno){
		ano = nAno;
		return;
	}

	int getDia(){
		return dia;
	}
	int getMes(){
		return mes;
	}
	int getAno(){
		return ano;
	}

	void displayDate(){
		if((dia == -1)||(mes == -1)||(ano == -1)){
			System.out.println("DATA INVALIDA");
		}else{
			System.out.println(dia+"/"+mes+"/"+ano);
		}
	}
}

class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int datas = input.nextInt();

		for(int i=0;i < datas; i++){
			int nDia = input.nextInt();
			int nMes = input.nextInt();
			int nAno = input.nextInt();

			Date classeData = new Date(nDia,nMes,nAno);
			classeData.displayDate();
		}

		input.close();
		classeData.close();
	}
}
