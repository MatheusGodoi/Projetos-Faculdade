#include <stdio.h>
#include <stdlib.h>

int calcula(int *ptr);
void printa(int tempo);

int main () {
    int total;
    int *ptr;
    int hora=0, min=0, seg=0;

    scanf("%d",&total);

    ptr = &total;

    while(total>=3600){//pega horas
        hora ++;
        total -= 3600;
    }

    min = calcula(ptr);//calcula minutos
    seg = total;//resto s�o os minutos

    printa(hora);
    printf(":");
    printa(min);
    printf(":");
    printa(seg);

    return 0;
}

int calcula(int *ptr){
    int resto;

    resto = *ptr / 60;

    *ptr %= 60;

    return resto;
}

void printa(int tempo){

    if(tempo>9){
        printf("%d",tempo);
    }else{
        printf("0%d",tempo);
    }

}
