//package PilhaInt;
import java.util.Scanner;

public class Exercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Pilha stack = new Pilha();
        int num;
        
        num = sc.nextInt();
        for(int i = 0; i < num; i++){
            char opc;
            int aux;
            opc = sc.next().charAt(0);
            switch(opc){
                case 'I': 
                    aux = sc.nextInt();
                    stack.push(aux);
                    break;
                case 'R':
                    stack.pop();
                    break;
                case 'P':
                    aux = stack.top();
                    if(aux != -1){
                        System.out.println(aux);
                    }
                    break;
            }
        }
    }  
}
