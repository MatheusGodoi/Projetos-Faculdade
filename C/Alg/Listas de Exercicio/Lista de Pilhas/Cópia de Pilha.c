#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int elem;

typedef struct bloco{
	elem info;
	struct bloco *prox;
}no;

typedef struct{
	no* top;
}pilha;


void criaPilha(pilha *P){

	P->top = NULL;

return;
}

void push(pilha *P , elem *X){
	no *aux;
	
	aux = (no*) malloc(sizeof(no));
	if(aux==NULL){
		printf("Problema malloc");
		return;
	}
	
	aux->info = *X;
	aux->prox = P->top;
	P->top = aux;	
	
}

int empty(pilha *P){
	if(P->top == NULL){
		return 1;
	}
	return 0;
}

int pop(pilha *P, elem *X){
	
	no* aux;
	
	if(empty(P)){
		return 0;
	}
	
	*X = P->top->info;
	aux = P->top;
	P->top = P->top->prox;
	free(aux);
	
	return 1;
}

void copiaPilha(pilha *P, pilha *C){
	elem aux;
	pilha Paux;
	
	criaPilha(&Paux);
	
	while(pop(P,&aux)){
		push(&Paux,&aux);
	}
	
	while(pop(&Paux,&aux)){
		push(C,&aux);
	}
	
	return;
}

int main (){

	pilha P, Copia;
	elem X;
	int i=0, erro = 0;
	
	criaPilha(&P);
	criaPilha(&Copia);
	
	
	for(;i<=5;i++){
		X = i;
		push(&P,&X);
	}

	copiaPilha(&P,&Copia);

	for(i=0;i<=5;i++){
		erro = pop(&Copia,&X);
		if(!erro){
			break;
		}
		printf("%d",X);
	}

return 0;
}
