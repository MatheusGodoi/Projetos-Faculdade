#include <stdio.h>
#include <stdlib.h>

typedef int elem;

typedef struct blocoF{
	elem info;
	struct blocoF *prox;
}noF;

typedef struct{
	noF* inicio, *fim;
}fila;

typedef struct bloco{
	elem info;
	struct bloco *prox;
}no;

typedef struct{
	no* top;
}pilha;


void criaPilha(pilha *P){

	P->top = NULL;

return;
}

void push(pilha *P , elem *X){
	no *aux;

	aux = (no*) malloc(sizeof(no));
	if(aux == NULL){
		printf("Problema push");
	}

	aux->info = *X;
	aux->prox = P->top;
	P->top = aux;

}

int emptyP(pilha *P){
	if(P->top == NULL){
		return 1;
	}
	return 0;
}

int pop(pilha *P, elem *X){

	no* aux;

	if(emptyP(P)){
		return 0;
	}

	*X = P->top->info;
	aux = P->top;
	P->top = P->top->prox;
	free(aux);

	return 1;
}

void criaFila(fila* F){
	F->inicio = NULL;
	F->fim = NULL;
}

void entraFila(fila* F,elem* X){
	noF *aux;

	aux = (noF*)malloc(sizeof(noF));
	if(aux == NULL){
		printf("Erro no malloc");
		return;
	}

	aux->info = *X;
	aux->prox = NULL;

	if(F->inicio == NULL){
		F->inicio = aux;
	}

	F->fim->prox = aux;
	F->fim = aux;

return;
}

int empty(fila* F){
	if(F->inicio == NULL){
		return 1;
	}
return 0;
}

int saiFila(fila* F, elem* X){
	noF* aux;

	if(empty(F)){
		return 0;
	}

	*X = F->inicio->info;
	aux = F->inicio;
	F->inicio = F->inicio->prox;
	if(F->inicio == NULL){
		F->fim = NULL;
	}
	free(aux);

return 1;
}

void inverteFila(fila* F){
	pilha aux;
	elem X;

	criaPilha(&aux);

	if(saiFila(F,&X)){
		push(&aux,&X);
	}

	if(pop(&aux,&X)){
		entraFila(F,&X);
	}

return;
}

int main () {
	fila F;
	elem X;
	int i=0;

	criaFila(&F);

	for(;i<5;i++){
		X = 1;
		entraFila(&F,&X);
	}

	inverteFila(&F);

	for(i=0;i<5;i++){
		saiFila(&F,&X);
		printf("%d",X);
	}


return 0;
}
