import java.util.Scanner;

class Date {
	int dia;
	int mes;
	int ano;

	Date(){
		dia = -1;
		mes = -1;
		ano = -1;
	}

	Date(int nDia,int nMes,int nAno){
		if((nDia <= 0)||(nMes <= 0)||(nAno <=0)){
			dia = -1;
			mes = -1;
			ano = -1;
			return;
		}
		if((nDia > 31)||(nMes > 12)){
			dia = -1;
			mes = -1;
			ano = -1;
			return;
		}


		if((nMes == 2)&&(nDia > 28)){
			dia = -1;
			mes = -1;
			ano = -1;
			return;
		}else if((nMes == 2)&&(nDia <= 28)){
			dia = nDia;
			mes = nMes;
			ano = nAno;
			return;
		}	

		if(nDia == 31){
			if((nMes == 4)||(nMes == 6)||(nMes == 7)||(nMes == 11)){
				dia = -1;
				mes = -1;
				ano = -1;
				return;
			}else{
				dia = nDia;
				mes = nMes;
				ano = nAno;				
				return;
			}
		}else{
			dia = nDia;
			mes = nMes;
			ano = nAno;
			return;
		}
	}

	int getDia(){
		return dia;
	}
	int getMes(){
		return mes;
	}
	int getAno(){
		return ano;
	}

	void displayDate(){
		if((dia == -1)||(mes == -1)||(ano == -1)){
			System.out.println("DATA INVALIDA");			
		}else{
			System.out.println(dia+"/"+mes+"/"+ano);			
		}
	}
}

class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int datas = input.nextInt();

		for(int i=0;i < datas; i++){
			int nDia = input.nextInt();
			int nMes = input.nextInt();
			int nAno = input.nextInt();

			Date classeData = new Date(nDia,nMes,nAno);	
			classeData.displayDate();
		}

		input.close();

	}
}
