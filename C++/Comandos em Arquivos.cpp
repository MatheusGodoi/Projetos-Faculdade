#include <stdio.h>
#include <stdlib.h>

int main(){
	FILE* fArq;
	int i=0;
	char palavra[100];
	char charAux;
	/*
	//Vai dar erro, arquivo não existe
	fArq = fopen("oi.txt","r");
	if(fArq == NULL){
		printf("ERRO AO ABRIR!\n");
	}*/
	
	//Vai funcionar, modo de escrita( w ) ele cria o arquivo
	fArq = fopen("oi.txt","w");
	if(fArq == NULL){
		printf("Nao acontece!\n");
	}
	
	//Fechar o arquivo 
	fclose(fArq);
	
	//Abrindo arquivo para escrever/ler
	fArq = fopen("teste.txt","r+");
	
	//Não chegou no final do arquivo
	//feof(arquivo) checa se achou o final do arquivo
	while(!feof(fArq)){
		i++;
		//Le palavra por palavra do arquivo
		fscanf(fArq,"%s",palavra);
		//Imprime palavra por palavra na tela
		printf("%d [%s]\n",i, palavra );
		//Escreve no arquivo a estrutura: "i ____", onde i é o numero da palavra; sobresceve o conteudo do arquivo.
		fprintf(fArq,"%d %s\n",i, palavra );
	}
	
	//Ter certeza que escreveu
	fflush(fArq);
	
	//Pular no arquivo com fseek( ponteiro_do_arquivo, +/- espaços, referencial)
	/*
	Referenciais possiveis:
	SEEK_SET	Beginning of file
	SEEK_CUR	Current position of the file pointer
	SEEK_END	End of file *
	*/
	fseek(fArq, 5, SEEK_SET);
	//Escreve na posicao 6 do arquivo a frase "posicao inicio + 5"
	fprintf(fArq,"posicao inicio + 5");

	fseek(fArq, -17, SEEK_END);
	//Mostra a palavra que esta 17 caracteres antes do final do arquivo
	fscanf(fArq,"%s",palavra);
	printf("Palavra na posicao final -17:\n%s",palavra);
	
	//Le 50*1 ( 50 unidades do tamanho char) do arquivo no ponteiro e salva em palavra
	fread(palavra, sizeof(char), 50, fArq);
	//Pega 10*1 ( 10 unidades do tamanho char) da variavel palavra e escreve no arquivo
	fwrite(palavra, sizeof(char), 10, fArq);

	//Retorna o caracter da posição do ponteiro no arquivo para a variavel
	charAux = fgetc(fArq);
	printf("%c\n",charAux);
	//Le uma string de 50 caracteres do arquivo e salva em palavra
	fgets(palavra, 50, fArq);
	printf("%s\n",palavra);

	//Escreve um char no arquivo na posição do ponteiro
	fputc(charAux,fArq);
	//Escreve uma string no arquivo na posição do ponteiro
	fputs(palavra,fArq);


	//Sempre lembrar de fechar o arquivo, senão fica preso na memória
	fclose(fArq);
	
	return 0;
}
