import sys
import csv 

def main(empresa):
	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)
	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			listaVoos.append(row)

	#Soma da quantidade de voo por tipo de voo e escrita em um arquivo
	tipo = [0, 0, 0]
	for user in listaUser:
		for voo in listaVoos:
			if user[0] == voo[1]:
				if voo[4] == 'economic':
					tipo[0] = tipo[0] + 1
				elif voo[4] == 'firstClass':
					tipo[1] = tipo[1] + 1
				elif voo[4] == 'premium':
					tipo[2] = tipo[2] + 1


	#Limpeza do arquivo para eliminar dados passados
	open('/home/matheus-godoi/Downloads/flask/static/tipoXqtd.txt', 'w').close()
	f = open("/home/matheus-godoi/Downloads/flask/static/tipoXqtd.txt", "a+")
	f.write("Economic," + str(tipo[0]) + "\n")
	f.write("First Class," + str(tipo[1]) + "\n")
	f.write("Premium," + str(tipo[2]) + "\n")
	f.close()	

if __name__ == '__main__':
	main(sys.argv[1])
