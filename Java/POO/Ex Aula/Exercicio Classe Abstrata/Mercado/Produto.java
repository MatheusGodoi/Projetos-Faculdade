package Mercado;

public abstract class Produto implements Comparable<Produto>{
	protected int preco;
	protected String marca;

	public void Produto(){
		this.preco = -1;
		this.marca = "ERRO";
	}

	public void Produto(int preco, String marca){
		this.preco = preco;
		this.marca = marca;
	}

	public abstract int compareTo();
}