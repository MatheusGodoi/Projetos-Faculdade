#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void achavalor(int vet[],int tam,int *menor,int *maior);

int main () {
    int tam,menor,maior;
    int * vet;
    int i=0;

    scanf("%d",&tam);
    vet = (int*)malloc(tam*sizeof(int));
    assert(vet!=NULL);
    for(;i<tam;i++){
        scanf(" %d",&vet[i]);
    }

    achavalor(vet,tam,&menor,&maior);
    printf("%d %d",maior,menor);

    return 0;
}

void achavalor(int vet[],int tam,int *menor,int *maior){
    int i=0;

    *maior = vet[0];        //acha maior valor
    for(;i<tam;i++){
        if(vet[i]>*maior){
            *maior = vet[i];
        }
    }

    *menor = vet[0];        //acha menor valor
    for(i=0;i<tam;i++){
        if(vet[i]<*menor){
            *menor = vet[i];
        }
    }
}
