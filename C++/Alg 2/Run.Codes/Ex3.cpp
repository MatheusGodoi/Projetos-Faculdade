#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <limits.h>

using namespace std;

#define INF INT_MAX

typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef vector<int> vi;

typedef struct
{
	int nVert;
	int nEdge;
	vector<vii> adjList;
}Graph;

Graph initGraph(int nVert)
{
	Graph g;
	g.nVert = nVert;
	g.adjList.resize(nVert);
	return g;
}

void add(Graph *g, int v, int w, int weight)
{
	g->adjList[v].push_back(make_pair(w, weight));
}

void dijkstra(Graph &g, vi &dist, vi &p)
{
    int shortestPath=INF;
    int vertShortestPath;
    ii w;

    for (int i=0; i<g.nVert; i++)
    {
        for (int j=0; j<(int)dist.size(); j++)
        {
            if (p[j]==0 && dist[j]<shortestPath)
            {
                shortestPath = dist[j];
                vertShortestPath = j;
            }
        }
        p[vertShortestPath] = 1;

        for (int k=0; k<(int)g.adjList[vertShortestPath].size(); k++)
        {
            w = g.adjList[vertShortestPath][k];

            if (p[w.first]==0 && w.second+shortestPath<dist[w.first])
                dist[w.first] = shortestPath + w.second;
        }
        shortestPath = INF;
    }
}

// void printGraph(Graph g)
// {
// 	for (int i = 0; i < g.nVert; ++i)
// 	{
// 		printf("%d ->> ", i);
// 		for (int j = 0; j < g.adjList[i].size(); ++j)
// 		{
// 			printf("%d ", g.adjList[i][j]);
// 		}
// 		printf("\n");
// 	}
// }


int main()
{
	int nCell, nExit, nTime, timer, nLine, v, w, nCase, count=0;

	scanf("%d", &nCase);
	scanf("%d", &nCell);
	scanf("%d", &nExit);
	scanf("%d", &nTime);
	scanf("%d", &nLine);

	Graph g = initGraph(nCell+1);
	
	scanf("%d %d %d", &v, &w, &timer);
	add(&g, v, w, timer);
	
	vi p = vi(nLine, 0);
	p[0] = nExit;
		
	vi dist = vi(nLine, INF);
	dist[0] = v;

	int i=0;
	do
	{
		scanf("%d %d %d", &v, &w, &timer);
		add(&g, v, w, timer);
		i++;
	}while(i<nLine-1);

	//printGraph(g);
	
	for(int j=0; j<(int)g.adjList[1].size(); j++)
		dist[g.adjList[1][j].first] = g.adjList[1][j].second;
		
	dijkstra(g, dist, p);
	for(int i=0; i<nLine; i++)
	{			
		if(dist[i]==nTime)		
		count++;
	}
	printf("%d\n", count);
	return 0;
}
