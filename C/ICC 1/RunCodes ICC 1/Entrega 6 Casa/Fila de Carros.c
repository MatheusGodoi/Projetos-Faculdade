#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct{
    char nome[10];
    int ano;
    float preco;
}carros;

struct Node{
    carros elem;
    struct Node *prox;
};
typedef struct Node node;


int inicia(node* fila);
int insereFila(node* fila,int tam);
node* removeFila(node* fila,int tam);
int vazia(node *fila);
void mostraFila(node* fila,int tam);

int main () {
    int tam=0, qtd;
    node *fila, *aux;
    char opt;

    fila = (node *)malloc(sizeof(node));
    assert(fila!=NULL);

    tam = inicia(fila);

    scanf("%d",&qtd);


    do{
        scanf(" %c",&opt);
        switch(opt){
            case 'i': tam = insereFila(fila,tam); qtd--; break;
            case 'r': aux = removeFila(fila,tam); free(aux); qtd--; break;
            default: printf("\nErro\n"); break;
        }
    }while(qtd!=0);

    mostraFila(fila,tam);

return 0;
}

int inicia(node* fila){
    fila->prox = NULL;

    return 0;
}

int insereFila(node* fila,int tam){
    carros aux;
    node* novoNo = (node*) malloc(sizeof(node));

    scanf(" %s %d %f",aux.nome,&aux.ano,&aux.preco);

    novoNo->elem = aux;
    novoNo->prox = NULL;

    if(vazia(fila)){
        fila->prox = novoNo;
    }else{
        node *temp = fila->prox;

            while(temp->prox!=NULL){
                temp = temp->prox;
            }

        temp->prox = novoNo;
    }
    tam++;

    return tam;
}

int vazia(node *fila){
	if(fila->prox == NULL)
		return 1;
	else
		return 0;
}

node* removeFila(node* fila, int tam){
    int lixo;

    scanf(" %d",&lixo);
    scanf(" %d",&lixo);
    scanf(" %d",&lixo);

	if(fila->prox == NULL){
		return NULL;
	}else{
		node *temp = fila->prox;
		fila->prox = temp->prox;
		tam--;
		return temp;
	}
}

void mostraFila(node* fila,int tam){

    if(vazia(fila)){
        return;
    }

    node* temp = fila->prox;
    while(temp != NULL){
        printf("%s %d %.2f\t",temp->elem.nome, temp->elem.ano, temp->elem.preco);
        temp = temp->prox;
    }
}
