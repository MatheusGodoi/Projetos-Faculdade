

public class Shampoo extends Produto{
	protected int numIrrita;

	public void Shampoo(int preco, String marca, int numIrrita){
		super(preco, marca);
		this.numIrrita = numIrrita;
	}

	public int compareTo(Object other){
		if(Object instanceof Leite){
			if(this.preco*this.numIrrita > other.preco*other.numIrrita){
				return -1 * (this.preco*this.numIrrita);
			}else if(this.preco*this.numIrrita > other.preco*other.numIrrita){
				return other.preco*other.numIrrita;
			}else{
				return 1;
			}
		}else if(Object instanceof Biscoito){
			if(this.preco*this.numIrrita > other.preco*other.compCancer){
				return -1 * (this.preco*this.numIrrita);
			}else if(this.preco*this.numIrrita > other.preco*other.compCancer){
				return other.preco*other.compCancer;
			}else{
				return 1;
			}
		}else if(Object instanceof Shampoo){
			if(this.preco*this.numIrrita > other.preco*other.numIrrita){
				return -1 * (this.preco*this.numIrrita);
			}else if(this.preco*this.numIrrita > other.preco*other.numIrrita){
				return other.preco*other.numIrrita;
			}else{
				return 1;
			}	
		}else{
			System.out.println("ERRO\n\n");
		}
	}
}