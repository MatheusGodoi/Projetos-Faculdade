import sys
import csv 

def main(empresa):
	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)
	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			listaVoos.append(row)
	#Leitura do CSV de hoteis, jogando valores em uma lista de hoteis
	with open('/home/matheus-godoi/Downloads/flask/hotels.csv') as csvfile:
		hotelCSV = csv.reader(csvfile, delimiter=',')
		listaHoteis = []
		for row in hotelCSV:
			listaHoteis.append(row)

	#Contador para limitar numero de funcionarios
	i = 0
	#Soma do custo de cada funcionario por voo
	preco = 0
	for user in listaUser:
		for voo in listaVoos:
			if user[0] == voo[1]:
				#Formatacao do custo para obter 2 casas decimais
				preco = preco + float("{0:.2f}".format(float(voo[5])))
		i = i + 1
		if i == 20:
			break

	#Contador para limitar numero de funcionarios
	i = 0
	#Soma do custo de cada funcionario por hotel
	for user in listaUser:
		for hotel in listaHoteis:
			if user[0] == hotel[1]:
				#Formatacao do custo para obter 2 casas decimais
				preco = preco + float("{0:.2f}".format(float(hotel[6])))
		i = i + 1
		if i == 20:
			break


	#Total de custos da empresa com hospedagem e voos em todos funcionarios
	print(preco)
	return preco

if __name__ == '__main__':
	main(sys.argv[1])
