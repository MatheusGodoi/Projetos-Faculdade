# Matheus Vinicius G de Godoi 10295217
# Hugo A Vitulli 10295221

import os
import time
from collections import defaultdict
from gensim import corpora, models, similarities
from gensim.parsing.preprocessing import remove_stopwords
from gensim.parsing.porter import PorterStemmer

# Read all documents in the given directory, trains a TF-IDF model and returns it, a dictionary with the unique terms indexed and a corpus
# IMPORTANT: All of the files needs to be in "utf8" format.
def trainModel(baseDirName, readDocumentsNameDir, mode):
    numberDocuments = 0
    documentTokenList = []
    processedCorpus = []
    uniqueTokenList = []
    frequency = defaultdict(int)

    # Initializes the Stemmer before looping
    p = PorterStemmer()
    # Opens a file to save all read documents name
    with open(readDocumentsNameDir, "w+") as readDocumentsName:
        # Opens the entire directory (./base) with all files inside
        listOfFiles = [os.path.join(dp, f) for dp, dn, filenames in os.walk(baseDirName) for f in filenames if os.path.splitext(f)[1] == ".utf8"]
        # Runs for every file inside the folder and its sub-folders
        for f in listOfFiles:
            uniqueTokenList.clear()
            frequency.clear()
            documentTokenList.clear()
            numberDocuments += 1
            # Opens a document
            with open(f, "r") as document:
                # Saves the document number and name in the readDocumentsName file
                readDocumentsName.write("Document: " + str(numberDocuments) + " Name: " + str(document.name) + "\n")
                documentData = document.read().lower()
                # For each token in the document
                if mode == "2":
                    documentData = remove_stopwords(documentData)
                elif mode == "3":
                    documentData = p.stem(documentData)
                elif mode == "4":
                    documentData = remove_stopwords(documentData)
                    documentData = p.stem(documentData)               
                for token in documentData.split():
                    # Increases its frequency
                    frequency[token] += 1
                    # If its a new token, adds to a unique token list
                    if token not in uniqueTokenList:
                        uniqueTokenList.append(token)

                # For each "unique" token, adds to a list if it appears at least 2 times
                for token in uniqueTokenList:
                    if frequency[token] > 1:
                        documentTokenList.append(token)

                # Adds to list of lists, with all tokens of all documents
                processedCorpus.append(list(documentTokenList))
            document.close()
    readDocumentsName.close()
    # Uses gensim dictionary to get unique tokens ids
    dictionary = corpora.Dictionary(list(processedCorpus))

    # Converts the dictionary into a vector with the ID and word count
    bowCorpus = [dictionary.doc2bow(text) for text in processedCorpus]

    # Trains a TFIDF model
    tfidf = models.TfidfModel(bowCorpus)

    return dictionary, bowCorpus, tfidf

def main():
    # Shows menu with options
    print("Choose method: ")
    print ("1 - Only indexing")
    print ("2 - Indexing + Removing Stop-words")
    print ("3 - Indexing + Stemming")
    print ("4 - Indexing + Removing Stop-words + Stemming")

    mode = input("Method: ")

    # Starts the dictionary, the model and the bow corpus
    dictionary, bowCorpus, tfidfModel = trainModel("./base", "./info/readDocuments.txt", mode)

    # Asks for a query
    query = input("Query to search: ").split()

    # Starting execution timer
    startTime = time.perf_counter()

   # Formats it
    queryBow = dictionary.doc2bow(query)
    index = similarities.SparseMatrixSimilarity(tfidfModel[bowCorpus], num_features=len(dictionary))
    # Looks for similarities
    sims = index[tfidfModel[queryBow]]

    # Final timer
    finalTime = time.perf_counter()

    # Sorts the results and prints the top 30 results to prevent cluttering
    i = 0
    for document_number, score in sorted(enumerate(sims), key=lambda x: x[1], reverse=True):
        if score > 0:
            i += 1
            print("Document " + str(document_number+1) + ", Similarity: " + str(score))
            if i == 29:
                break

    print("============================================")
    print (f"\nExecution time: {finalTime - startTime:0.4f} seconds")

if __name__ == "__main__":
    main()