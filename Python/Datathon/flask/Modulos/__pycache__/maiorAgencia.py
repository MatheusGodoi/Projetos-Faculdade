import sys
import csv 

def main():
	#Abertura do relatorio de quantidade de voo por agencia aerea
	with open("/home/matheus-godoi/Downloads/flask/qtdeXagencia.txt") as fp:
		line = fp.readline()
		#Total de agencias [Rainbow, CloudFy, FlyingDrops, Outras]
		totalAgencias = [0, 0, 0, 0]
		i = 0
		#Leitura e atribuição dos valores de voos por empresa
		while line:
			print("Line: " + line)
			totalAgencias[i] = (line.split(","))[1]
			line = fp.readline()
			i = i + 1

	print(totalAgencias)
	rainbow = totalAgencias[0]
	cloudFy = totalAgencias[1]
	flyingDrops = totalAgencias[2]
	outras = totalAgencias[3]

	#Checagem pela maior empresa de voo e retorno
	if rainbow > cloudFy:
		if rainbow > flyingDrops:
			if rainbow > outras:
				print("rainbow maior" + str(rainbow))
				return ["rainbow", rainbow]

	if cloudFy > flyingDrops:
		if cloudFy > outras:
			print("cloudFy maior" + str(cloudFy))
			return ["cloudFy", cloudFy]

	if flyingDrops > outras:
		print("flyingDrops maior" + str(flyingDrops))
		return ["flyingDrops", flyingDrops]

	print("outras empresas" + str(outras))
	return ["outras", outras]

if __name__ == '__main__':
	main()
