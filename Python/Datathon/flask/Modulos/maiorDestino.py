import sys
import csv 
import datetime
import operator

def main(empresa):
	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)

	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			if row[0] != 'travelCode':
				listaVoos.append(row)

	#Dicionario guardando valores do destino e quantidade de viagens ate o mesmo
	destinos = {}
	for voo in listaVoos:
		if voo[3] not in destinos:
			destinos[voo[3]] = 1
		else:
			destinos[voo[3]] += 1

	#Retorna o destino com maior viagens e o total
	maiorDestino = max(destinos.items(), key=operator.itemgetter(1))
	print(maiorDestino)

	return maiorDestino

if __name__ == '__main__':
	main(sys.argv[1])