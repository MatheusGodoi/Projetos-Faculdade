#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
int matriz[max][max], i, j, m,n, soma=0; //contadores i,j   linha (m) x colunas (n)


printf("Insira Linhas x Colunas: ");
scanf("%d %d",&m,&n);

//valores aleatorios na matriz
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matriz[i][j]=rand()%21-10;
    }
}//fim da atribuišao

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        if(matriz[i][j]>0){
            soma=soma+matriz[i][j];
        }
    }
}

printf("\nSoma final: %d\n",soma);
printf("\nMatriz:\n");
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}


printf("\n");
system ("pause");
return 0;
}
