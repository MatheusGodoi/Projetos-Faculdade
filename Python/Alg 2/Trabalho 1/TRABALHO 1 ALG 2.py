from __future__ import print_function
import queue

#==================Grafo Parte 1=============================#
class Grafo_parte1:
	def __init__(self):
		self.numVert = 0
		self.listaAdj = [[]]							#Lista vazia para o grafo

	def iniciaGrafo1(self,nVert):			#iniciacao do grafo. Salva o numero de vertices e inicia todas as listas de adjacencia sendo vazia ( vert 0 recebe -1)
		self.numVert = nVert
		self.listaAdj.append([])
		self.listaAdj[0].append(-1)

	def addAresta1(self, noV, noW):			#Adcionar aresta na listaAdj
		self.listaAdj[noV].append(noW)
#============================================================#
#==================Grafo Parte 2=============================#
class Grafo_parte2:
	def __init__(self):
		self.numVert = 0
		self.numCaixa = 0
		self.tamCaixa = 0
		self.numObj = 0
		self.listaAdj = [[]]						#Lista vazia para o grafo

	def iniciaGrafo2(self,nVert):				#iniciacao do grafo. Salva o numero de vertices e inicia todas as listas de adjacencia sendo vazia ( vert 0 recebe -1)
		self.numVert = nVert
		for cadaVert in range(nVert):
			self.listaAdj.append([])

	def addAresta2(self, noV, noW, peso):		#Adcionar aresta na listaAdj
		parAux = (noW, peso)
		self.listaAdj[noV].append(parAux)

	def printGrafo(self):						#Método para printar o grafo p/ debug
		contador = 0							
		for cadaVertice in self.listaAdj:
			print("[",contador,"]")
			for cadaAresta in cadaVertice:
				print(cadaAresta)
			contador += 1
#============================================================#
#==================Funçoes Parte 1===========================#

def iniciaVetVisitados(grafo):					#Função para inciar o vetor de visitados (vert 0 recebe -1)
		vetVisitados = [0] * int(grafo.numVert+1)
		vetVisitados[0] = -1
		return vetVisitados

def iniciaVetPred(grafo):						#Função para inciar o vetor de predecessores (vert 0 recebe -1)
		vetPred = [0] * int(grafo.numVert+1)

		for cadaLista in grafo.listaAdj:
			for cadaAresta in cadaLista:
				if(int(cadaAresta)!=-1):
					vetPred[int(cadaAresta)] += 1
		vetPred[0] = -1
		return vetPred

def ordTopBFS(grafo):								#Função para tirar a Ord. Topológica
		vetVisitados = iniciaVetVisitados(grafo)		#Inicia vetor de visitados
		vetPredecessor = iniciaVetPred(grafo)			#Inicia vetor predecessores
		vetOrdTop = []
		filaOrd = queue.PriorityQueue()					#Cria uma fila de prioridade para ser a fila do BFS

		for cadaNo in range(1, grafo.numVert+1):		#Checa os predecessores e poe na fila para iniciar
			if vetPredecessor[cadaNo] == 0:
				filaOrd.put(cadaNo)

		while not filaOrd.empty():					#Enquanto a fila não esta vazia
			auxFila = filaOrd.get()						#Retira um elemento
			vetVisitados[auxFila] = 1					#Marca como visitado
			vetOrdTop.append(auxFila)					#Coloca o elemento retirado no vetor ordenado

			for cadaAresta in grafo.listaAdj[auxFila]:	#Diminui predecessor e checa todos os adjacentes com pred = 0 para por na fila
				vetPredecessor[cadaAresta] -= 1
				if vetPredecessor[cadaAresta] == 0:
					filaOrd.put(cadaAresta)

		return vetOrdTop
#============================================================#
#==================Funçoes Parte 2===========================#
def gerarGrafo(grafo, matrizEstado, stackObjetos):
	i = 0
	while i < grafo.numCaixa:							#Checagem de cada ID da matriz, adcionando se possivel a aresta no grafo com o custo
		j = 0
		while j < grafo.numObj+1:
			peso = 0
			k = 0
			while k < grafo.numObj+1-j:
				if peso > grafo.tamCaixa:
					break
				else:
					grafo.addAresta2(matrizEstado[i][j],matrizEstado[i+1][j+k],pow(grafo.tamCaixa-peso,2))
					peso += stackObjetos[j+k]
				k += 1
			j += 1
		i += 1									

def dijkstra(grafo, vetDist, vetVisitados):			#Dijkstra para calcular menor caminho
	menorCaminho = float('Inf')
	vertMenorCaminho = -1
	auxPar = (-1,-1)

	i = 0
	while i < grafo.numVert:
		j = 0
		while j < len(vetDist):
			if j not in vetVisitados:
				if vetDist[j] < menorCaminho:
					menorCaminho = vetDist[j]
					vertMenorCaminho = j
			j += 1
		vetVisitados.append(vertMenorCaminho)
		i += 1

		k = 0
		while k < len(grafo.listaAdj[vertMenorCaminho]):
			auxPar = grafo.listaAdj[vertMenorCaminho][k]

			if auxPar[0] not in vetVisitados:
				if (auxPar[1] + menorCaminho) < vetDist[auxPar[0]]:
					vetDist[auxPar[0]] = menorCaminho + auxPar[1]
			k += 1
		menorCaminho = float('Inf');    
#============================================================#
#========================Main================================#
def main():
	grafoPt1 = Grafo_parte1()
	grafoPt2 = Grafo_parte2()
	modo = int(input())

	if modo == 1:									#Parte 1 do trabalho
		numVert, numAre = input().split()					#leitura de entrada
		grafoPt1.iniciaGrafo1(int(numVert))

		for arestas in range(int(numAre)):
			noV, noW = input().split()

			while int(noV) >= len(grafoPt1.listaAdj):
				grafoPt1.listaAdj.append([])

			grafoPt1.addAresta1(int(noV), int(noW))

		vetOrdTop = ordTopBFS(grafoPt1)						#Função da Ordenação Topológica

		for cadaElemento in vetOrdTop:						#Rotina para printar a lista da ordenação topológica 
			if cadaElemento == '[':
				continue
			if cadaElemento == ',':
				continue
			if cadaElemento == ']':
				break
			print(cadaElemento, end=' ', flush=True)

	elif modo == 2:
		grafoPt2.numObj, grafoPt2.numCaixa, grafoPt2.tamCaixa = input().split()	#Leitura de entrada

		grafoPt2.numObj = int(grafoPt2.numObj)				#Cast para inteiro
		grafoPt2.numCaixa = int(grafoPt2.numCaixa)	
		grafoPt2.tamCaixa = int(grafoPt2.tamCaixa)	
	
		stackObjetos = []

		aux = input().split()								#Criação da pilha com todos objetos
		for item in aux:
			stackObjetos.append(int(item))
		stackObjetos.append(float('Inf'))						#Inserção de um infinito no final para evitar estourar o indice

		contadorIDs = 0										#Criação da matriz de estados com os ID's
		matrizEstado = []
		for cadaObj in range(grafoPt2.numCaixa+1):
			matrizEstado.append([])
			for cadaCaixa in range(grafoPt2.numObj+1):
				matrizEstado[cadaObj].append(contadorIDs)
				contadorIDs += 1

		grafoPt2.iniciaGrafo2(int(contadorIDs-1))				#Tamanho do grafo ira ser igual o numero de casas na matriz

		gerarGrafo(grafoPt2, matrizEstado,stackObjetos)		#Função para geraro grafo

		#grafoPt2.printGrafo()								#Printar grafo para debug

		vetDist = []
		vetDist.append(0)
		for cadaIndice in range(int(grafoPt2.numVert)):		#Inicia o vetor com as distancias
			vetDist.append(float('Inf'))
		for cadaAresta in grafoPt2.listaAdj[0]:					#Adciona os valores conhecidos
			vetDist[cadaAresta[0]] = cadaAresta[1]

		vetVisitados = []									#Vetor visitado vazio

		dijkstra(grafoPt2, vetDist, vetVisitados)			#Calculo do caminho minimo

		if vetDist[grafoPt2.numVert] == float('Inf'):		#Caso a distancia até o nó final seja infinito, não existe solução	
			print("-1")											#Print caso não existe
		else:
			print(vetDist[grafoPt2.numVert])					#Print final
		
	else:
		print("Alternativa de modo inválida!")

if __name__ == '__main__':
	main()