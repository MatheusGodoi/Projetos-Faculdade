public class Motorcycle {
	private int speed;
	private int gear;
	private int id;
	static int numberOfMotorcycles;

	public Motorcycle(){
		//Construtor Padrão
		speed = 0;
		gear = 0;
		id = numberOfMotorcycles+50;
		numberOfMotorcycles++;
	}

	public Motorcycle(int valueSpeed, int valueGear){
		//Construtor com parametros
		speed = valueSpeed;
		gear = valueGear;
		id = numberOfMotorcycles+50;
		numberOfMotorcycles++;
	}

	public int getGear(){
		return gear;
	}

	public int gearUp(){
		gear++;
	}

	public int getSpeed(){
		return speed;
	}

	public int speedUp(int newSpeed){
		speed += newSpeed;
		gear = Integer.parseInt(speed/7)+1;
	}

	public int applyBreaks(int breakValue){
		speed -= breakValue;
	}

	public int getID(){
		return id;
	}
}

