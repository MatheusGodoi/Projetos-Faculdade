import Animal

public class Vaca extends Animal {
	protected String corteVaca;

	public Vaca(String nomeAnimal, int numeroPatas, String corte) {
		super(nomeAnimal, numeroPatas);
		corteVaca = corte;
	}
	public void emitirSom(){
		System.out.println("MUUUUUUoisouamaedoshotsUUUU");
	}

	public String toString(){
		return ("Nome: "+nomeAnimal+"\nNumero de Patas: "+numeroPatas+"\n"+"Corte: "+corteVaca);
	}
}