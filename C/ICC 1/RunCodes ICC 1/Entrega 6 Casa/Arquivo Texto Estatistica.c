#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    char nome[20];
    char sexo;
    int idade;
}regStruct;

regStruct* leArquivo(int* cont);

int main () {
    regStruct* registro;
    int cont = 0, i=0;
    int masc = 0, fem = 0;
    float mascmed = 0, femmed = 0, medtotal = 0;

    registro = leArquivo(&cont);

    printf("%d ",cont); //total de registros

    for(;i<cont;i++){
        if(registro[i].sexo == 'm'){
            masc++;
            mascmed += registro[i].idade;
        }else{
            fem++;
            femmed += registro[i].idade;
        }
        medtotal += registro[i].idade;
    }

    printf("%d %d ",masc, fem);

    medtotal = medtotal / cont;
    mascmed = mascmed / masc;
    femmed = femmed / fem;

    printf("%.2f %.2f %.2f",medtotal,mascmed,femmed);

    return 0;
}

regStruct* leArquivo(int* cont){
    regStruct* reg;
    int i=0;

    FILE*arq = fopen("dados.txt","r");
    if (arq == NULL) {
       printf ("Houve um erro ao abrir o arquivo\n");
       exit(0);
    }

    reg = (regStruct*) malloc(sizeof(regStruct));
    assert(reg!=NULL);

    while(1){
        reg[i].idade = -1;

       /* fscanf(arq," %s",reg[i].nome);
        fscanf(arq," %c",&reg[i].sexo);
        fscanf(arq," %d",&reg[i].idade);*/
        fscanf(arq," %s %c %d", reg[i].nome, &reg[i].sexo, &reg[i].idade);

        if(reg[i].idade == -1){
            break;
        }

        i++;
        reg = (regStruct*) realloc(reg, (i+1) * sizeof(regStruct));
        assert(reg!=NULL);
    }

    (*cont) = i;

    fclose(arq);

    return reg;
}

