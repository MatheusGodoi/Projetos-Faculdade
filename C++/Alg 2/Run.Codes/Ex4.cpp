#include <vector>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

typedef pair<int,int> parInt;
typedef vector<parInt> vetorParInt;
typedef vector<int> vetorInt;

typedef struct{
	int nVert;
	int nEdge;
	vector<vetorParInt> adjList;
}Grafo;

Grafo iniciaGrafo(int nVert){
	Grafo g;
	g.nVert = nVert;
	g.adjList.resize(nVert);
	return g;
}

void addAresta(Grafo &g, int noV, int noW, int peso){
	g.adjList[noV].push_back(make_pair(noW, peso));
}

void BelmanFord(Grafo &g, vetorInt &vetDist, vetorInt &vetPai){
	parInt wAux;

	for(int i = 0; i < g.nVert-1; i++){
		for(int j = 0; j < g.nVert; j++){
			for(int k = 0; k < (int)g.adjList[j].size(); k++){
				wAux = g.adjList[j][k];
				if(vetDist[j] + wAux.second < vetDist[wAux.first]){
					vetPai[wAux.first] = j;
					vetDist[wAux.first] = vetDist[j] + wAux.second;
				}
			}
		}	
	}
}

bool cicloNegativo(Grafo &g, vetorInt &dist){
	parInt wAux;
	
	for (int j = 0; j < g.nVert; ++j)
		for (int k = 0; k < (int)g.adjList[j].size(); ++k){
			wAux = g.adjList[j][k];  
			if (dist[j] + wAux.second < dist[wAux.first]){
				return true;
			}
		}
	return false;
}

int main(){
	int nCasos, nEstrela, nWormHoles, noV, noW, peso;

	scanf("%d", &nCasos);

	for (int i = 0; i < nCasos; i++){
		scanf("%d %d", &nEstrela, &nWormHoles);
		Grafo g = iniciaGrafo(nEstrela);
		
		for (int j = 0; j < nWormHoles; j++){
			scanf("%d %d %d", &noV, &noW, &peso);
			addAresta(g, noV, noW, peso);
		}

		vetorInt vetPai(g.nVert);

		vetorInt vetDist(nEstrela, 9999);
		vetDist[0] = 0;

		BelmanFord(g, vetDist, vetPai);

		if(cicloNegativo(g, vetDist)){ 
			printf("possible\n");
		}else{
			printf("not possible\n");
		}
	}
	return 0;
}