#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int calculafat(int num,int i);

int main () {
    int inicio, tam;
    int num;
    int * vetor;
    int i=0;

    scanf("%d %d",&inicio,&tam);
    num = inicio;

    vetor = (int*)malloc(tam*sizeof(int));//aloco vetor do tamanho necessário

    assert(vetor!=NULL);

    for(;i<tam;i++){
        vetor[i] = calculafat(num,i);
    }

    for(i=0;i<tam;i++){
        printf("%d ",vetor[i]);
    }

    return 0;
}

int calculafat(int num, int i){
    int j=1, fat=1;

    num = num+i;

    for(;j<=num;j++){
        fat *= j;
    }

return fat;
}
