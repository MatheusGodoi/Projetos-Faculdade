/* RODRIGO LUIZ PEIREIRA DOS SANTOS N�MERO USP: 10262461
MATHEUS VINICIUS GOUVEA DE GODOI N�MERO USP: 10295217*/

/* PARTE 1

DADOS: OS DADOS COLETADOS PARA CADA USU�RIO SER�O NOME E ENDERE�O.

OPERA��ES: AS OPERA��ES ESCOLHIDAS PARA O TAD FORAM "INSERIR NOME NA AGENDA","REMOVER NOME DA AGENDA","IMPRIMIR ENDERE�O COM BASE EM UM NOME BUSCADO"
E "IMPRIMIR NOME COM BASE EM UM ENDERE�O BUSCADO".
*/

/* PARTE 2: EST� IMPLEMENTADA NO C�DIGO ABAIXO*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 100


typedef struct{
    char nome[max];
    char endereco[max];
}agenda;

/* INSERIR NOME NA AGENDA*/
agenda* insereAgenda(agenda *vet,int* tam){ //variavel tam para receber o tamanho do vetor
    int z = *tam; //auxiliar para alterar o tamanho original do vetor

    vet = (agenda*)realloc(vet,(z+1)*sizeof(agenda)); //realoca o tamanho novo para o vetor

    printf("Nome para ser inserido?\n");
    scanf(" %[^\n]s",vet[z].nome);

    printf("Endereco para ser inserido?\n");
    scanf(" %[^\n]s",vet[z].endereco); //pega os dados para serem inseridos

	z++;
    *tam = z;
    return vet; //retorna o vetor com o novo termo.
}

/* REMOVER NOME DA AGENDA*/
agenda* removeAgenda(agenda *vet, int* tam){
    char auxNome[max], auxEnde[max];
    int i=0, z = *tam; //z � auxiliar para alterar tamanho do vetor

    printf("Nome para ser removido?\n");
    scanf(" %[^\n]s",auxNome); //recebe os termos para serem removidos
    printf("Endereco para ser removido?\n");
    scanf(" %[^\n]s",auxEnde);

    for(;i<z;i++){
        if(!strcmp(vet[i].nome,auxNome)){
            if(!strcmp(vet[i].endereco,auxEnde)){
                strcpy(vet[i].nome,vet[z-1].nome);                 //caso o tamanho seja maior que 1, o ultimo termo da agenda
                strcpy(vet[i].endereco,vet[z-1].endereco);          // substitui o termo a ser removido, assim n�o se perde dados
                vet = (agenda*)realloc(vet,(z-1)*sizeof(agenda));   //e podemos usar o realloc sem medo
                break;
            }
        }
    }

    
    z--;
    *tam = z;
    return vet; //retorna o vetor com o novo termo.
}

/* BUSCAR NOME NA AGENDA*/
void buscaNome(agenda *vet,int tam){
    char auxNome[max];
    int i=0, j=1, flag=0; //flag para ver se encontrou algum caso

    printf("Nome para ser procurado?\n");
    scanf(" %[^\n]s",auxNome);  //recebe o nome para procurar

    for(;i<tam;i++){
        if(!strcmp(vet[i].nome,auxNome)){
                    printf("Caso %d\nNome: %s\nEndereco: %s\n",j,vet[i].nome,vet[i].endereco);
                    j++;
                    flag++;
                    //caso nome e o procurado sejam iguais, ele printa todas as ocorrencias.
                    //variavel j somente para contar quantos termos foram encontrados
            }
        }

        if(!flag){
            printf("\nNenhum caso Encontrado\n");
        }

return;
}

/* BUSCAR ENDERE�O NA AGENDA*/
void buscaEndereco(agenda *vet,int tam){
    char auxEnde[max];
    int i=0,j=1, flag=0; //flag para ver se encontrou algum caso

    printf("Endereco para ser procurado?\n");
    scanf(" %[^\n]s",auxEnde);

    for(;i<tam;i++){
        if(!strcmp(vet[i].endereco,auxEnde)){
                  printf("Caso %d\nNome: %s\nEndereco: %s\n",j,vet[i].nome,vet[i].endereco);
                    j++;
                    flag++;
                    //caso nome e o procurado sejam iguais, ele printa todas as ocorrencias.
                    //variavel j somente para contar quantos termos foram encontrados
            }
        }

        if(!flag){
            printf("\nNenhum caso Encontrado\n");
        }

return;
}

int main () {
	agenda *vet = NULL;
	int tam=0;

return 0;
}
