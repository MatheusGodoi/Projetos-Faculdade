#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main ()
{
    int a, b, atroca, btroca;

    printf ("Escreva valor a: ");
    scanf ("%d",&a);
    printf ("\nEscreva valor b: ");
    scanf ("%d",&b);

    atroca = a;
    btroca = b;
    a = btroca;
    b = atroca;

    printf ("\nSeus valores: a = %d, b = %d\n",a,b);

    system("pause");
    return 0;
}
