#include <stdio.h>
#include <stdlib.h>
#define max 100001

typedef int elem;

typedef struct temp{
    elem info;
    struct temp *esq, *dir;
}no;

typedef struct{
    no *raiz;
}BST;

void inicializa(BST *Arvore){
     Arvore->raiz=NULL;
}

void printaArvore(no **p){
     if (*p!=NULL) {
        printf("%d ",(*p)->info);
        printaArvore(&(*p)->esq);
        printaArvore(&(*p)->dir);
     }
}

void finalizaArvore(no **p){
     if(*p!=NULL){
        finalizaArvore(&(*p)->esq);
        finalizaArvore(&(*p)->dir);
        free(*p);
     }
}

void insereArvore(no **p, elem *x){

    if(*p==NULL){
        *p=(no*) malloc(sizeof(no));

        (*p)->info=*x;
        (*p)->esq=NULL;
        (*p)->dir=NULL;
        return;
     }else if(*x<(*p)->info){
          return(insereArvore(&(*p)->esq,x));
     }else if(*x>=(*p)->info){
          return(insereArvore(&(*p)->dir,x));
     }
}

int main(){
    int tam, i, aux[max];
    BST Arvore;

    inicializa(&Arvore);

    scanf("%d",&tam);

    for(i=0;i<tam;i++){
        scanf("%d", &aux[i]);
    }
    for(i=0;i<tam;i++){
        insereArvore(&Arvore.raiz,&aux[i]);
    }

    printaArvore(&Arvore.raiz);
    printf("\n");

    finalizaArvore(&Arvore.raiz);

    return 0;
}
