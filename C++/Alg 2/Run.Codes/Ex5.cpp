#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

#define MAX 1000

typedef pair<int,int> parInt;
typedef vector<parInt> vetorParInt;
typedef vector<int> vetorInt;

typedef struct {
	int nVert;
	int nEdge;
	int **matAdj;
}Grafo;

Grafo iniciaGrafo(int nVert){
	Grafo g;
	g.nVert = 0;
	g.nEdge = 0;
	g.matAdj = (int **) malloc(nVert * sizeof(int *));
	for (int i = 0; i < nVert; ++i){
		g.matAdj[i] = (int *) malloc(nVert * sizeof(int));
	}
	for (int i = 0; i < nVert; ++i){
		for (int j = 0; j < nVert; ++j){
			g.matAdj[i][j] = 9999;
		}
	}
	   
	return g;
}

void addAresta(Grafo *g, int noV, int noW){
	g->matAdj[noV][noW] = 1;
	g->nEdge++;
}

int verMinimo(int a, int b){
	if(a < b){
		return a;
	}else{
		return b;
	}
}

void FloydWarshall(Grafo &g){
	for (int k = 1; k <= g.nVert; ++k){
		for (int i = 1; i <= g.nVert; ++i){
			for (int j = 1; j <= g.nVert; ++j){
				g.matAdj[i][j] = verMinimo(g.matAdj[i][j], g.matAdj[i][k] + g.matAdj[k][j]);
			}
		}
	}
}

int main(){
	Grafo g;
	int noV, noW;
	int nVert = 0;
	int totalAux = 0;
	int totalPares = 0;
	int auxCasos = 1;
	float totalFinal = 0;

	g = iniciaGrafo(100);

	while(1){

		while(1){
			scanf("%d %d", &noV, &noW);
			if((noV == 0)&&(noW == 0)){
				break;
			}
			addAresta(&g, noV, noW);

			if(noV > noW){
				if(nVert < noV){
					nVert = noV;
				}
			}else{
				if(nVert < noW){
					nVert = noW;
				}
			}

		}
		g.nVert = nVert;

		for (int i = 1; i <= g.nVert; ++i){
			for (int j = 1; j <= g.nVert; ++j){
				if (i == j){
					g.matAdj[i][j] = 0;
				}
			}
		}

		FloydWarshall(g);

		for (int i = 1; i <= g.nVert; ++i){
			for (int j = 1; j <= g.nVert; ++j){
				if(j == i ){
					continue;
				}else if(g.matAdj[i][j] >= 9999){
					continue;
				}else{
					totalAux += g.matAdj[i][j];
					totalPares++;
				}
			}
		}
		totalFinal = (float)((float)totalAux/(float)totalPares);
		printf("Case %d: average length between pages = %.3f clicks\n",auxCasos,totalFinal);
		auxCasos++;
		scanf("%d %d",&noV, &noW);

		if((noV == 0)&&(noW == 0)){
			break;
		}else{
			//Resetando todos valores
			nVert = 0;
			totalPares = 0;
			totalAux = 0;
			totalFinal = 0;
			for (int i = 0; i <= g.nVert; ++i){
				for (int j = 0; j <= g.nVert; ++j){
					g.matAdj[i][j] = 9999;
				}
			}
			g.nVert = 0;
			g.nEdge = 0;

			addAresta(&g, noV, noW);
			if(noV > noW){
				nVert = noV;
			}else{
				nVert = noW;
			}
		}
	}
	return 0;
}
