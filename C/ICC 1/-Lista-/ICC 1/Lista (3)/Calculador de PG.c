#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>

int main (){

    float raz, primeiro, ultimo, soma, segundo;
    int numerotermo;
    numerotermo=2;

    printf ("Insira Raiz, o Primeiro termo e o Ultimo termo da PG: ");
    scanf ("%f %f %f",&raz,&primeiro,&ultimo);

    segundo=primeiro*raz;

    if (segundo==ultimo){
            printf ("Sua pg tem os termos: %.1f | %.1f !\n",primeiro,ultimo);
            system ("pause");
            return 0;

    } else {
        printf ("%.1f | %.1f | ",primeiro,segundo);

        do while (segundo!=ultimo){
        segundo=primeiro*(pow(raz,numerotermo));
                numerotermo = numerotermo+1;
                printf ("%.1f | ",segundo);
        }while (segundo!=ultimo);
    }
    printf ("\nPronto!Esses sao os termos da sua PG!\n");
    system ("pause");
    return 0;

}
// Soma = a1 (razao^numerotermo - 1) / razao - 1
