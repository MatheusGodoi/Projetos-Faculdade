#include <stdio.h>
#include <stdlib.h>
#define max 100001

typedef int elem;

typedef struct temp{
    elem info;
    struct temp *esq, *dir;
}no;

typedef struct{
    no *raiz;
}BST;

void inicializa(BST *Arvore){
     Arvore->raiz=NULL;
}

void insereArvore(no **p, elem *x){

    if(*p==NULL){
        *p=(no*) malloc(sizeof(no));

        (*p)->info=*x;
        (*p)->esq=NULL;
        (*p)->dir=NULL;
        return;
     }else if(*x<(*p)->info){
          return(insereArvore(&(*p)->esq,x));
     }else if(*x>=(*p)->info){
          return(insereArvore(&(*p)->dir,x));
     }
}

int buscaElem(no **p, elem *x){
    if(*p==NULL){
       return 0;
    }else if(*x<(*p)->info){
         return(buscaElem(&(*p)->esq,x));
    }else if(*x>(*p)->info){
         return(buscaElem(&(*p)->dir,x));
    }else{
        return 1;
    }
}

void finalizaArvore(no **p){
     if(*p!=NULL){
        finalizaArvore(&(*p)->esq);
        finalizaArvore(&(*p)->dir);
        free(*p);
     }
}

int main(){
    int tam, i, aux[max], flag;
    elem busca;
    BST Arvore;

    inicializa(&Arvore);

    scanf("%d",&tam);

    for(i=0;i<tam;i++){
        scanf("%d", &aux[i]);
    }
    for(i=0;i<tam;i++){
        insereArvore(&Arvore.raiz,&aux[i]);
    }

    scanf("%d",&busca);

    flag = buscaElem(&Arvore.raiz,&busca);

    if(flag){
        printf("elemento encontrado\n");
    }else{
        printf("elemento nao encontrado\n");
    }

    finalizaArvore(&Arvore.raiz);

    return 0;
}
