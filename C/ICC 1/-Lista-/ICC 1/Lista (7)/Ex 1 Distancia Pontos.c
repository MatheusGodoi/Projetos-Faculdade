#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define max 2

float distancia(int a[],int b[]);

int main () {
float pontoa[max], pontob[max], dist;
int i=0;

printf("Insira cordenadas do primeiro ponto: ");
scanf("%f %f",&pontoa[0],&pontoa[1]);
printf("Insira cordenadas do segundo ponto: ");
scanf("%f %f",&pontob[0],&pontob[1]);

dist = distancia(pontoa,pontob);

printf("\nDistancia: %.2f",dist);
return 0;
}

float distancia(int a[],int b[]){
float x1, x2, y1, y2;
float quad1, quad2, soma;

float dist;

x1 = a[0]; x2 = b[0];
y1 = a[1]; y2 = b[1];

quad1 = pow((x2-x1),2);
quad2 = pow((y2-y1),2);
soma = quad1+quad2;

dist=sqrt(soma);

return dist;
}
