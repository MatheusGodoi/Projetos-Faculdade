#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

#define MAX 1000

typedef pair<int,int> parInt;
typedef vector<parInt> vetorParInt;
typedef vector<int> vetorInt;

typedef struct {
	int nVert;
	int nEdge;
	vector<vetorParInt> adjList;
}Grafo;

Grafo iniciaGrafo(int nVert){
	Grafo g;
	g.nVert = nVert;
	g.adjList.resize(nVert);
	return g;
}

void addAresta(Grafo *g, int noV, int noW, int peso){
	g->adjList[noV].push_back(make_pair(noW, peso));
}

void dijkstra(Grafo &g, vetorInt &dist, vetorInt &p){
    int menorCaminho=99999;
    int vertMenorCaminho;
    parInt w;

    for (int i=0; i<g.nVert; i++){
        for (int j=0; j<(int)dist.size(); j++){
            if (p[j]==0 && dist[j]<menorCaminho){
                menorCaminho = dist[j];
                vertMenorCaminho = j;
            }
        }
        p[vertMenorCaminho] = 1;

        for (int k=0; k<(int)g.adjList[vertMenorCaminho].size(); k++){
            w = g.adjList[vertMenorCaminho][k];

            if (p[w.first]==0 && w.second+menorCaminho<dist[w.first])
                dist[w.first] = menorCaminho + w.second;
        }
        menorCaminho = 99999;
    }
}


int main(){
	int i, j, k;
	Grafo g;
	int noV, noW, peso, priAux, ultAux;
	int nVert, cnt, nLine, aux;

	i = 0; j = 0; k = 0;

	scanf("%d\n", &cnt);
	for (; i<cnt; i++){
		aux = 0;
		scanf("%d %d %d %d", &nVert, &nLine, &priAux, &ultAux);
		g = iniciaGrafo(nVert);

		if(nLine > 0){
			while(aux<nLine){
				scanf("%d %d %d", &noV, &noW, &peso);
				addAresta(&g, noV, noW, peso);
				addAresta(&g, noW, noV, peso);
				aux++;

			}

			vetorInt vetVisitados = vetorInt(nVert, 0);
			vetVisitados[0] = ultAux;

			vetorInt vetDistancias = vetorInt(nVert, 9999);
			vetDistancias[0] = noV;

			for (j=0; j<(int)g.adjList[0].size(); j++){
	       		vetDistancias[g.adjList[0][j].first] = g.adjList[0][j].second;

	    	}
	    	dijkstra(g, vetDistancias, vetVisitados);

	    	for (k=0; k<nVert; k++){
	    		if(aux<vetDistancias[k])
	    			aux = vetDistancias[k];

	   		}
	   		printf("Case #%d: %d\n", i+1, aux);
	   	}else{
			printf("Case #%d: unreachable\n", i+1);
	   	}
	}

	return 0;
}
