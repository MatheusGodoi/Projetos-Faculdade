//package ex1rc;

public class App<T  extends Comparable<? super T>>{
    public T[] vetorT;
    
    public App(T[] vetorM){
        this.vetorT = vetorM;
    }
    
    public void printVector(){
        for (int i = 0; i < this.vetorT.length; i++) {
            System.out.print(this.vetorT[i].toString() + " ");
        }
        System.out.print("\n");        
    }
    
    public void bubbleSort(){
        for (int i = 0; i < this.vetorT.length; i++) {
            for (int j = 0; j < this.vetorT.length-1; j++) {
                T aux;
                if(this.vetorT[j].compareTo(this.vetorT[j+1]) > 0){
                    aux = this.vetorT[j+1];
                    this.vetorT[j+1] = this.vetorT[j];
                    this.vetorT[j] = aux;
                }
            }
        }
    }
}
