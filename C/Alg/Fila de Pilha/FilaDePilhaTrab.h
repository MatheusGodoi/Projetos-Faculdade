#include <stdio.h>
#include <stdlib.h>
#include "Pilha.h"

/*
Matheus Vinicius Gouvea de Godoi	10295217
Luiz Guilherme Martins Adorno		10392171
*/

typedef int elem;

typedef struct {
	int total;
	Pilha P;
} Fila;

// Cria uma fila F.
void Cria(Fila *F);

// Esvazia uma fila F.
void Esvazia(Fila *F);

// Retorna 1 se a fila F estiver vazia e zero se n�o estiver vazia.
int EstaVazia(Fila *F);

// Retorna 1 se a fila F estiver cheia e zero se n�o estiver cheia.
int EstaCheia(Fila *F);

// Adiciona o elemento X � fila F. Se houver erro retorna 1. Caso contr�rio, retorna 0
int Entra(Fila *F, elem *X);

// Adiciona o primeiro elemento da fila F, atribuindo seu valor � X. Se houver erro retorna 1. Caso contr�rio, retorna 0
int Sai(Fila *F, elem *X);
