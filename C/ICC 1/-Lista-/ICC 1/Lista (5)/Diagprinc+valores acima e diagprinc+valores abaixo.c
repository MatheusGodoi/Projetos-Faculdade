#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
int matriz[max][max], somaacima=0, somaabaixo=0, somadiag=0;
int i, j, m, n; //contadores i,j   linha (m) x colunas (n)


printf("Insira Linhas x Colunas: ");
scanf("%d %d",&m,&n);

//valores aleatorios na matriz
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matriz[i][j]=rand()%101+100;
    }
}//fim da atribuišao

for(i=0;i<m;i++){//soma da diagonal principal
        if(i<n){
    somadiag=somadiag+matriz[i][i];
}   else {
        break;
    }
}

for(i=0;i<m;i++){ //valores acima
        j=i+1;
    for(;j<n;j++){
        somaacima=somaacima+matriz[i][j];
    }
}
somaacima=somaacima+somadiag;//resultado final acima

for(j=0;j<n;j++){
    i=j+1;
    for(;i<m;i++){
        somaabaixo=somaabaixo+matriz[i][j];
    }
}
somaabaixo=somaabaixo+somadiag;//resultado final abaixo

printf("\nMatriz:\n");//printa matriz
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}

printf("\nSoma dos valores acima: %d\nSoma dos valores abaixo: %d\n",somaacima, somaabaixo);

printf("\n");
system ("pause");
return 0;
}
