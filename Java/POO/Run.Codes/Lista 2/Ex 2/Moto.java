class Moto extends Veiculo {

	public Moto(float velocidadeAtual, double aceleracaoBase, float distanciaPercorrida){
		super(velocidadeAtual, (4.5*aceleracaoBase), distanciaPercorrida);
	}
}