from flask import Flask, render_template, request, json
import Modulos.qtdeXfunc as modQtdeXfunc
import Modulos.custoXfunc as modCustoXfunc
import Modulos.mesXcusto as modMesXcusto
import Modulos.mesXqtd as modMesXqtd
import Modulos.qtdeXagencia as modQtdeXagencia
import Modulos.tipoXqtd as modTipoXqtd
import Modulos.custoTotal as modCustoTotal
import Modulos.maiorAgencia as modMaiorAgencia
import Modulos.maiorDestino as modMaiorDestino

from flask_bootstrap import Bootstrap
import operator
import sys
import csv 

app = Flask(__name__)
Bootstrap(app)

#Pagina principal
@app.route('/')
def index():
	#return render_template('/home/matheus-godoi/Downloads/flask/index.html')
    #return open('index.html').read()
    return open('argoDashboard.html').read()

#Metodos separados para cada request que gera cada relatorio
#Relatorio de quantidade de viagens por funcionario
@app.route('/qtdeXfunc/')
def handleQtdeXfunc():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modQtdeXfunc.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Relatorio de quantidade de viagens por agencia
@app.route('/qtdeXagencia/')
def handleQtdeXagencia():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modQtdeXagencia.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Relatorio de tipos de voo (1 classe, economico, premium) por quantidade
@app.route('/tipoXqtd/')
def handleTipoXqtd():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modTipoXqtd.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Relatorio de quantidade de voos por mes
@app.route('/mesXqtd/')
def handleMesXqtd():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modMesXqtd.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Relatorio de custos com voos por mes
@app.route('/mesXcusto/')
def handleMesXcusto():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modMesXcusto.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Relatorio custo total com voos por funcionario
@app.route('/custoXfunc/')
def handleCustoXfunc():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modCustoXfunc.main(empresa)
	print("pronto!")
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

#Custos totais da empresa com todos voos e funcionarios
@app.route('/custoTotal/')
def handleCustoTotal():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	custos = modCustoTotal.main(empresa)
	print("pronto!")
	return json.dumps({'success':True, 'total': custos}), 200, {'ContentType':'application/json'} 

#Agencia com maior numero de viagems
@app.route('/maiorAgencia/')
def handleMaiorAgencia():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	modQtdeXagencia.main(empresa)
	maiorAgencia = modMaiorAgencia.main()
	print("pronto!")
	return json.dumps({'success':True, 'agencia': maiorAgencia[0], 'numero': maiorAgencia[1]}), 200, {'ContentType':'application/json'} 

#Destino com maior numero de viagens
@app.route('/maiorDestino/')
def handleMaiorDestino():
	#argumentos = request.args
	#empresa = str(argumentos.get('nomeEmpresa'))
	empresa = '4You'
	maiorDestino = modMaiorDestino.main(empresa)
	print("pronto!")
	print(maiorDestino)
	return json.dumps({'success':True, 'destino': maiorDestino[0], 'numero': maiorDestino[1]}), 200, {'ContentType':'application/json'} 


if __name__ == '__main__':
	app.run(debug=True)