class Grafo:
	def __init__(self):
		self.numVert = 0
		self.listaAdj = [[]]

	def iniciaGrafo(self, nVert):						# Inicializacao do grafo ( automato )
		self.numVert = int(nVert)
		for cadaNumero in range(self.numVert):
			self.listaAdj.append([])

	def addAresta(self, noV, noW, peso):				# Adcionar Aresta no grafo ( automato )
		par = (peso, int(noW))
		indice = int(noV)
		self.listaAdj[indice].append(par)

	def printGrafo(self):								# Metodo para printar o grafo p/ debug
		contador = 0
		for cadaVertice in self.listaAdj:
			print("[",contador,"]")
			for cadaAresta in cadaVertice:
				print(cadaAresta)
			contador += 1

def main():
	nVert = int(input())
	g = Grafo()

	g.iniciaGrafo(nVert)

	vetorTerminais = input()
	#vetorTerminais = vetorTerminais.replace(vetorTerminais[0], "")
	vetorTerminais= vetorTerminais[2:]

	estadosIniciais = int(input())
	for cadaNumero in range(estadosIniciais):
		parAux = ('i', -1)									# 'i' indica que e um inicial do grafo
		g.listaAdj[cadaNumero].append(parAux)

	vetorAceitacao = input()

	numArestas = int(input())
	for cadaNumero in range(numArestas):
		noV, peso, noW = input().split()
		g.addAresta(int(noV),int(noW), peso)

	numTestes = int(input())
	for cadaNumero in range(numTestes):
		fraseTestas = input()

	#g.printGrafo()

if __name__ == '__main__':
	main()
