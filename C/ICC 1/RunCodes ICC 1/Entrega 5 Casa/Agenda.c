#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define max 200

typedef struct {
    int dia;
    int mes;
    int ano;
} datas;

typedef struct {
    int hora;
    int min;
    int seg;
} tempo;

typedef struct {
    datas data;
    tempo horario;
    char frase[max];
} agenda;

agenda *criaAgenda(int *qtd);

int main () {
    int qtd, i;
    agenda *point;

    scanf("%d",&qtd);

    point = criaAgenda(&qtd);

    for(i=0;i<qtd;i++){
        printf("%.2d/%.2d/%.2d - ",point[i].data.dia,point[i].data.mes,point[i].data.ano);
        printf("%.2d:%.2d:%.2d\n",point[i].horario.hora,point[i].horario.min,point[i].horario.seg);
        printf("%s\n",point[i].frase);
    }

    free(point);

    return 0;
}

agenda *criaAgenda(int *qtd){
    agenda *vet;
    int j=0;

    vet = NULL;

    for(j=0;j<*qtd;j++){

        vet = (agenda*) realloc (vet, *qtd*sizeof(agenda));
        assert (vet!=NULL);

        scanf(" %d",&vet[j].data.dia);
        scanf(" %d",&vet[j].data.mes);
        scanf(" %d",&vet[j].data.ano);
        scanf(" %d",&vet[j].horario.hora);
        scanf(" %d",&vet[j].horario.min);
        scanf(" %d",&vet[j].horario.seg);
        //fflush(stdin);
        scanf(" %[^\n]s",vet[j].frase);
    }

    return vet;
}
