package bas;
import java.util.*;
import robocode.*;
import robocode.control.*;
import java.awt.Color;

public class R2D2 extends Robot
{
	//Taxa de crossover
	private static final double CRATE = 100;
	//Taxa de mutação
	private static final double MRATE = 100;
	//Tamanho da população
	private static final int POPSIZE = 9;
	//Soma de comandos + parâmetros em um indivíduo
	private static final int INDSIZE = 8;
	//Salva acertos e dano
	private int hit = 0;
	//Inicializa número aleatório
	Random r = new Random();	
	//Se foi atingido, verdadeiro
	boolean hitByBullet = false;

	public int matrizRobos[][] = new int [POPSIZE][INDSIZE];
    public int vetFit[] = new int [POPSIZE];	
	/*Comandos úteis
	//Coleta energia do robô no momento atual:
	//this.getEnergy();
	//Move robô para frente X pixels
	ahead(int pixels);
	//Move robô para trás X pixels
	back(int pixels);
	//Vira arma para direita (0 a 360)
	turnGunRight(int graus);
	//Vira arma para esquerda (0 a 360)
	turnGunLeft(int graus);
	//Vira robô para direita (0 a 180)
	turnRight(int graus);
	//Vira robô para esquerda (0 a 180)
	turnLeft(int graus);
	*/
	
	//Aqui começam as funções do algoritmo evolutivo
	
	public static int randInt(int min, int max) {

	    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
	}
	
	//Inicialize seus indivíduos aqui
    private void initInd(int i){
    	for(int j=0; j < INDSIZE; j++){
    		aux = randInt(1,9);
    		if((aux > 9)||(aux < 1)){
    			System.out.prinln("ERRO NO RANDOM");
   				aux = 8;
    		}

    		if((aux < 7)&&(aux > 2)){
				matrizRobos[i][j] = aux;
				aux = randInt(0,360);
				matrizRobos[i][j+1] = aux;
				j++;    			
    		}else if(aux > 3){
   				matrizRobos[i][j] = aux;
				aux = randInt(0,battlefield_height);
				matrizRobos[i][j+1] = aux;
    		}else{
    			matrizRobos[i][j] = aux;
    		}
    	}
    }
	
	//Inicialize sua população aqui
	private void initPop(){
    	for(int i = 0; i < POPSIZE; i++){
    		initInd(i);
    	}
    }
	

	private void execute(){
		for(int i = 0; i < POPSIZE; i++){
			/*Colocar a lista de comandos que o robô deverá realizar e coletar dados do fitness
			Ex:
			ahead(10);
			turnLeft(60);
			*/
			for(int j = 0; j < INDSIZE; j++){
				switch(matrizRobos[i][j]){
					case 1: ahead(matrizRobos[i][j++]); j++; break;
					case 2: back(matrizRobos[i][j++]); j++; break;
					case 3:	turnLeft(matrizRobos[i][j++]); j++; break;
					case 4: turnRight(matrizRobos[i][j++]); j++; break;
					case 5: turnGunLeft(matrizRobos[i][j++]); j++; break;
					case 6: turnGunRight(matrizRobos[i][j++]); j++; break;
					case 7: fire(1); break;
					case 8: fire(2); break;
					case 9:	fire(3); break;
					default: fire(2); break;
				}
				vetFit[i] = hit;
			}
	    }
    }
	
	//Ordena população em ordem de melhor fitness (OPCIONAL)
	private void bestFirst(){
	
	}

	//Mata piores indivíduos (OPCIONAL) e substitui por outro
    private void predation(){
    
    }
	
	//Seleção de pais para reprodução
	private void selection(int parent1, int parent2){
    	int pai1[] = matrizRobos[parent1];
    	int pai2[] = matrizRobos[parent2];
    	int filho1[] = new int [INDSIZE];
    	
    	filho1 = crossover(pai1,pai2);
    	filho2 = crossover(pai1,pai2);

    	int auxProb = 0;
    	for(int i = 0; i < INDSIZE; i++){
    		auxProb += filho1[i];
    	}
    	if(auxProb < 488){
    		mutation(filho1);
    	}

    	for(int i = 0; i < INDSIZE; i++){
    		auxProb += filho2[i];
    	}
    	if(auxProb > 488){
    		mutation(filho2);
    	}
	
	//Realiza a troca de genes entre 2 pais
	private int[] crossover(int pai1[],int pai2[]){
		int cross1[] = new int [INDSIZE];
		for(int i = 0; i < INDSIZE; i++){
			auxProb = randInt(1,2);
			if(auxProb == 1){
				cross1[i] = pai1[i];
				if((pai1[i] > 2)&&(pai1[i] < 7)){
					cross1[i+1] = pai1[i+1];
					i++;
				}
			}else if(auxProb == 2){
				cross1[i] = pai2[i];
				if((pai2[i] > 2)&&(pai2[i] < 7)){
					cross1[i+1] = pai2[i+1];
					i++;
				}
			}
		}

		return cross1;
		//Teste a taxa de crossover
		//Selecione os pais
		//Troque os genes entre os pais
    }
	
	//Realiza mutação em um indivíduo
	private void mutation(int filho[]){
		// Teste a taxa de mutação
		// Altere o cromossomo
		for(int i = 0; i < INDSIZE; i++){
			if(filho[i] > 3){
				filho[i+1] = randInt(0,battlefield_height);
				i+=2;
			}else if(filho[i] > 7){
				filho[i+1] = randInt(0,360);
				i+=2;
			}else{
				filho[i] = randInt(7,9);
			}
		}
    }
	
	//Aqui acabam as funções do algoritmo evolutivo
	
	
	//Função principal do seu algoritmo evolutivo
		public void run() {
        setColors(null, Color.WHITE, Color.BLUE, null, new Color(150, 0, 150)); 
		//Inicializa população inicial
		initPop();
		//Executa o RoboCode
        execute();
		//Executa o robô até acabar todos os rounds do campeonato
		for(int i = 0; true; i++) {
			//Decida aqui onde verificar fitness, chamar crossover, mutação, torneio, predação, etc :)
			//Executa o Robocode
		    int maiorFit = 0;
		    int auxFit = vetFit[0];
		    int pai1;
		    int pai2;
		    for(int j = 0; j<POPSIZE; j++){
		    	if(auxFit < vetFit[j]){
		    		maiorFit = j;
		    	}
		    }
		    pai1 = randInt(0,POPSIZE);
		    if(pai1 == maiorFit){
		    	if(pai1 == 0){
		    		pai1 +=1;
		    	}else{
		    		pai1 -=1;
		    	}
		    }
		    pai2 = randInt(0,POPSIZE);
		    if(pai2 == maiorFit){
		    	if(pai2 == 0){
		    		pai2 +=1;
		    	}else{
		    		pai2 -=1;
		    	}
		    }

		    selection(pai1,pai2)

		    execute();
		}
	}

	//Aqui são comandos para verificar diferentes ações que acontecem durante o jogo. Escolha como pontuar cada uma :)
	
	/**
	 * onScannedRobot: Alvo avistado. Quais as suas ordens, mestre?
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if(hitByBullet){
		    hitByBullet = false;
	    } else {
		    if (e.getDistance() > 200 || getEnergy() < 15) {
			    fire(1);
		    } else if (e.getDistance() > 50) {
			    fire(3);
		    } else {
			    fire(2);
		    }
        }
	}

	/**
	 * onHitByBullet: Você foi atingido. Do a barrel roll!
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		hitByBullet = true;
		hit -= 5; 
	}
	/**
	 * onBulletHit: Seu tiro acertou outro robô, parabéns :)
	 */
    public void onBulletHit(BulletHitEvent event) {
		hit = hit + 3;
    }
	/**
	 * onBulletHitBullet: Seu tiro acertou outro tiro. Você é um ninja?
	 */
    public void onBulletHitBullet(BulletHitBulletEvent event){
    	hit += 0;
    }
	/**
	 * onBulletMissed: Você errou o tiro... Precisa de óculos?
	 */
    public void onBulletMissed(BulletMissedEvent event){
        hit -= 2;
    }
	/**
	 * onHitRobot: Você bateu em outro robô... Acho que não é assim que você dá dano.
	 */
    public void onHitRobot(HitRobotEvent event){
        fire(3);
        hit -= 3;
        back(10);
    }
	/**
	 * onHitWall: Você bateu na parede... Como anda sua carteira de motorista?
	 */
    public void onHitWall(HitWallEvent event){
        hit -= 4;
        back(20);
    }  
	
}