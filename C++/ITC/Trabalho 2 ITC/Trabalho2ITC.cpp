/*
Introdução a Teoria da Computação - turma 2018
Alunos:
Hugo de Azevedo Vitulli  			10295221
Matheus Vinicius Gouvea de Godoi  	10295217
Reinaldo Mizutani					7062145
*/

#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX 30

using namespace std;

//Estrutura para armanezar as instruções 
typedef struct{
	int prox;
	char lido;
	char escrito;
	char direcao;
}structAdj;

typedef vector<structAdj> strAdj;

typedef struct{
	int nVert;
//	int nEdge;
	vector<strAdj> listaAdj;
}Grafo;

Grafo iniciaGrafo(int nVert){
	Grafo g;
	g.nVert = nVert;
	g.listaAdj.resize(nVert);
	return g;
}	
//Adcionar aresta da instrucao no grafo
void addAresta(Grafo *g, int noV, int noW, char lido, char escrito, char direcao){
	structAdj strAux;
	strAux.prox = noW;
	strAux.lido = lido;
	strAux.escrito = escrito;
	strAux.direcao = direcao;
	g->listaAdj[noV].push_back(strAux);
}

//Função para imprimir grafo 
void printGrafo(Grafo g){
	for(unsigned int i = 0; i < g.listaAdj.size(); i++){
		printf("[%d]\n",i);
		for(unsigned int j = 0; j < g.listaAdj[i].size(); j++){
			printf("Prox: %d\nLido: %c\nEscrito: %c\nDirecao: %c\n\n",g.listaAdj[i][j].prox, g.listaAdj[i][j].lido, g.listaAdj[i][j].escrito, g.listaAdj[i][j].direcao);
		}
	}
}

//Funcao para verificar se a cadeia de entrada só contem caracteres terminais
bool checarTerminais(char terminal[],char fita[], char frase[]){
	bool flagEncontrou;

	for (unsigned int i = 0; i < strlen(frase); i++){
		flagEncontrou = false;
		if(frase[i] == 'B'){
			continue;
		}
		for (unsigned int j = 0; j < strlen(terminal); j++){
			if(terminal[j] == ' '){
				continue;
			}
			if(frase[i] == terminal[j]){
				flagEncontrou = true;
				break;
			}
		}
		if(flagEncontrou){
			continue;
		}
		for (unsigned int k = 0; k < strlen(fita); k++){
			if(fita[k] == ' '){
				continue;
			}
			if(frase[i] == fita[k]){
				flagEncontrou = true;
				break;
			}
		}

		if(!flagEncontrou){
			return false;
		}
	}
	return true;
}


bool checarFrase(Grafo g, int vertAtual, char frase[], char terminal[], char fita[], int aceitacao, int indice){
	//Se chegou até estado de aceitação, é aceito		
	if(aceitacao == vertAtual){
		return true;
	}

	for (unsigned int i = 0; i < g.listaAdj[vertAtual].size(); i++){
		if(g.listaAdj[vertAtual][i].lido == frase[indice]){
			frase[indice] = g.listaAdj[vertAtual][i].escrito;

			if(g.listaAdj[vertAtual][i].direcao == 'L'){
				if(checarFrase(g, g.listaAdj[vertAtual][i].prox, frase, terminal, fita, aceitacao, indice-1)){
					return true;
				}
			}else if(g.listaAdj[vertAtual][i].direcao == 'R'){
				if(checarFrase(g, g.listaAdj[vertAtual][i].prox, frase, terminal, fita, aceitacao, indice+1)){
					return true;
				}
			}else if(g.listaAdj[vertAtual][i].direcao == 'S'){
				if(checarFrase(g, g.listaAdj[vertAtual][i].prox, frase, terminal, fita, aceitacao, indice)){
					return true;
				}
			}
		}
	}

	return false;
}

int main(){
	//Todas variaveis utilizadas
	int nVert = 0;
	int estadoAceitacao = 0;
	char vetorTerminais[MAX];
	char vetorFita[MAX];
	char fraseTestar[MAX];
	char vetorAux[20];
	int numArestas = 0;
	int numTestes = 0;
	int noV = 0;
	int noW = 0;
	bool flagFrase = false;
	int contadorSaida = 1;
	int aux;
	char lido;
	char escrito;
	char direcao;
	Grafo g;

	//Número de estados do grafo
	scanf("%d\n",&nVert);
	g = iniciaGrafo(nVert);

	//Vetor de caracteres terminais
	scanf("%d ",&aux);			//Numero de caracteres terminais nao importa na nossa implementaçao
	scanf("%[^\n]s",vetorTerminais);
	getchar();

	//Vetor de caracteres de fita
	scanf("%d ",&aux);			//Numero de caracteres de fita nao importa na nossa implementaçao
	scanf("%[^\n]s",vetorFita);
	getchar();

	//Estado de aceitacao da maquina
	scanf("%d\n",&estadoAceitacao);

	//Número de instruções entre estados
	scanf("%d\n", &numArestas);

	//Cada instruçao dos estados é uma nova aresta no grafo
	for (int i = 0; i < numArestas; i++){			
		scanf("%d %c %d %c %c\n",&noV, &lido, &noW, &escrito, &direcao);
		addAresta(&g, noV, noW, lido, escrito, direcao);
	}

	//Print do grafo para debug
	//printGrafo(g);

	//Número de frases para testar
	scanf("%d\n", &numTestes);
	for(int i = 0; i < numTestes; i++){				//Para cada frase de teste
		scanf("%[^\n]s",vetorAux);				//Pegar a frase
		getchar();

		//Montagem da cadeia w na estrutura BBBBBwBBBBB
		for(int j = 0; j < 5; j++){
			fraseTestar[j] = 'B';
		}
		fraseTestar[5] = '\0';
		strcat(fraseTestar,vetorAux);
		aux = strlen(vetorAux) + 5;
		fraseTestar[aux] = '\0';
		for(int j = strlen(fraseTestar); j < MAX; j++){
			fraseTestar[j] = 'B';
		}
		fraseTestar[MAX] = '\0';

		if(!checarTerminais(vetorTerminais, vetorFita, fraseTestar)){		//Checar se a frase contem apenas caracteres terminais
			printf("%d. rejeita\n",contadorSaida);
			contadorSaida++;
			continue;
		}

		flagFrase = checarFrase(g, 0, fraseTestar, vetorTerminais, vetorFita, estadoAceitacao, 5);	
		if(flagFrase){
			printf("%d. aceita\n",contadorSaida);		//Caso tenha encontrado uma aceitacao
			contadorSaida++;
		}

		if(!flagFrase){									//Caso nao tenha chegado no estado de aceitacao
			printf("%d. rejeita\n",contadorSaida);
			contadorSaida++;
		}
	}

	return 0;
}