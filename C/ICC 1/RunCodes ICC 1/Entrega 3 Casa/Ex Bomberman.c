#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 201

void tempo(char matriz[][max],int* lin,int* col,int* tem);
void calcula(char matriz[][max],int* lin,int* col,int tempo);

int main () {
   int m, n, t;
   char matriz[max][max];
   int i=0, j=0;
   int *lin,*col,*tem;

   lin = &m;
   col = &n;
   tem = &t;

   scanf("%d %d %d",&m,&n,&t);

   for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            scanf(" %c",&matriz[i][j]);
        }
   }

    tempo(matriz,lin,col,tem);

    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            printf("%c",matriz[i][j]);
        }
        printf("\n");
   }

return 0;
}

void tempo(char matriz[][max],int* lin,int* col,int* tem){
    int tempo;
    int i, j;
    tempo = *tem;
    tempo--;

    if((tempo%2)==1){
        for(i=0;i<*lin;i++){
            for(j=0;j<*col;j++){
                if(matriz[i][j]=='.'){
                    matriz[i][j]='0';
                }
            }
        }
        return;
    }

    while(tempo>0){
        calcula(matriz,lin,col,tempo);
        --tempo;//diminuo a a��o
    }

    for(i=0;i<*lin;i++){
        for(j=0;j<*col;j++){
            if(matriz[i][j]=='x'){
                matriz[i][j]='0';
            }
        }
    }
    return;
}

void calcula(char matriz[][max],int* lin,int* col,int tempo){
    int i=0, j=0, resto;
    resto = tempo%2;

    if(resto==0){
        for(i=0;i<*lin;i++){
            for(j=0;j<*col;j++){
                if(matriz[i][j]=='0'){
                    matriz[i][j]='x';
                }else  if(matriz[i][j]=='.'){
                    matriz[i][j]='0';
                }
            }
        }
    }else if(resto==1){
        for(i=0;i<*lin;i++){
            for(j=0;j<*col;j++){
                if(matriz[i][j]=='x'){
                    matriz[i][j]='.';
                    if(matriz[i+1][j]=='0'){
                        matriz[i+1][j]='.';
                    }
                    if(matriz[i-1][j]=='0'){
                        matriz[i-1][j]='.';
                    }
                    if(matriz[i][j+1]=='0'){
                        matriz[i][j+1]='.';
                    }
                    if(matriz[i][j-1]=='0'){
                        matriz[i][j-1]='.';
                    }
                }
            }
        }
    }

    return;
}
