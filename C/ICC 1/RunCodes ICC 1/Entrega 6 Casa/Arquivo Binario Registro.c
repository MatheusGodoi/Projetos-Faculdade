#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    char nome[40];
    int idade;
    char sexo;
    int matricula;
}strFunc;

strFunc* leArquivo(int* cont);

int main () {
    strFunc* registro;
    int cont = 0, i=0;
    int procura;

    registro = leArquivo(&cont);

    scanf("%d",&procura);

    for(;i<cont;i++){
        if(registro[i].matricula == procura){
            printf("Nome: %s\n", registro[i].nome);
            printf("Idade: %d\n", registro[i].idade);
            printf("Sexo: %c\n", registro[i].sexo);
            printf("Matricula: %d\n", registro[i].matricula);
        }
    }

    return 0;
}

strFunc* leArquivo(int* cont){
    strFunc* reg;
    int i=0, qtd;

    FILE*arq = fopen("dados-alunos.bin","rb");
    if (arq == NULL) {
       printf ("Houve um erro ao abrir o arquivo\n");
       exit(0);
    }

    fread(&qtd,sizeof(int),1,arq);

    reg = (strFunc*) malloc(sizeof(strFunc)*qtd);
    assert(reg!=NULL);

    while(1){
        reg[i].idade = -1;

        fread(reg[i].nome,sizeof(char),40,arq);
        fread(&reg[i].idade,sizeof(int),1,arq);
        fread(&reg[i].sexo,sizeof(char),4,arq);
        fread(&reg[i].matricula,sizeof(int),1,arq);

        if(reg[i].idade == -1){
            break;
        }

//printf("%s %d %c %d\n",reg[i].nome,reg[i].idade,reg[i].sexo,reg[i].matricula);

        i++;
    }

    (*cont) = qtd;

    fclose(arq);

    return reg;
}

