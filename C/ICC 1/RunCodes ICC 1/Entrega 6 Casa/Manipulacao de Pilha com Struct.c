#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct{
    char nome[20];
    int idade;
    char sexo;
}alStruct;

typedef struct {
    int topo;
    int capacidade;
    alStruct* elem;
}pStruct;

void criaPilha(pStruct* pilha,int capac);
int checaEstado(pStruct* pilha);    /* 1 = Vazia, 2 = Cheia, 3 = Nenhum dos dois */
void push(pStruct* pilha,char nome[],int idade, char sexo);
void pop(pStruct* pilha);
void mostraPilha(pStruct* pilha);

int main() {
    int qtd, i=0;
    char opc;
    pStruct pilha;
    char nome[20], sexo;
    int idade;

    scanf("%d",&qtd);

    criaPilha(&pilha,qtd);

    for(;i<qtd;i++){

        scanf(" %c %s %d %c", &opc, nome, &idade, &sexo);
        switch(opc){
            case 'i':
                if(checaEstado(&pilha)==2){
                    break;
                }
                push(&pilha,nome,idade,sexo);
                break;
            case 'r':
                if(checaEstado(&pilha)==1){
                    break;
                }
                pop(&pilha);
                break;
            default:
                printf("\nErro\n");
                break;
        }
    }

    mostraPilha(&pilha);

    return 0;
}

void criaPilha(pStruct* pilha,int capac){
    pilha->topo = -1;
    pilha->capacidade = capac;
    pilha->elem = (alStruct*) malloc(2 * sizeof(alStruct));

}

int checaEstado(pStruct* pilha){

    if(pilha->topo==-1){
        return 1;
    }else if(pilha->topo == pilha->capacidade-1){
        return 2;
    }else{
        return 3;
    }
/* 1 = Vazia, 2 = Cheia, 3 = Nenhum dos dois */
}

void push(pStruct* pilha,char nome[20],int idade, char sexo){

    if(checaEstado(pilha) != 2){
        pilha->topo++;

        strcpy(pilha->elem[pilha->topo].nome,nome);

        pilha->elem[pilha->topo].idade = idade;
        pilha->elem[pilha->topo].sexo = sexo;
    }
}

void pop(pStruct* pilha){
    if(checaEstado(pilha) != 1){
        pilha->topo--;
    }
}

void mostraPilha(pStruct* pilha){
    int i = 0;

    for(i=pilha->topo;i>=0;i--){
        printf("%s %d %c\t",pilha->elem[i].nome, pilha->elem[i].idade, pilha->elem[i].sexo);;
    }
}

