#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define max 30

int primeira(char pal1[max],char pal2[max]);//maior tamanho
int segunda(char pal1[max],char pal2[max]);//Ordem Alfab�tica
int terceira(char pal1[max],char pal2[max]);//soma dos valores das letras (A=0 at� Z=25)
int quarta(char pal1[max],char pal2[max],char letra);//repeti��es de uma determinada letra
int quinta(char pal1[max],char pal2[max],char letra);//letra aparece primeiro

int main () {
int menu,resultado;
char pal1[max],pal2[max], letra;

scanf("%d",&menu);
scanf(" %s",pal1);
scanf(" %s",pal2);
if((menu==4)||(menu==5)){
    scanf(" %c",&letra);
}

switch (menu){
    case 1: resultado =  primeira(pal1,pal2); break;
    case 2: resultado = segunda(pal1,pal2); break;
    case 3: resultado = terceira(pal1,pal2); break;
    case 4: resultado = quarta(pal1,pal2,letra); break;
    case 5: resultado = quinta(pal1,pal2,letra); break;
    default: break;
}

printf("%d",resultado);

return 0;
}


int primeira(char pal1[max],char pal2[max]){//maior tamanho
int compa1,compa2,resul;

compa1=strlen(pal1);
compa2=strlen(pal2);

if(compa1>compa2){
    resul=1;
}else if(compa2>compa1){
    resul=2;
}else if(compa2==compa1){
    resul=0;
}

return resul;
}

int segunda(char pal1[max],char pal2[max]){//Ordem Alfab�tica
int checa, resul, i, tam1, tam2;

tam1 = strlen(pal1);
tam2 = strlen(pal2);

for(i=0;i<tam1;i++){
    pal1[i]=toupper(pal1[i]);
}
for(i=0;i<tam2;i++){
    pal2[i]=toupper(pal2[i]);
}

checa=strcmp(pal1,pal2);

if(checa>0){
    resul = 2;
}else if(checa<0){
    resul = 1;
}else if(checa==0){
    resul = 0;
}

return resul;
}

int terceira(char pal1[max],char pal2[max]){//soma dos valores das letras (A=0 at� Z=25)
int tam1, tam2, i, soma1=0, soma2=0, resul;

tam1 = strlen(pal1);
tam2 = strlen(pal2);

for(i=0;i<tam1;i++){
    pal1[i]=toupper(pal1[i]);
}
for(i=0;i<tam2;i++){
    pal2[i]=toupper(pal2[i]);
}

for(i=0;i<=tam1;i++){
    soma1 += (pal1[i]-65);
}
for(i=0;i<=tam2;i++){
    soma2 += (pal2[i]-65);
}

if(soma1>soma2){
    resul = 1;
} else if(soma1<soma2){
    resul = 2;
}else if(soma1==soma2){
    resul = 0;
}

return resul;
}

int quarta(char pal1[max],char pal2[max],char letra){//repeti��es de uma determinada letra
int resul=5, rep1=0, rep2=0, i, tam1, tam2;

tam1 = strlen(pal1);
tam2 = strlen(pal2);

for(i=0;i<tam1;i++){
    pal1[i]=toupper(pal1[i]);
}
for(i=0;i<tam2;i++){
    pal2[i]=toupper(pal2[i]);
}
letra = toupper(letra);

for(i=0;i<tam1;i++){
    if(pal1[i]==letra){
        rep1++;
    }
}
for(i=0;i<tam2;i++){
    if(pal2[i]==letra){
        rep2++;
    }
}

if(rep1>rep2){
    resul = 1;
}else if(rep1<rep2){
    resul = 2;
}else if(rep1==rep2){
    resul = 0;
}

return resul;
}

int quinta(char pal1[max],char pal2[max],char letra){//letra aparece primeiro
int resul=5, tam1, tam2, posi1=0, posi2=0, i;

tam1 = strlen(pal1);
tam2 = strlen(pal2);

for(i=0;i<tam1;i++){
    pal1[i]=toupper(pal1[i]);
}
for(i=0;i<tam2;i++){
    pal2[i]=toupper(pal2[i]);
}
letra = toupper(letra); //tudo toupper

for(i=0;i<tam1;i++){
    if((letra)==(pal1[i])){
        posi1 = i+1;
        break;
    } //letra na primeira palavra
}
for(i=0;i<tam2;i++){
    if((letra)==(pal2[i])){
        posi2 = i+1;
        break;
    }//letra na segunda palavra
}

if(posi1 < posi2){
    resul = 1;
}else if(posi1 > posi2){
    resul = 2;
}else if(posi1 == posi2){
    resul = 0;
}else {
    resul = 575;
}

return resul;
}
