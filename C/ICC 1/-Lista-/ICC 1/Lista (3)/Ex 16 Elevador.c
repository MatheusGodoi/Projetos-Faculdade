#include <stdio.h>
#include <stdlib.h>
#define max 50

int main () {
int quantidades=0, distancia, andar1, andar2, percorreu, andares;
int total[max]={0};
int a,b, j; // contadores

scanf("%d %d",&quantidades,&distancia);

for (j=0;j<=quantidades;j++){
scanf("%d",&total[j]);
}

percorreu = 0;
a=-1;

for (b=1;b<quantidades;b++){
    a++;

    andar1=total[a];
    andar2=total[b];

    andares = andar1-andar2;
    if (andares<0){
        andares=andares*-1;
    }

    percorreu = percorreu + (andares*distancia);
}

printf("%d",percorreu);

return 0;
}
