#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 2000

void criptografa(int n,int e,char codificar[max]);
void descriptografar(int n, int d,int tam,int decodificar[max]);

int main () {
    int opcao;//checagem do menu
    int p, q, e, n, d, w, ew=1, tam;//formula (w=omega), ew = E para o d
    int i, qtd, x;//contador
    char codificar[max];//para codificar (frase)
    int decodificar[max];//para traduzir (numeros)

    scanf("%d",&opcao);
    scanf("%d %d %d",&p, &q, &e);
    if(opcao==1){
        scanf(" %[^\n]s",codificar);
    }else if(opcao==2){
        scanf("%d",&qtd);
        for(i=0;i<qtd;i++){
            scanf("%d",&decodificar[i]);
        }
    }

    n = p*q;
    tam = i-1;
    w = (p-1)*(q-1);

    //parte para calcular d
    ew = e;
    ew = ew%w;
    for (x=1;x<w;x++){
       if ((ew*x)%w == 1)	{
            d = x;
        }
    }
    //fim da parte para calcular d

    switch(opcao){
    case 1: criptografa(n,e,codificar); break;
    case 2: descriptografar(n,d,tam,decodificar);break;
    }

return 0;
}

void criptografa(int n,int e,char codificar[max]){
    int m=0,resul=1;
    int i, j; //contadores

    for(i=0;i<strlen(codificar);i++){
    	m = codificar[i]%n;
    	resul = m;
    	for(j=1;j<e;j++){
    		resul *= m;
    		resul %= n;
		}
		printf("%d ",resul);
		resul=1;
	}
}

void descriptografar(int n, int d,int tam,int decodificar[max]){
	int m=0,resul=1;
	int i, j;//contadores

    for(i=0;i<=tam;i++){
    	m = decodificar[i]%n;
    	resul=decodificar[i];
    	for(j=d-1;j>0;j--){
    		resul*=m;
    		resul%=n;
		}
		printf("%c",resul);
		resul=1;
	}
}


