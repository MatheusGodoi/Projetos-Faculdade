import os

#=====#Estrutura para Arquivo Primario#====================================================#
class arquivoIndice:
	def __init__(self):
		self.numRegistro = -1				#Numero de Registros
		self.listaRegistros = []			#Lista vazia para armazenar os indices

	def addRegistro(self, nApt, nLinha):	#Adcionar um registro na lista
		self.numRegistro += 1
		nApt = int(nApt)
		if self.chaveExiste(nApt):				#Checagem para ver se repete
			self.numRegistro -= 1
			print("\n\nAPARTAMENTO EXISTENTE\n\n")
			exit(1)

		parAux = (nApt, nLinha)
		self.listaRegistros.append(parAux)

	def chaveExiste(self, nApt):			#Checar se o apartamento ja existe
		if self.numRegistro == 0:				#Se a lista vazia ele não existe
			return 0

		for i in range(self.numRegistro):		#Varre a lista procurando o registro
			if nApt == self.listaRegistros[i][0]:
				return 1
		return 0

	def printLista(self):					#Printar a lista para debug
		for i in range(self.numRegistro):
			print(self.listaRegistros[i])

#==========================================================================================#
#=====#Funções Necessárias#================================================================#
def criarArquivos(fTabela, fDados, arqPrimario):		#Função para criar Arquivos necessários
	linhaTabela = fTabela.readline()
	linhaTabela = linhaTabela[19:len(linhaTabela)]		#Insere o numero de registros na primeira casa do arquivo de dados
	fDados.write(str(linhaTabela))

	aux = int(linhaTabela)

	fTemp = open("temp.txt", "w+")					#Abre um arquivo temporario para escrever linhas nao desejadas

	linhaTabela = fTabela.readline()
	fTemp.write(str(linhaTabela))

	linhaTabela = fTabela.read(71)
	fTemp.write(str(linhaTabela))

	cont = 0	
	for lines in fTabela:							#Até acabar as linhas todas
		linhaTabela = fTabela.read(71)
		if linhaTabela[1] == '-':						#Caso seja a ultima linha da tabela, para
			break		
		#print(linhaTabela) 									#Linha para debug

		cont += 1

		fDados.write(str(linhaTabela[1:70])+"\n")	#Escreve a linha toda do cadastro, excluindo os caracteres inicais e finais ( | )

		arqPrimario.addRegistro(linhaTabela[1:4], cont*69+1)	#Adciona o registro no arqPrimario em ram

	fTemp.close()									#Fecha e deleta oarquivo primario
	os.remove("temp.txt")

def criarPrimario(arqPrimario, fPrimario):			#Criacao do arquivo primario.ndx
	for i in range(arqPrimario.numRegistro):
		auxApt = str(arqPrimario.listaRegistros[i][0])		#auxApt recebe a chave unica

		j = len(auxApt)
		while j < 3:										#Preenche com 0 até ter 3 caracteres
			fPrimario.write("0")
			j += 1

		fPrimario.write(auxApt)								#Escreve o numero do apartamento e um espaço
		fPrimario.write(" ")

		auxRnn = str(arqPrimario.listaRegistros[i][1])		#Recebe o rnn
		k = len(auxRnn)
		while k < 5:										#Preenche com 0 até ter 5 caracteres
			fPrimario.write("0")
			k += 1

		fPrimario.write(auxRnn)								#Escreve o valor do rnn e um enter
		fPrimario.write("\n")
#==========================================================================================#
#=====#Main#===============================================================================#

def main():
	arqPrimario = arquivoIndice()						#Cria a estrutura para montar o arquivo primario no final
	
	try:
		fTabela = open("TabelaInicial.txt", "r")		#Abre a tabela inicial, se não conseguir, execução para
	except:
		print("ERRO AO ABRIR TABELA INICIAL!")
		exit(1)

	try:												#Tenta abrir flag de energia
		fFlagEnergia = open("flag.txt", "r")
	except:												#Caso não consiga, o program segue normal
		pass
	else:
		print("\n\t\t\tATENCAO!\n\tRECUPERANDO BACKUP DOS ARQUIVOS!")	#Caso consiga, recupera os arquivos

	try:												#Tenta abrir oarquivo dados.txt
		fDados = open("dados.txt", "r")
	except:												#Caso ele não exista, começa a criar os arquivos necessários para executar o programa
		print("Criando arquivos necessarios...")
		fDados = open("dados.txt", "w+")
		fPrimario = open("primario.ndx", "w+")

		criarArquivos(fTabela, fDados, arqPrimario)		#Criação de todos arquivos usados
		criarPrimario(arqPrimario, fPrimario)

		print("\n\nPronto!\n\n")

	fFlagEnergia = open("flag.txt", "w+")				#Cria flag para gerar backup em caso de erro
	
	#arqPrimario.printLista()	#Linha para debug

	fTabela.close()						#Fecha todos arquivos e deleta arquivo de flag caso o programa chegue ao final normalmente
	fDados.close()
	fPrimario.close()
	fFlagEnergia.close()
	os.remove("flag.txt")
	os.remove("primario.ndx")
	os.remove("dados.txt")

if __name__ == '__main__':
	main()
