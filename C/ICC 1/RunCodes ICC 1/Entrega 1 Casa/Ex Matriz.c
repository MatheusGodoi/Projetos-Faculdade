#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int checaesparsa(int tam,float mat[][tam]);
int checaidentidade(int tam,float mat[][tam]);
int checasimetrica(int tam,float mat[][tam]);

int main(){
    int i,j,temp,input, resul=0;
    do{
        scanf("%d",&temp);
    }while(temp<2);

    const int tam=temp;
    float matriz[tam][tam];

    for(i=0;i<tam;i++){
        for(j=0;j<tam;j++){
            scanf("%f",&matriz[i][j]);
        }
    }
    scanf("%d",&input);
    if (input == 1){
        resul = checaesparsa(tam,matriz);
    }else if (input == 2){
        resul = checaidentidade(tam,matriz);
    }else if (input ==3){
        resul = checasimetrica(tam,matriz);
    }if (resul){
        printf("SIM");
    }else{
        printf("NAO");
    }
    return 0;
}

int checaesparsa(int tam,float mat[][tam]){
    int i,j,cont=0;
    for(i=0;i<tam;i++){
        for(j=0;j<tam;j++){
            if(mat[i][j]==0){
                    cont++;
                }
        }
    }
    if(cont>pow(tam,2)/2){
        return 1;
    }
return 0;
}


int checasimetrica(int tam,float mat[][tam]){
    int i,j,cont=0;
    for(i=0;i<tam-1;i++){
        for(j=i+1;j<tam;j++){
            if(mat[i][j]!=mat[j][i]){
                    cont++;
                }
        }
    }
    if(cont){
        return 0;
    }else{
        return 1;
    }
}

int checaidentidade(int tam,float mat[][tam]){
    int i,j,cont=0;
    for(i=0;i<tam;i++){
        for(j=0;j<tam;j++){
            if(i == j){
                if(mat[i][j]!=1){
                    cont++;
                }
            }else{
                if(mat[i][j]!=0){
                    cont++;
                }
            }
        }
    }
    if(cont){
        return 0;
    }else{
        return 1;
    }
}
