import sys
import csv 

def main():
	#Abertura do relatorio de quantidade de voo por agencia aerea
	with open("/home/matheus-godoi/Downloads/flask/static/qtdeXagencia.txt") as fp:
		line = fp.readline()
		#Total de agencias [Rainbow, CloudFy, FlyingDrops, Outras]
		totalAgencias = [0, 0, 0, 0]
		i = 0
		#Leitura e atribuição dos valores de voos por empresa
		while line:
			print("Line: " + line)
			totalAgencias[i] = (line.split(","))[1]
			line = fp.readline()
			i = i + 1

	print(totalAgencias)
	rainbow = totalAgencias[0]
	cloudFy = totalAgencias[1]
	flyingDrops = totalAgencias[2]

	#Checagem pela maior empresa de voo e retorno
	if rainbow > cloudFy:
		if rainbow > flyingDrops:
			print("rainbow maior" + rainbow)
			return ["Rainbow", rainbow]

	if cloudFy > flyingDrops:
		print("cloudFy maior" + cloudFy)
		return ["CloudFy", cloudFy]

	print("flyingDrops maior" + flyingDrops)
	return ["FlyingDrops", flyingDrops]

if __name__ == '__main__':
	main()
