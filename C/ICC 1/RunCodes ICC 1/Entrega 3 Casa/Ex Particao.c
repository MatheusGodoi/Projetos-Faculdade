#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define max 100

int particiona(char frase[], int* tam, char mat[max][max]);
void imprime(char mat[max][max], int pala);
void sort(char mat[max][max], int pala);

int main () {
    char frase[max];
    char matriz[max][max];
    int opcao, tam, pala;
    int *p;
    p = &tam;

    scanf("%[^\n]s",frase);
    scanf(" %d",&opcao);

    tam = strlen(frase);
    pala = particiona(frase,p,matriz);

    if(opcao==1){
        imprime(matriz,pala);
    }else if(opcao==2){
        sort(matriz,pala);
        imprime(matriz,pala);
    }

    return 0;
}


int particiona(char frase[], int* tam, char mat[max][max]){
    int i=0, j=0, k=0;//contador para particionar
    int pala=1;
    int w=0;    //contador para contar palavras

    for(;w<*tam;w++){
        if(frase[w]==' '){
            pala++;
        }
    }
//==========particao das palavras=============//

    for(;i<*tam;i++){
            while((frase[k]!=' ')&&(frase[k]!='\0')){
                mat[i][j] = frase[k];
                j++;
                k++;
            }
        mat[i][j]='\0';
        j=0;
        k++;
    }

return pala;
}

void imprime(char mat[max][max],int pala){
    int i=0;

    for(;i<pala;i++){
      printf("%s\n",mat[i]);
    }

return;
}

void sort(char mat[max][max], int pala){
    int i=0,j=0, tam; //contadores para tolower
    int w, k; //contadores para bubble
    char aux[max];

    //tolower em tudo====================
    for(;i<pala;i++){
        tam = strlen(mat[i]);
        for(j=0;j<tam;j++){
            if(mat[i][j]=='\0'){
                break;
            }
            mat[i][j] = tolower(mat[i][j]);
        }
    }//tolower em tudo===============


    for(w=0;w<pala-1;w++){
        for(k=0;k<pala-1;k++){
            if(strcmp(mat[k],mat[k+1])>0){
                strcpy(aux,mat[k]);
                strcpy(mat[k],mat[k+1]);
                strcpy(mat[k+1],aux);
            }
        }
    }
return;
}

