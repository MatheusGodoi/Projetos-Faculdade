#include <stdio.h>
#include <stdlib.h>
#define max 131080

void radixSort(int vet[], int n, int maior){
    int i,j;
    int ordenado[n];
    int digito = 1;

    while((maior/digito)>0){
        int bucket[10];

        for(j=0;j<10;j++){
            bucket[j]=0;
        }

        for(i=0;i<n;i++){
            bucket[(vet[i] / digito) % 10]++;
        }

        for(i=1;i<10;i++){
            bucket[i]+=bucket[i-1];
        }

        for(i=n-1;i>=0;i--){
            ordenado[--bucket[(vet[i] / digito) % 10]] = vet[i];
        }

        for(i=0;i<n;i++){
            vet[i] = ordenado[i];
        }

    digito *= 10;

    }

    for(i=0;i<n;i++){
        vet[i] = ordenado[i];
    }

return;
}


int main(){
    int vet[max];
    int n, i, maior;

	maior = 0;

	scanf("%d",&n);

	for(i=0;i<n;i++){
		scanf("%d", &vet[i]);
        if(vet[i]>maior){
            maior = vet[i];
        }
	}

	radixSort(vet,n,maior);


    for(i=0;i<n;i++){
        printf("%d ",vet[i]);
    }
return 0;
}
