import sys
import csv 

def main(empresa):
	#Limpeza do arquivo para eliminar dados passados
	open("/home/matheus-godoi/Downloads/flask/static/custoXfunc.txt", "w").close()

	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)
	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			listaVoos.append(row)

	#Soma do custo de cada funcionario e escrita em um arquivo
	preco = 0
	f = open("/home/matheus-godoi/Downloads/flask/static/custoXfunc.txt", "a+")

	#Contador para limitar o numero de usuarios
	i = 0
	for user in listaUser:
		for voo in listaVoos:
			if user[0] == voo[1]:
				#Formatacao do custo para obter 2 casas decimais
				preco = preco + float(voo[5])#float("{0:.2f}".format(float(voo[5])))		
		i = i + 1
		if i == 20:
			break

		f.write(user[2]+","+ str(preco) + "\n")
		preco = 0
	f.close()
	print("acabou")


if __name__ == '__main__':
	main(sys.argv[1])
