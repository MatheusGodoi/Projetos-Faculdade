#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#define max 50

typedef struct {
    int id;
    char nome[max];
    char curso[max];
    int idade;
} cadastro;

cadastro *criaCadastro(int* qtd);
void buscaId(int* aux, cadastro* point,int* qtd);
void buscaCr(cadastro* vet,int* qtd,char* curso);
void imprime(cadastro* vet, int* qtd);

int main () {
    int qtd, op, aux;
    cadastro *point;
    char curso[max];

    qtd = 0;  //quantidade de cadastros realizados
    point = criaCadastro(&qtd);

    do{
        scanf(" %d",&op);
        switch(op){
            case 1: scanf(" %d",&aux); buscaId(&aux,point,&qtd); break;
            case 2: scanf(" %[^\n]s",curso); buscaCr(point,&qtd,curso); break;
            case 3: imprime(point,&qtd); break;
            case -1: free(point); return 0;
            default: printf("\n[Erro]\n"); break;
        }

    }while(1);

    free(point);

    return 0;
}

cadastro *criaCadastro(int *qtd){
    cadastro *vet;
    int j=0, opc;
    int z=0;            //z � uma auxiliar para descobrir tamanho do vetor

    vet = NULL;
    z++;

    do{
        vet = (cadastro*) realloc (vet, z*sizeof(cadastro));
        assert (vet!=NULL);

        scanf(" %d",&opc);
          if(opc != -1){
            vet[j].id = opc;
        }else{
            break;
        }

        scanf(" %[^\n]s",vet[j].nome);
        scanf(" %[^\n]s",vet[j].curso);
        scanf(" %d",&vet[j].idade);
        j++;
        z++;

    }while(1);

    z--;
    *qtd = z;

    return vet;
}

void buscaId(int* aux, cadastro* vet, int* qtd){
    int i=0, flag=0;

    for(;i<*qtd;i++){
        if(vet[i].id == *aux){
            flag++;
            printf("Nome: %s\nCurso: %s\nN USP: %d\nIDADE: %d\n\n",vet[i].nome,vet[i].curso,vet[i].id,vet[i].idade);
            break;
        }
    }
    if(!flag){
        printf("Aluno nao cadastrado\n");
    }
}

void buscaCr(cadastro* vet,int* qtd,char* curso){
    int i=0;

    for(i=0;i<*qtd;i++){
        if(!(strcmp(vet[i].curso,curso))){
            printf("Nome: %s\nCurso: %s\nN USP: %d\nIDADE: %d\n\n",vet[i].nome,vet[i].curso,vet[i].id,vet[i].idade);
        }
    }
}

void imprime(cadastro* vet, int* qtd){
    int i=0;

    for(;i<*qtd;i++){
        printf("Nome: %s\nCurso: %s\nN USP: %d\nIDADE: %d\n\n",vet[i].nome,vet[i].curso,vet[i].id,vet[i].idade);
    }
}
