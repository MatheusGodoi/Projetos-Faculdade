package bas;
import java.util.*;
import robocode.*;
import robocode.control.*;
import java.awt.Color;

public class R1D1 extends Robot
{
	//Taxa de crossover
	private static final double CRATE = 100;
	//Taxa de mutação
	private static final double MRATE = 100;
	//Tamanho da população
	private static final int POPSIZE = 9;
	//Soma de comandos + parâmetros em um indivíduo
	private static final int INDSIZE = 8;
	//Salva acertos e dano
	private int hit = 0;
	//Inicializa número aleatório
	Random r = new Random();	
	//Se foi atingido, verdadeiro
	boolean hitByBullet = false;

	public int matrizRobos[][] = new int [POPSIZE][INDSIZE];
    public int vetFit[] = new int [POPSIZE];	
	/*Comandos úteis
	//Coleta energia do robô no momento atual:
	//getEnergy();
	//Move robô para frente X pixels
	ahead(int pixels);
	//Move robô para trás X pixels
	back(int pixels);
	//Vira arma para direita (0 a 360)
	turnGunRight(int graus);
	//Vira arma para esquerda (0 a 360)
	turnGunLeft(int graus);
	//Vira robô para direita (0 a 180)
	turnRight(int graus);
	//Vira robô para esquerda (0 a 180)
	turnLeft(int graus);
	*/
	
	//Aqui começam as funções do algoritmo evolutivo
	
	
	//Inicialize seus indivíduos aqui
    private void initInd(int i){
    	int j=0;
	   	int aux;
    	for(; j < INDSIZE; j++){
    		aux = r.nextInt(6)+1;

    		if(aux < 2){
				matrizRobos[i][j] = aux;
				aux = r.nextInt(300)+101;
				matrizRobos[i][j+1] = aux;
				j++;    			
    		}else{
    			matrizRobos[i][j] = aux;
    			aux = r.nextInt(270)+46;
				matrizRobos[i][j+1] = aux;
    			j++;
    		}
    	}
    }
	
	//Inicialize sua população aqui
	private void initPop(){
    	for(int i = 0; i < POPSIZE; i++){
    		vetFit[i] = 0;
    		initInd(i);
    	}

    }
	

	private void execute(){
		int vezes  = 5;
		for(int i = 0; i < POPSIZE*vezes; i++){
			double energiaInicial = getEnergy();
			/*Colocar a lista de comandos que o robô deverá realizar e coletar dados do fitness
			Ex:
			turnLeft(60);
			ahead(10);
			*/
			for(int j = 0; j < INDSIZE; j++){
				switch(matrizRobos[i/vezes][j]){
					case 1: ahead(matrizRobos[i][++j]); break;
					case 2: back(matrizRobos[i][++j]); break;
					case 3:	turnLeft(matrizRobos[i][++j]); break;
					case 4: turnRight(matrizRobos[i][++j]); break;
					case 5: turnGunLeft(matrizRobos[i][++j]); break;
					case 6: turnGunRight(matrizRobos[i][++j]); break;
					default: break;
				}
			}

			double energiaFinal = getEnergy();
			vetFit[i/vezes] += (int) (energiaInicial - energiaFinal);
	    }

    }
	
	//Ordena população em ordem de melhor fitness (OPCIONAL)
	private void bestFirst(){
	
	}

	//Mata piores indivíduos (OPCIONAL) e substitui por outro
    private void predation(){
    
    }
	
	//Seleção de pais para reprodução
	private void selection(int matrizRobos[][]){
		int maiorFit = 0;
	    int segMaiorFit = 0;
	    int auxFit = vetFit[0];
   	    int cross[] = new int[INDSIZE];
	    for(int j = 0; j<POPSIZE; j++){
	    	if(auxFit < vetFit[j]){
	  		maiorFit = j;	
	  	 	}
		}

		auxFit = vetFit[0];
		for(int j = 0; j<POPSIZE; j++){
			if(j==maiorFit){
			   	j++;
		  	}
			if(auxFit < vetFit[j]){
		   		segMaiorFit = j;
			}
	    }

	    cross =  crossover(matrizRobos[maiorFit],matrizRobos[segMaiorFit]);
		matrizRobos[segMaiorFit] = cross;
	}
	
	//Realiza a troca de genes entre 2 pais
	private int[] crossover(int pai1[],int pai2[]){
		int cross[] = new int [INDSIZE];
		int auxProb;
		for(int i = 0; i < INDSIZE; i++){
			auxProb = r.nextInt(2)+1;
			if(auxProb == 1){
				cross[i] = pai1[i];
				if(cross[i] < 3){
					cross[i+1] = r.nextInt(300)+101;
				}else{
					cross[i+1] = r.nextInt(270)+46;
				}

			}else if(auxProb == 2){
				cross[i] = pai2[i];
				if(cross[i] < 3){
					cross[i+1] = r.nextInt(300)+101;
				}else{
					cross[i+1] = r.nextInt(270)+46;
				}
			}
			i++;
		}

		auxProb = r.nextInt(1000)+1;
		if(auxProb%10 == 0){
			mutation(cross);
		}
		//Teste a taxa de crossover
		//Selecione os pais
		//Troque os genes entre os pais
    
		return cross;
    }
	
	//Realiza mutação em um indivíduo
	private void mutation(int filho[]){
		// Teste a taxa de mutação
		// Altere o cromossomo
		for(int i = 0; i < INDSIZE; i++){
			if(filho[i] < 3){
				filho[i] = r.nextInt(2)+1;
    			filho[i+1] = r.nextInt(300)+101;
			}else if(filho[i] > 2){
				filho[i] = r.nextInt(6)+1;
    			filho[i+1] = r.nextInt(270)+46;			
    		}
			i++;
		}
    }
	
	//Aqui acabam as funções do algoritmo evolutivo
	
	
	//Função principal do seu algoritmo evolutivo
		public void run() {
        setColors(null, Color.WHITE, Color.BLUE, null, new Color(150, 0, 150)); 
		//Inicializa população inicial
		initPop();
		//Executa o RoboCode
        execute();
		//Executa o robô até acabar todos os rounds do campeonato
		for(int i = 0; true; i++) {
			//Decida aqui onde verificar fitness, chamar crossover, mutação, torneio, predação, etc :)
			//Executa o Robocode
		    selection(matrizRobos);

		    execute();
		}
	}

	//Aqui são comandos para verificar diferentes ações que acontecem durante o jogo. Escolha como pontuar cada uma :)
	
	/**
	 * onScannedRobot: Alvo avistado. Quais as suas ordens, mestre?
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if(hitByBullet){
		    hitByBullet = false;
	    } else {
		    if (e.getDistance() > 200 || getEnergy() < 15) {
			    fire(1);
		    }else if((e.getDistance() > 50)&&(getEnergy() < 10)){
			    fire(3);
		    }else if(getEnergy() > 20){
			    fire(2);
		    }
        }
	}

	/**
	 * onHitByBullet: Você foi atingido. Do a barrel roll!
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		hitByBullet = true;
		hit -= 5; 
	}
	/**
	 * onBulletHit: Seu tiro acertou outro robô, parabéns :)
	 */
    public void onBulletHit(BulletHitEvent event) {
		hit = hit + 3;
    }
	/**
	 * onBulletHitBullet: Seu tiro acertou outro tiro. Você é um ninja?
	 */
    public void onBulletHitBullet(BulletHitBulletEvent event){
    	hit += 0;
    }
	/**
	 * onBulletMissed: Você errou o tiro... Precisa de óculos?
	 */
    public void onBulletMissed(BulletMissedEvent event){
        hit -= 2;
    }
	/**
	 * onHitRobot: Você bateu em outro robô... Acho que não é assim que você dá dano.
	 */
    public void onHitRobot(HitRobotEvent event){
        hit -= 3;
        back(50);
    }
	/**
	 * onHitWall: Você bateu na parede... Como anda sua carteira de motorista?
	 */
    public void onHitWall(HitWallEvent event){
        hit -= 4;
        back(100);
        ahead(50);
    }  
}
