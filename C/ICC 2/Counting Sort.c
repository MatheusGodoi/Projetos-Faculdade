#include <stdio.h>
#include <stdlib.h>
#define max 131100

void countingSort (int ori[],int orde[],int k,int n){
    int i;
    int count[k+1];

    for(i=0;i<=k;i++){
        count[i] = 0;
    }

	for(i=0;i<n;i++){
        count[ori[i]-1]++;
	}

	for(i=1;i<k;i++){
        count[i] += count[i-1];
	}

    for(i=n-1;i>=0;i--) {
        orde[count[ori[i]-1]-1] = ori[i];
        count[ori[i]-1]--;
    }
return;
}

int main(){
    int vet[max];
    int n,i, maior;
    int orde[max];

    maior = 0;


    scanf("%d",&n);

    for(i=0;i<n;i++){
        scanf("%d",&vet[i]);
        if(vet[i]>maior){
            maior = vet[i];
        }
    }

    countingSort(vet, orde, maior, n);

    for(i=0;i<n;i++){
        printf("%d ",orde[i]);
    }

return 0;
}
