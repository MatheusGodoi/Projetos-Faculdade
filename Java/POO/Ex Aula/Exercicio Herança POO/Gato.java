import Animal

public class Gato extends Animal {
	protected int vidasRestantes;

	public Gato(String nomeAnimal, int numeroPatas, int vidas) {
		super(nomeAnimal, numeroPatas);
		vidasRestantes = vidas;
	}
	public void emitirSom(){
		System.out.println("MIAU");
	}

	public String toString(){
		return ("Nome: "+nomeAnimal+"\nNumero de Patas: "+numeroPatas+"\n"+"Vidas: "+vidasRestantes);
	}
}
