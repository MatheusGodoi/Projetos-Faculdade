#include "FilaDePilhaTrab.h"

/*
Matheus Vinicius Gouvea de Godoi	10295217
Luiz Guilherme Martins Adorno		10392171
*/

// Cria uma fila F
void Cria(Fila *F) {
	F->total = 0;
	Create(&F->P);
}

// Esvazia uma fila F
void Esvazia(Fila *F) {
	Cria(F);
}

// Retorna 1 se a fila F estiver vazia e zero se tiver conte�do
int EstaVazia(Fila *F) {
	return IsEmpty(&F->P);
}

// Retorna 1 se a fila F estiver cheia e zero se tiver conte�do ou vazia
int EstaCheia(Fila *F) {
	return IsFull(&F->P);
}

// Adiciona o elemento X � fila F. Se houver erro retorna 1, se n�o retorna 0
int Entra(Fila *F, elem *X) {
	return Push(&F->P, X);
}

// Remove o  elemento X a fila F. Se houver erro retorna 1, se n�o retorna 0
int Sai(Fila *F, elem *X) {
	int item;
	Fila Faux;
	
	Cria(&Faux);
	
	if(!EstaVazia(F)) {
		while(!IsEmpty(&F->P)) {
			Pop(&F->P, &item);
			Push(&Faux.P, &item);
		}
		
		Pop(&Faux.P, X);
		
		while(!IsEmpty(&Faux.P)) {
			Pop(&Faux.P, &item);
			Push(&F->P, &item);
		}
		
		return 0;
	}
	else
		return 1;
	
}

int main() {
	Fila F;
	int X, erro;
	int cont = 0;
	
	Cria(&F);

	scanf("%d",&X);

	while( X != -1){
		erro = Entra(&F,&X);
		if(erro){
			printf("Fila Cheia\n");
			break;	
		}
		scanf("%d",&X);
		cont++;
	}
	
	for(;cont>0;cont--){
		erro = Sai(&F,&X);
		printf("[%d] ",X);
	}
	
	return 0;
}
