import java.util.Scanner;

class Exercicio2 {
	public static void main(String[] args){
		Scanner scanInput = new Scanner(System.in);
		double aceleracaoBase = scanInput.nextDouble();
		Carro carroI = new Carro(0, aceleracaoBase, 0);
		Moto motoI = new Moto(0, aceleracaoBase, 0);
		Bicicleta bicicletaI = new Bicicleta(0, aceleracaoBase, 0);

		int numeroAcoes;
		numeroAcoes = scanInput.nextInt();
		String acao;
		int tempo;

		for(int i=0;i<numeroAcoes;i++){
			acao = scanInput.next();
			tempo = scanInput.nextInt();
			carroI.calculoDistancia(acao, tempo);
		}
		for(int i=0;i<numeroAcoes;i++){
			acao = scanInput.next();
			tempo = scanInput.nextInt();
			motoI.calculoDistancia(acao, tempo);
		}
		for(int i=0;i<numeroAcoes;i++){
			acao = scanInput.next();
			tempo = scanInput.nextInt();
			bicicletaI.calculoDistancia(acao, tempo);

		}
		System.out.println(carroI.printDistanciaPercorrida() + ' ' + motoI.printDistanciaPercorrida() + ' ' + bicicletaI.printDistanciaPercorrida());
	}
}