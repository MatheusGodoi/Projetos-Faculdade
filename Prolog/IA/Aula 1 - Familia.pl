%mulheres
mulher(falt).
mulher(buda).
mulher(tsai).
mulher(lucia).
mulher(maria).
mulher(ingrid).
mulher(julia).
mulher(glauco).
mulher(ana).

%homens
homem(brinde).
homem(bauru).
homem(aristides).
homem(pedro).
homem(joao).
homem(andre).
homem(tales).
homem(francisco).
homem(paulo).

%pais
pai(aristides,tsai).
pai(pedro,joao).
pai(joao,andre).
pai(andre,glauco).
pai(bauru,brinde).
pai(tales,falt).
pai(brinde,julia).
pai(pedro,francisco).

%maes
mae(buda,tsai).
mae(lucia,joao).
mae(tsai,andre).
mae(julia,glauco).
mae(maria,brinde).
mae(ingrid,falt).
mae(falt,julia).
mae(lucia,francisco).

%PREDICADOS%
%é progenitor se for pai ou mae
progenitor(X,Y)	:-	pai(X,Y).
progenitor(X,Y)	:-	mae(X,Y).

avoH(X,Y)	:-	progenitor(Z,Y),
				progenitor(X,Z),
				homem(X),
				not(Z=X).

avoM(X,Y)	:-	progenitor(Z,Y),
				progenitor(X,Z),
				mulher(X),
				not(Z=X).

/*
%ele está provando 4x, usando os dois irmãos em ordem trocada e pai/mãe com ordem trocada
irmao(X,Y)	:-	progenitor(Z,X),
				progenitor(Z,Y),
				not(X=Y).

*/
%assim repete apenas 2x
irmao(X,Y)	:-	pai(Z,X),
				pai(Z,Y),
				not(X=Y).

tio(X,Y)	:-	progenitor(Z,X),
				irmao(Z,Y),
				not(Y=X).

primo(X,Y)	:-	tio(Z,X),
				progenitor(Z,Y).

%FUNÇOES%
%achar pai
obterFilhos	:-	write('Nome do pai?\n'),
				read(Pai),
				write('Filhos:\n'),
				pai(Pai,X),
				write(X),
				write('\n'),
				fail.
			obterFilhos	:-	write('Não tem mais').


/*
ÁRVORE GENEALÓGICA
buda - aristides	pedro - lucia					maria - bauru	ingrid - tales
	 |					  |								  |					|
	 tsai---------------joao / francisco - ana			brinde	----------falt
	 			|						|						|
	 		andre						paulo				   julia
	 			|												|
	 			|												|
				-------------------------------------------------
										|
	 						  		  glauco	
*/