#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main ()
{
    int p1a,p1b, p2a,p2b, dist;
    int p2ap1a, p2bp1b;
    int quad1, quad2, soma;

    printf ("Insira cordenadas do ponto A: ");
    scanf ("%d %d",&p1a,&p1b);
    printf ("Insira cordenadas do ponto B: ");
    scanf ("%d %d",&p2a,&p2b);

    p2ap1a = p2a - p1a;
    p2bp1b = p2b - p1b;
    quad1 = pow(p2ap1a,2);
    quad2 = pow(p2bp1b,2);
    soma = quad1 + quad2;
    dist = sqrt(soma);

    printf ("Distancia dos pontos: %d\n",dist);

    system("pause");
    return 0;

}
