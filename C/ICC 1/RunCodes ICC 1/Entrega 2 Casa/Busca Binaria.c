#include <stdio.h>
#include <stdlib.h>
#define max 100

void levetor(int vet[max],int tam);
void ordenacao(int vet[max],int tam);
int buscabin(int vet[],int chave,int tam);

int main () {
  int tam, chave, vetor[max], resultado;


  scanf("%d %d",&tam, &chave); //le o tamanho e a chave

  levetor(vetor,tam); //funcao pra ler os elementos do vetor
  ordenacao(vetor,tam);//ordena o vetor
  resultado = buscabin(vetor,chave,tam); //faz a busca

  if(resultado==0){
    printf("Chave inexistente");
  }else{
    printf("%d",resultado);
  }

  return 0;
}

void levetor(int vet[max],int tam){
    int i;
    for(i=0;i<tam;i++){
        scanf("%d",&vet[i]);
    }

}

void ordenacao(int vet[max],int tam){
  int i, j, menor, aux;
  for(i=0;i<(tam-1);i++){
    menor=i;
    for(j=(i+1);j<tam;j++) {
      if(vet[j]<vet[menor]) {
        menor=j;
      }
    }
    if(i!=menor) {
      aux=vet[i];
      vet[i]=vet[menor];
      vet[menor]=aux;
    }
  }
}

int buscabin(int vet[],int chave,int tam){
    int menor=0; //menor espaco
    int maior=tam-1; //maior espa�o
    int meio; //variavel pra ser o meio do vetor

    while(menor<=maior){
        meio=(menor+maior)/2;
        if(chave==vet[meio]){
            return meio;
        }
        if (chave<vet[meio]){
            maior=meio-1;
        }else{
            menor=meio+1;
        }
    }
    return 0;   //nao existe
}
