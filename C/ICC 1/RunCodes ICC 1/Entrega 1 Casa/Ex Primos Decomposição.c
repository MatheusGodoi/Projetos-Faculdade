#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define max 100

int checaprimo(int num);
int maiorprimo(int num);

int main(){
    int i, num, aux, resul[max], cont=0, qntd=0;
    resul[0]=1;

    scanf("%d",&num);
    i = num;

    do{
        aux = maiorprimo(i);
        if (num%aux==0){
            resul[++cont]=aux;
            num/=aux;
        }else{
            i = aux-1;
        }
    }while(num>1);
    i=cont;
    do
    {
        do
        {
            qntd++;
            i--;
        }while(resul[i] == resul[i+1]);
    printf("%d %d\n",resul[i+1],qntd);
    qntd = 0;
    }while(i>0);
    return 0;
}

int checaprimo(int num){
    int i=num-1, aux;
    if(num ==1){
        return 1;
    }else if(num<1){
        return 0;
    }

    do{
      aux = num%i;
      i--;
    }while(aux!=0);

    if (i==0){
        return 1;
    }else{
        return 0;
    }
}

int maiorprimo(int num){
    int i,aux;
    if (num<1){
        return 1;
    }
    do{
        i=num;
        aux = checaprimo(num--);
    }while (aux==0);
    return i;
}
