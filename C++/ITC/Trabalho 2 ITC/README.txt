﻿README
Como realizar testes:
	Para realizar o testes do programa, podem ser feito 3 métodos:
		1- ENTRADA MANUAL
			O programa pode ser utilizado de forma manual, inserindo as estradas como específicadas no trabalho.
		2- COLANDO PELO TERMINAL
			Com o terminal na pasta do executável, basta rodar o programa digitando o nome do mesmo e colando no terminal todas as
			entradas no formato específicado no relatório
				Ex: Trabalho2ITC.exe
					Botão direito no terminal -> colar
		3- ENTRADA PELO TERMINAL
			Com o terminal na pasta do executável, basta digitar o nome do programa seguido de um "< entrada.txt", onde "entrada.txt" deve ser um arquivo contendo todas as entradas do caso de teste com formato específicado no relatório.
				Ex: Trabalho2ITC.exe < entrada.txt

