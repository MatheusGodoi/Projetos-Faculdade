//package PilhaInt;

public class Pilha {
    public int numElem;
    public int maxElem;
    public int elementos[];
    
    public Pilha(){
        this.numElem = 0;
        this.maxElem = 5;
        this.elementos = new int[this.maxElem];
        for(int i = 0; i < this.maxElem; i++){
            elementos[i] = -1;
        }
    }
    
    public void push(int elem){
        try{
            if(this.numElem == this.maxElem){
                //Exception
                throw new FullStackException();
            }else{
                this.elementos[this.numElem] = elem;
                this.numElem++;
            }
        }
        catch(FullStackException f){
            System.out.println("Erro!");
            System.out.println(f.getMessage());
            System.exit(0);
        }
        
    }
    
    public int top(){
        try{
            if(this.numElem == 0){
                //Exception
                throw new EmptyStackException(0);
            }else{
                return this.elementos[this.numElem-1];
            }
        }
        catch(EmptyStackException e){
            System.out.println("Erro!");
            System.out.println(e.getMessage());
        }
        return -1;
    }
    
        public void pop(){
        try{
            if(this.numElem == 0){
                //Exception
                throw new EmptyStackException(1);
            }else{
                this.elementos[this.numElem-1] = -1;
                this.numElem--;
            }
        }
        catch(EmptyStackException e){
            System.out.println("Erro!");
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }
}
