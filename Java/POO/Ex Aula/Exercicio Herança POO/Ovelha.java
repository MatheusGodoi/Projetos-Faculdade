import Animal

public class Ovelha extends Animal {
	protected String corDaLa;

	public Ovelha(String nomeAnimal, int numeroPatas, String cor) {
		super(nomeAnimal, numeroPatas);
		corDaLa = cor;
	}
	public void emitirSom(){
		System.out.println("BEEEEE");
	}

	public String toString(){
		return ("Nome: "+nomeAnimal+"\nNumero de Patas: "+numeroPatas+"\n"+"Cor da La: "+corDaLa);
	}
}