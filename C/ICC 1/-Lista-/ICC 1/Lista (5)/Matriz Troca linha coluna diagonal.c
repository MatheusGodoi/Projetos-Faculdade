#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
int matriz[max][max], i, j, troca, m,n;

printf("Insira Linhas = Colunas: ");
scanf("%d",&m);
n=m;

//valores aleatorios na matriz
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matriz[i][j]=rand()%101;
    }
}

printf("\nMatriz Original:\n");
for(i=0;i<m;i++){// matriz original
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\n");

//linha 3 pela linha 5
for(j=0;j<n;j++){
    troca=matriz[4][j];
    matriz[4][j]=matriz[2][j];
    matriz[2][j]=troca;
}

printf("\nLinha 3 pela linha 5:\n");

for(i=0;i<m;i++){// linha 3 pela coluna 3
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\n");

//linha 3 pela coluna 3
i=0;
for(j=0;j<m;j++){
    troca=matriz[i][2];
    matriz[i][2]=matriz[2][j];
    matriz[2][j]=troca;
    i++;
}
printf("\nLinha 3 pela coluna 3:\n");
for(i=0;i<m;i++){// linha 3 pela coluna 5
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\n");

//diagonal principal pela secundaria
for(i=0;i<n;i++){
    troca=matriz[i][i];
    matriz[i][i]=matriz[i][n-1-i];
    matriz[i][n-1-i]=troca;
}

printf("\nDiagonal Principal pela secundaria:\n");
for(i=0;i<m;i++){// Diagonal Principal pela secundaria
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\n");

printf("\nMatriz Final:\n");
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matriz[i][j]);
    }
    printf("\n");
}
printf("\n");
system ("pause");
return 0;
}
