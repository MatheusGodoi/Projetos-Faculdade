import os

#=====#Classes para ser o Nó e a Arvore B em si#===========================================#
class NoArvoreB:							#Um nó de arvore B
	def __init__(self, folha):				# Folha um booleano para indicar se é nó folha ou não
		self.folha = folha
		self.valores = []					#Vetor com os valores do nó
		self.filhos = []					#Vetor com os ponteiros

	def printaNo(self):						#Print do nó
		if self.folha:
			print("No folha com:")
			print("Valores:" + str(self.valores))
			print("Num de filhos:" + str(len(self.filhos)) + "\n")
		else:
			print("No interno com:")
			print("Valores:" + str(self.valores))
			print("Num de filhos:" + str(len(self.filhos)) + "\n")

class ArvoreB:								#Classe para ser a arvore B
	def __init__(self):						#Valores necessários
		self.ordem = 5
		self.maxFilhos = 5
		self.maxValores = 4
		self.minFilhos = 3
		self.minValores = 2
		self.raiz = NoArvoreB(True)
		self.paginas = 0

	def printaArvore(self):					#Print para debug
		print("\nPrint da arvore:\n")
		for filho in self.raiz.filhos:
			filho.printaNo()
			for filho2 in filho.filhos:
				filho2.printaNo()
		self.raiz.printaNo()

	def inserirArvore(self, valor):
		if self.paginas == 0:
			if len(self.raiz.valores) == self.maxValores:
				print("Raiz cheia!")
				#split#
			else:
				self.raiz.valores.append(valor)
				self.raiz.valores.sort()

		i = 0
		while self.raiz.valores[i] < valor and i < self.maxValores:
			i += 1
			self.inserirArvoreFilho(self.raiz.filhos[i], valor)

	def inserirArvoreFilho(self, no, valor):
		if len(no.valores) == self.maxValores:
			print("Filho Cheio!")
			#split again#
		else:
			no.valores.append(valor)
			no.valores.sort()

#==========================================================================================#
#=====#Funções Necessárias#================================================================#
def procuraArvoreB(valor):						#Busca um valor a partir do arquivo de indice (primario.ndx)
		fPrimario = open("primario.ndx","r")
		for lines in fPrimario:						#Varre todas as linhas procurando o valor
			linhaPrimario = fPrimario.readline()
			aux = int(linhaPrimario[0:3])
			if aux == valor:						#Quando encontra o valor, checa se o rrn existe
				print(int(linhaPrimario[4:len(linhaPrimario)]))
				if linhaPrimario[4:5] != "#":
					return -1						#Se não existe, retorna -1
				return int(linhaPrimario[4:len(linhaPrimario)])	#Caso exista, retorna o rrn

def descompactarArvore(arvoreB, fTabela):
	linhaTabela = fTabela.readline()
	linhaTabela = linhaTabela[19:len(linhaTabela)]		#Insere o numero de registros na primeira casa do arquivo de dados
	fPrimario = open("primario.ndx", "w+")
	fPrimario.write(str(linhaTabela))

	aux = int(linhaTabela)

	fTemp = open("temp.txt", "w+")					#Abre um arquivo temporario para escrever linhas nao desejadas

	linhaTabela = fTabela.readline()
	fTemp.write(str(linhaTabela))

	linhaTabela = fTabela.read(71)
	fTemp.write(str(linhaTabela))

	fTemp.close()									#Fecha e deleta oarquivo primario
	os.remove("temp.txt")

	cont = 0
	for lines in fTabela:							#Até acabar as linhas todas
		linhaTabela = fTabela.read(71)
		if linhaTabela[1] == '-':						#Caso seja a ultima linha da tabela, nao le mais
			break
		#print(linhaTabela) 									#Linha para debug
		fPrimario.write(str(linhaTabela[1:4])+"|"+str(cont)+"\n")	#Escreve o apartamento e a pagina atual (RRN)
		cont += 1
		print("Inserindo: " + str(linhaTabela[1:4]))
		arvoreB.inserirArvore(linhaTabela[1:4])


#==========================================================================================#

def main():	
	try:
		fTabela = open("TabelaInicial.txt", "r")		#Abre a tabela inicial, se não conseguir, execução para
	except:
		print("ERRO AO ABRIR TABELA INICIAL!")
		exit(1)

	try:												#Tenta abrir flag de energia
		fFlagEnergia = open("flag.txt", "r")
	except:												#Caso não consiga, o program segue normal
		pass
	else:
		print("\n\t\t\tATENCAO!\n\tRECUPERANDO BACKUP DOS ARQUIVOS!")	#Caso consiga, recupera os arquivos

	fFlagEnergia = open("flag.txt", "w+")				#Cria flag para gerar backup em caso de erro

	try:
		fArvore = open("arvore00.txt", "r")
	except:
		print("Criando arquivos necessarios...")
		arvB = ArvoreB()
		descompactarArvore(arvB, fTabela)					#Abrir poginas da arvore B e ler todos dados salvando na arvore

	arvB.printaArvore()

	fTabela.close()						#Fecha todos arquivos e deleta arquivo de flag caso o programa chegue ao final normalmente
	fFlagEnergia.close()
	os.remove("flag.txt")

if __name__ == '__main__':
	main()
