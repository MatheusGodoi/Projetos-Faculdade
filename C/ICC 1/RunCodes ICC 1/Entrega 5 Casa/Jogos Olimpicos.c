#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char sigla[3];
    int ouro;
    int prata;
    int bronze;
}pais;

pais* geraLista(int* n);
void bubble(pais* lista, int *n);
void imprime(pais* lista, int* n);

int main () {
    int n;
    pais * lista;

    scanf("%d",&n);

    lista = geraLista(&n);

    bubble(lista,&n);

    imprime(lista,&n);

    return 0;
}

pais* geraLista(int* n){
    int i=0;
    pais* vet;

    vet = NULL;
    vet = (pais*)realloc(vet,(*n)*sizeof(pais));

    for(;i<*n;i++){
        scanf(" %s",vet[i].sigla);
        scanf(" %d",&vet[i].ouro);
        scanf(" %d",&vet[i].prata);
        scanf(" %d",&vet[i].bronze);
    }

    return vet;
}

void bubble(pais* lista, int *n){
    int i=0,j=0;
    pais aux;

    for(j=0;j<*n;j++)
        for(i=0;i<*n-1;i++){
            if(lista[i].bronze<lista[i+1].bronze){
                aux = lista[i];
                lista[i] = lista[i+1];
                lista[i+1] = aux;
            }
        }

    for(j=0;j<*n;j++)
        for(i=0;i<*n-1;i++){
            if(lista[i].prata<lista[i+1].prata){
                aux = lista[i];
                lista[i] = lista[i+1];
                lista[i+1] = aux;
            }
        }

    for(j=0;j<*n;j++)
        for(i=0;i<*n-1;i++){
            if(lista[i].ouro<lista[i+1].ouro){
                aux = lista[i];
                lista[i] = lista[i+1];
                lista[i+1] = aux;
            }
        }

}

void imprime(pais* lista, int* n){
    int i=0;

    for(;i<*n;i++){
        printf("%s %d %d %d\n",lista[i].sigla,lista[i].ouro,lista[i].prata,lista[i].bronze);
    }



}
