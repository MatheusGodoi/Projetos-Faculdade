#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int elem;

typedef struct bloco{
	elem info;
	struct bloco *prox;
}no;

typedef struct{
	no* top;
}pilha;


void criaPilha(pilha *P){

	P->top = NULL;

return;
}

void push(pilha *P , elem *X){
	no *aux;
	
	aux = (no*) malloc(sizeof(no));
	assert(aux!=NULL);
	
	aux->info = *X;
	aux->prox = P->top;
	P->top = aux;	
	
}

int empty(pilha *P){
	if(P->top == NULL){
		return 1;
	}
	return 0;
}

void pop(pilha *P, elem *X){
	
	no* aux;
	
	if(empty(P)){
		return;
	}
	
	*X = P->top->info;
	aux = P->top;
	P->top = P->top->prox;
	free(aux);
}

int main (){

	pilha P;
	int num, divi = 2;
	int i=0;
	elem X;
	
	criaPilha(&P);
	
	printf("Insira numero: ");
	scanf("%d",&num);

	while(1){
		if(num == 0){
			break;
		}
		
		X = num%divi;
		num = num/divi;

		push(&P,&X);
		i++;
		
	}
	
	printf("\nResultado:\n");
	
	for(;i>0;i--){
		pop(&P,&X);
		printf("%d",X);
	}
	

return 0;
}
