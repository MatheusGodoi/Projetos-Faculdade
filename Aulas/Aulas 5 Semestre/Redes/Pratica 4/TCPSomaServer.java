import java.io.*;
import java.net.*;

class TCPSomaServer
{
   public static void main(String argv[]) throws Exception
      {
         String clientSentence;
         String capitalizedSentence;
         ServerSocket welcomeSocket = new ServerSocket(7777);

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();
            BufferedReader inFromClient =
               new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            clientSentence = inFromClient.readLine();
            System.out.println("Received: " + clientSentence);
            String[] numeros = clientSentence.split(" ");
            int numA = Integer.parseInt(numeros[0]);
            int numB = Integer.parseInt(numeros[1]);
            int somaNumeros = numA+numB;
            String somaNumerosStr = String.valueOf(somaNumeros);
            System.out.println(numA);
            System.out.println(numB);
            System.out.println(somaNumeros);
            System.out.println(somaNumerosStr);
            outToClient.writeBytes(somaNumerosStr+"\n");
         }
      }
}
