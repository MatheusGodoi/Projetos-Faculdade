//package ex1rc;

import java.util.Scanner;
        
public class Exercicio1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n1, n2, n3;
        
        n1 = sc.nextInt();
        Integer[] vetorInteiros;
        vetorInteiros = new Integer[n1];
        for(int i = 0; i < n1; i++){
            vetorInteiros[i] = sc.nextInt();
        }
        
        n2 = sc.nextInt();
        Double[] vetorFloat;
        vetorFloat = new Double[n2];
        for(int i = 0; i < n2; i++){
            vetorFloat[i] = sc.nextDouble();
        }
        
        n3 = sc.nextInt();
        String str = sc.nextLine();  //Remover \n da leitura
        String stringAux;
        stringAux = new String();
        stringAux = sc.nextLine();
        
        String[] vetorString = new String[n3];
        vetorString = stringAux.split(" ");

        App<Integer> appInt = new App<>(vetorInteiros);
        App<Double> appDouble = new App<>(vetorFloat);
        App<String> appString = new  App<>(vetorString);
    
        appInt.bubbleSort();
        appDouble.bubbleSort();
        appString.bubbleSort();
        
        appInt.printVector();
        appDouble.printVector();
        appString.printVector();
    }   
}