#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#define max 50

typedef struct {
    int id;
    char fabri[max];
    char modelo[max];
    int ano;
    char cor[max];
    int preco;
} cadastro;

cadastro *criaCadastro(int *qtd);
void busca(int* aux, cadastro* point,int* qtd);
void troca(cadastro* vet,int* qtd);
void imprime(cadastro* vet, int* qtd);

int main () {
    int qtd, op, aux;
    cadastro *point;

    scanf(" %d",&qtd);

    point = criaCadastro(&qtd);

    do{
        scanf(" %d",&op);
        switch(op){
            case 1: scanf(" %d",&aux); busca(&aux,point,&qtd); break;
            case 2: troca(point,&qtd); break;
            case 3: imprime(point,&qtd);printf("\n"); break;
            case 0: free(point);printf("\n"); return 0;
            default: printf("\n[Erro]\n"); break;
        }
        fflush(stdin);
    }while(1);

    free(point);

    return 0;
}

cadastro *criaCadastro(int *qtd){
    cadastro *vet;
    int j=0;

    vet = NULL;
    vet = (cadastro*) realloc (vet, *qtd*sizeof(cadastro));
    assert (vet!=NULL);

    for(j=0;j<*qtd;j++){
        scanf(" %d",&vet[j].id);
        scanf(" %[^\n]s",vet[j].fabri);
        scanf(" %[^\n]s",vet[j].modelo);
        scanf(" %[^\n]s",vet[j].cor);
        scanf(" %d",&vet[j].ano);
        scanf(" %d",&vet[j].preco);
    }

    return vet;
}

void busca(int* aux, cadastro* vet, int* qtd){
    int i=0;

    for(;i<*qtd;i++){
        if(vet[i].id == *aux){
            printf("Fab: %s\nMod: %s\nCor: %s\nAno: %d\nPre: %d\n",vet[i].fabri,vet[i].modelo,vet[i].cor,vet[i].ano,vet[i].preco);
            break;
        }
    }
}

void troca(cadastro* vet,int* qtd){
    int i=0;

    for(i=0;i<*qtd;i++){
        if(!(strcmp(vet[i].fabri,"Chevrolet"))){
            strcpy(vet[i].fabri,"GM");
        }
    }
}

void imprime(cadastro* vet, int* qtd){
    int i=0;

    for(;i<*qtd;i++){
        printf("\nFab: %s\nMod: %s\nCor: %s\nAno: %d\nPre: %d\n",vet[i].fabri,vet[i].modelo,vet[i].cor,vet[i].ano,vet[i].preco);
    }
}
