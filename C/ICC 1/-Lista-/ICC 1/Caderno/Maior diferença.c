#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 10


int main () {
int numeros[max]={0}, i=0, j=0, resultado[max]={0}, maior=0;

printf("Insira 10 numeros inteiros: ");

srand(time(NULL));

for(;i<max;i++){   //entrada padrao
    scanf("%d",&numeros[i]);
}
/*for(;i<max;i++){   //entrada aleatoria
    numeros[i] = rand()%10;
} */

for(j=0;j<=max-1;j++){ //soma
    resultado[j]=numeros[j]-numeros[j+1];
}

maior = resultado[0];
for (j=1;j<max;j++){ //saida
   if (resultado[j]>maior){
    maior=resultado[j];
   }
}

printf("\nMaior diferenca: %d\n",maior);

system ("pause");
return 0;
}
