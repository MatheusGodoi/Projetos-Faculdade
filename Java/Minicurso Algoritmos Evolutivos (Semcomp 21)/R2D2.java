package bas;
import java.util.*;
import robocode.*;
import robocode.control.*;
import java.awt.Color;

public class R2D2 extends Robot
{
	//Taxa de crossover
	private static final double CRATE = 100;
	//Taxa de mutação
	private static final double MRATE = 100;
	//Tamanho da população
	private static final int POPSIZE = 5;
	//Soma de comandos + parâmetros em um indivíduo
	private static final int INDSIZE = 8;
	//Salva acertos e dano
	private int hit = 0;
	//Inicializa número aleatório
	Random r = new Random();	
	//Se foi atingido, verdadeiro
	boolean hitByBullet = false;
	
	/*Comandos úteis
	//Coleta energia do robô no momento atual:
	//this.getEnergy();
	//Move robô para frente X pixels
	ahead(int pixels);
	//Move robô para trás X pixels
	back(int pixels);
	//Vira arma para direita (0 a 360)
	turnGunRight(int graus);
	//Vira arma para esquerda (0 a 360)
	turnGunLeft(int graus);
	//Vira robô para direita (0 a 180)
	turnRight(int graus);
	//Vira robô para esquerda (0 a 180)
	turnLeft(int graus);
	*/
	
	//Aqui começam as funções do algoritmo evolutivo
	
	//Inicialize seus indivíduos aqui
    private void initInd(int i){
    }
	
	//Inicialize sua população aqui
	private void initPop(){
    }
	
	private void execute(){
		for(int i = 0; i < POPSIZE; i++){
			/*Colocar a lista de comandos que o robô deverá realizar e coletar dados do fitness
			Ex:
			ahead(10);
			turnLeft(60);
			*/
	    }
    }
	
	//Ordena população em ordem de melhor fitness (OPCIONAL)
	private void bestFirst(){
	}

	//Mata piores indivíduos (OPCIONAL) e substitui por outro
    private void predation(){
    }
	
	//Seleção de pais para reprodução
	private void selection(int parent1, int parent2){
    }
	
	//Realiza a troca de genes entre 2 pais
	private void crossover(){
		//Teste a taxa de crossover
		//Selecione os pais
		//Troque os genes entre os pais
    }
	
	//Realiza mutação em um indivíduo
	private void mutation(){
		// Teste a taxa de mutação
		// Altere o cromossomo
    }
	
	//Aqui acabam as funções do algoritmo evolutivo
	
	
	//Função principal do seu algoritmo evolutivo
		public void run() {
        setColors(null, Color.RED, Color.GREEN, null, new Color(150, 0, 150));
		
		//Inicializa população inicial
		initPop();
		//Executa o RoboCode
        execute();
		//Executa o robô até acabar todos os rounds do campeonato
		for(int i = 0; true; i++) {
			//Decida aqui onde verificar fitness, chamar crossover, mutação, torneio, predação, etc :)

			//Executa o Robocode
		    execute();
		}
	}

	//Aqui são comandos para verificar diferentes ações que acontecem durante o jogo. Escolha como pontuar cada uma :)
	
	/**
	 * onScannedRobot: Alvo avistado. Quais as suas ordens, mestre?
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if(hitByBullet){
		    hitByBullet = false;
	    } else {
		    if (e.getDistance() > 200 || getEnergy() < 15) {
			    fire(1);
		    } else if (e.getDistance() > 50) {
			    fire(2);
		    } else {
			    fire(3);
		    }
        }
	}

	/**
	 * onHitByBullet: Você foi atingido. Do a barrel roll!
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		hitByBullet = true;
		hit -= 3; 
	}
	/**
	 * onBulletHit: Seu tiro acertou outro robô, parabéns :)
	 */
    public void onBulletHit(BulletHitEvent event) {
		hit = hit + 3;
    }
	/**
	 * onBulletHitBullet: Seu tiro acertou outro tiro. Você é um ninja?
	 */
    public void onBulletHitBullet(BulletHitBulletEvent event){
        hit -= 1;
    }
	/**
	 * onBulletMissed: Você errou o tiro... Precisa de óculos?
	 */
    public void onBulletMissed(BulletMissedEvent event){
        hit -= 2;
    }
	/**
	 * onHitRobot: Você bateu em outro robô... Acho que não é assim que você dá dano.
	 */
    public void onHitRobot(HitRobotEvent event){
        hit -= 3;
    }
	/**
	 * onHitWall: Você bateu na parede... Como anda sua carteira de motorista?
	 */
    public void onHitWall(HitWallEvent event){
        hit -= 3;
    }  
	
}