#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int flag=0;         //flag para saber se � palindromo
void palindromo(char vet[],int *tam,int j);

int main () {
    char * vet;
    char letra;
    int i=0, tam, j=0;


    vet = (char*)malloc(1);
    //assert(vet!=NULL);

   do{
        scanf("%c",&letra);

        if(((letra>=65)&&(letra<=90))||((letra>=97)&&(letra<=122))){
            vet[i] = tolower(letra);
            i++;
            vet = (char*)realloc(vet,1+i);
            //assert(vet!=NULL);
        }else if(letra=='\n'){
            vet[i] = '\0';
            tam = i-1;
            break;
        }
    } while(1);

     palindromo(vet,&tam,j);


    if(flag==((tam+1)/2)){
        printf("SIM");
    }else{
        printf("NAO");
    }

    free(vet);
    vet = NULL;

    return 0;
}

void palindromo(char vet[],int *tam,int j){
    int tamanho = *tam;

    if((tamanho-j == j)||(tamanho-j < j)){
        return;
    }

    if(vet[j] != vet[tamanho-j]){
        return;
    }

    j++;
    flag++;

    palindromo(vet,&tamanho,j);
}
