#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#define max 50

typedef struct {
    char nome[max];
    double altura;
    double peso;
    int id;
    char sexo;
} cadastro;

cadastro *criaCadastro(int *qtd);
void printaCadastro(int *qtd,int *id,cadastro *Pessoas);

int main () {
    int qtd, aux;
    cadastro *point;

    scanf("%d",&qtd);

    point = criaCadastro(&qtd);

    do{
        scanf(" %d",&aux);
        if(aux == -1){
            break;
        }
        printaCadastro(&qtd,&aux,point);
    }while(1);

    free(point);

    return 0;
}

cadastro *criaCadastro(int *qtd){
    cadastro *vet;
    int j=0;

    vet = NULL;
    vet = (cadastro*) realloc (vet, *qtd*sizeof(cadastro));
    assert (vet!=NULL);

    for(j=0;j<*qtd;j++){
        scanf(" %[^\n]s",vet[j].nome);
        scanf(" %lf",&vet[j].altura);
        scanf(" %lf",&vet[j].peso);
        scanf(" %d",&vet[j].id);
        scanf(" %c",&vet[j].sexo);
    }

    return vet;
}

void printaCadastro(int* qtd, int *id,cadastro* Pessoas){
    int i=0;
    double imc, dividir;

    for(;i<*qtd;i++){
        if(Pessoas[i].id == *id){
            dividir =(double)pow(Pessoas[i].altura,2);
            //printf("%lf\n",dividir);
            imc = (double) Pessoas[i].peso/dividir;
            //printf("%lf\n",imc);

            printf(" %c\t%s\t%1.2lf\n", Pessoas[i].sexo, Pessoas[i].nome, imc);
        }
    }
}
