#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 30

int cnt = 0;
void contaletra(char vet[],char *letra,int* tam);

int main () {
    char vet[max], letra;
    int tam;
    char* let;              //ponteiro da letra
    int* ptr;               //ponteiro do tamanho

    scanf("%s %c",vet,&letra);
    tam = strlen(vet);
    tam--;

    let = &letra;
    ptr = &tam;

    contaletra(vet,let,ptr);

    printf("%d",cnt);
    return 0;
}

void contaletra(char vet[],char *letra,int* tam){
    int tamanho = *tam;

    if(tamanho<0){
        return;
    }

    if(vet[tamanho]==*letra){
        cnt++;
    }

    tamanho--;
    contaletra(vet,letra,&tamanho);

    return;
}

