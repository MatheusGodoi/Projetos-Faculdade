#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int cnt=0;

void binario(int* num,int* vet);

int main () {
    int decimal, i=0, negativo=0;          //flag para saber se o numero era negativo
    int * vet, * num;

    scanf("%d",&decimal);
    num = &decimal;

    if(decimal<0){
        negativo = 1;
        decimal *= -1;                      //para fazer complemento depois
    }

    vet = (int*)calloc(8,sizeof(int));
    assert(vet!=NULL);

    binario(num,vet);                       //pega os valores em binario

    if(negativo){                           //faz complemento
        for(i=0;i<8;i++){                   //este for transforma 0 em 1, e 1 em 0
            if(vet[i]){
                vet[i]=0;
            }else{
                vet[i]=1;
            }
        }
        vet[cnt-1]++;
        for(i=7;i>=0;i--){
            if(vet[i]==2){
                vet[i] = 0;
                vet[i-1]++;
            }
        }

    }

    for(i=7;i>=cnt;i--){
        printf("%d",vet[i]);
    }
    for(i=0;i<cnt;i++){
        printf("%d",vet[i]);
    }

    return 0;
}

void binario(int* num,int* vet){
    int resto = *num%2;
    int quoci = *num/2;

    if(quoci==0){
        vet[cnt] = resto;
        cnt++;
        //printf("%d",resto);
    }else{
        binario(&quoci,vet);
        //printf("%d",resto);
        vet[cnt] = resto;
        cnt++;
    }
}



