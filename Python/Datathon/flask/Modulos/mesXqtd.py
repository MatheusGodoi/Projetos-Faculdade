import sys
import csv 
import datetime

def main(empresa):
	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)

	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			if row[0] != 'travelCode':
				listaVoos.append(row)

	#Soma da quantidade de viajens por mes de voo e escrita em um arquivo
	meses = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	for voo in listaVoos:
			data = voo[9]
			mes = (data.split('/')[0])
			meses[int(mes)-1] = meses[int(mes)-1] + 1

	#Limpeza do arquivo para eliminar dados passados
	open('/home/matheus-godoi/Downloads/flask/static/mesXqtd.txt', 'w').close()
	#Escrita dos valores
	f = open("/home/matheus-godoi/Downloads/flask/static/mesXqtd.txt", "a+")
	i = 1
	for mes in meses:
		f.write(str(i) + "," + str(mes) + "\n")
		i = i + 1
	f.close()	


if __name__ == '__main__':
	main(sys.argv[1])