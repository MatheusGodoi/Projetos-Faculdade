#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 100

int main () {
int matrizA[max][max],matrizB[max][max], soma[max][max], sub[max][max], trans[max][max];
int i, j, m, n; //contadores i,j   linha (m) x colunas (n)


printf("Insira Linhas = Colunas: ");
scanf("%d",&m);
n=m;

//valores aleatorios na matriz A
srand(time(NULL));

for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matrizA[i][j]=rand()%101+100;
    }
}//fim da atribuišao
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        matrizB[i][j]=rand()%101+100;
    }
}//fim da atribuišao

//soma sub transposta
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        soma[i][j]=matrizA[i][j] + matrizB[i][j];
        sub[i][j]= matrizA[i][j] - matrizB[i][j];
        trans[j][i]=matrizA[i][j];
    }
}//fim da soma/sub/transposta


printf("\nMatriz A:\n");//printa matrizA
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matrizA[i][j]);
    }
    printf("\n");
}
printf("\nMatriz B:\n");//printa matrizB
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,matrizB[i][j]);
    }
    printf("\n");
}

printf("\nSoma:\n");//printa soma
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,soma[i][j]);
    }
    printf("\n");
}

printf("\nSubtrašao:\n");//printa subt
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,sub[i][j]);
    }
    printf("\n");
}

printf("\nMatriz A Transposta:\n");//printa transposta
for(i=0;i<m;i++){
    for(j=0;j<n;j++){
        printf("[%d][%d]= %d ",i+1,j+1,trans[i][j]);
    }
    printf("\n");
}
printf("\n");
system ("pause");
return 0;
}
