#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define max 100


int main () {
int numeros[max]={0}, i=0, j=0, maior=0, menor=0, soma=0, media=0, variancia=0, desvio=0;
int varisoma=0, pot2=0;
float pot;

srand(time(NULL));

for(;i<max;i++){   //entrada aleatoria
    numeros[i] = rand()%101;
}

printf("\n");

for(j=0;j<max;j++){ //soma
    soma=numeros[j]+soma;
}

media = soma/100; // media

maior = numeros[0];
for(j=1;j<max;j++){ //soma
    if (numeros[j]<maior){
        maior=numeros[j];
    }
}
maior = numeros[0];
for(j=1;j<max;j++){ //maior
    if (numeros[j]>maior){
        maior=numeros[j];
    }
}

menor = numeros[0];
for(j=1;j<max;j++){ //menor
    if (numeros[j]<menor){
        menor=numeros[j];
    }
}

//variancia============================================================

for(j=0;j<max;j++){
    pot2=numeros[j]-media;
    pot=pow(pot2,2);
    varisoma=pot+varisoma;
}
variancia = varisoma/max-1;
//desvio============================

desvio=sqrt(varisoma);

//fim==============

printf("Soma: %d\nMaior termo: %d\nMenor Termo: %d\nMedia: %d\nVariancia: %d\nDesvio padrao: %d\n",soma,maior,menor,media,variancia,desvio);
printf("\n");
system ("pause");
return 0;
}
