#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int **gera(int m, int n);
void print(int m, int n, int **matriz);

void fazborda(int m, int n, int borda, int **matriz, int **padding);

int main(){
    int m, n, borda, borda2;
    int i=0, j;
    int **matriz, **padding;

    scanf("%d %d", &n, &m);

    matriz = gera(m, n);//calloc matriz vazia

    for(;i<m;i++){ //scan na matriz pequena
        for(j=0;j<n;j++){
            scanf("%d", &matriz[i][j]);
        }
    }

    scanf("%d", &borda);

    borda2 = borda*2; //para prinar / gerar mais facil

    //faz a borda da matriz e poe dentro a original
    padding=gera(m+borda2, n+borda2);
    fazborda(m, n, borda, matriz, padding);

    //imprime matriz com borda e depois a original
    print(m+borda2, n+borda2, padding);
    printf("\n");
    print(m, n, matriz);

    return 0;
}

int **gera(int m, int n){
    int i=0, **matriz;

    //tratando matriz como vetores
    matriz = (int**)calloc(m, sizeof(int*));
    assert(matriz!=NULL); // cria as linhas

    for(;i<m;i++){
        //cada linha tendo endere�o de um vetor para formar colunas
        matriz[i]=(int*)calloc(n, sizeof(int));//cria as colunas
        assert(matriz!=NULL);
    }

    return matriz;
}

void print(int m, int n, int **matriz){
    int i=0, j;

    for(;i<m;i++){
        for(j=0;j<n;j++){
            printf("%d ",matriz[i][j]);
        }
        printf("\n");
    }
}

void fazborda(int m, int n, int borda, int **matriz, int **padding){
    int i=0, j;

    for(;i<m;i++){
        for(j=0;j<n;j++){
                //colocar a mtriz original dentro da borda
            padding[i+borda][j+borda] = matriz[i][j];
        }
    }
}
