package bas;
import java.util.*;
import java.lang.*;
import robocode.*;
import robocode.control.*;
import java.awt.Color;

public class Brinde extends Robot
{
	//Taxa de crossover
	private static final double CRATE = 100;
	//Taxa de mutação
	private static final double MRATE = 100;
	//Tamanho da população
	private static final int POPSIZE = 10;
	//Soma de comandos + parâmetros em um indivíduo
	private static final int INDSIZE = 30;
	//Salva acertos e dano
	private int hit = 0;
	//Inicializa número aleatório
	Random r = new Random();	
	//Se foi atingido, verdadeiro
	boolean hitByBullet = false;

	public ArrayList<ArrayList<Integer>> matrizRobos = new ArrayList<ArrayList<Integer>>();

	//public int matrizRobos[][] = new int [POPSIZE][INDSIZE];
    public int vetFit[] = new int [POPSIZE];	
	/*Comandos úteis
	//Coleta energia do robô no momento atual:
	//getEnergy();
	//Move robô para frente X pixels
	ahead(int pixels);
	//Move robô para trás X pixels
	back(int pixels);
	//Vira arma para direita (0 a 360)
	turnGunRight(int graus);
	//Vira arma para esquerda (0 a 360)
	turnGunLeft(int graus);
	//Vira robô para direita (0 a 180)
	turnRight(int graus);
	//Vira robô para esquerda (0 a 180)
	turnLeft(int graus);
	*/
	
	//Aqui começam as funções do algoritmo evolutivo
	
	
	//Inicialize seus indivíduos aqui
    private void initInd(int i){
	   	int aux;
	   	int randAux = r.nextInt(INDSIZE)+8;
	   	if(randAux%2 == 1){
	   		randAux++;
	   	}
    	for(int j=0; j < randAux; j++){
    		aux = r.nextInt(6)+1;

    		if(aux < 2){
    			matrizRobos.get(i).add(aux);
				aux = r.nextInt(300)+101;
				matrizRobos.get(i).add(aux);    			
    		}else{
    			matrizRobos.get(i).add(aux);
    			aux = r.nextInt(270)+46;
				matrizRobos.get(i).add(aux);;
    		}

    		j++;
    	}
    }
	
	//Inicialize sua população aqui
	private void initPop(){
		for ( int i = 0; i < POPSIZE; i++ ) {
			matrizRobos.add(new ArrayList<Integer>());
		    initInd(i);
		    vetFit[i] = 0;
		}
    }
	

	private void execute(){
		for(int i = 0; i < POPSIZE; i++){
			/*Colocar a lista de comandos que o robô deverá realizar e coletar dados do fitness
			Ex:
			turnLeft(60);
			ahead(10);
			*/
			System.out.println((matrizRobos.get(i)).get(2));
			for(int j = 0; j < matrizRobos.get(i).size(); j++){
				int indice = j + 1;
				int auxSwitch = ((matrizRobos.get(i)).get(indice)).intValue();
				switch(auxSwitch){
					case 1: ahead(auxSwitch); break;
					case 2: back(auxSwitch); break;
					case 3:	turnLeft(auxSwitch); break;
					case 4: turnRight(auxSwitch); break;
					case 5: turnGunLeft(auxSwitch); break;
					case 6: turnGunRight(auxSwitch); break;
					default: break;
				}
			}
	    }

    }
	
	//Ordena população em ordem de melhor fitness (OPCIONAL)
	private void bestFirst(){
	
	}

	//Mata piores indivíduos (OPCIONAL) e substitui por outro
    private void predation(){
    
    }
	
	//Seleção de pais para reprodução
	private void selection(ArrayList<ArrayList<Integer>> matrizRobos){
		int maiorFit = 0;
	    int segMaiorFit = 0;
	    int auxFit = vetFit[0];
	    ArrayList<Integer> cross = new ArrayList<Integer>();
	    for(int j = 0; j<POPSIZE; j++){
	    	if(auxFit < vetFit[j]){
	  		maiorFit = j;	
	  	 	}
		}

		auxFit = vetFit[0];
		for(int j = 0; j<POPSIZE; j++){
			if(j==maiorFit){
			   	j++;
		  	}
			if(auxFit < vetFit[j]){
		   		segMaiorFit = j;
			}
	    }

	    cross =  crossover(matrizRobos.get(maiorFit),matrizRobos.get(segMaiorFit));
	    for(int l = 0;l < matrizRobos.get(segMaiorFit).size(); l++){
	    	matrizRobos.get(segMaiorFit).remove(0);
	    }
	    for(int l = 0;l < matrizRobos.get(segMaiorFit).size(); l++){
	    	matrizRobos.get(segMaiorFit).add(cross.get(l));
	    }
	}
	
	//Realiza a troca de genes entre 2 pais
	private ArrayList<Integer> crossover(ArrayList<Integer> pai1,ArrayList<Integer> pai2){
		ArrayList<Integer> cross = new ArrayList<Integer>();
		int auxProb;

		if(pai1.size() > pai2.size()){
			for(int i = 0; i < pai2.size(); i++){
				auxProb = r.nextInt(2)+1;
				if(auxProb == 1){
					cross.add(pai1.get(i));
					if(cross.get(i) < 3){
						cross.add(r.nextInt(300)+101);
					}else{
						cross.add(r.nextInt(270)+46);
					}
				}else{
					cross.add(pai2.get(i));
					if(cross.get(i) < 3){
						cross.add(r.nextInt(300)+101);
					}else{
						cross.add(r.nextInt(270)+46);
					}
				}
				for(int j = pai2.size(); j<pai1.size(); j++){
					cross.add(pai1.get(j));
				}
			}
		}else{
			for(int i = 0; i < pai1.size(); i++){
				auxProb = r.nextInt(2)+1;
				if(auxProb == 1){
					cross.add(pai1.get(i));
					if(cross.get(i) < 3){
						cross.add(r.nextInt(300)+101);
					}else{
						cross.add(r.nextInt(270)+46);		
					}
				}else{
					cross.add(pai2.get(i));
					if(cross.get(i) < 3){
						cross.add(r.nextInt(300)+101);
					}else{
						cross.add(r.nextInt(270)+46);
					}
				}
			for(int j = pai1.size(); j<pai2.size(); j++){
				cross.add(pai2.get(j));
			}				
		}	

		auxProb = r.nextInt(1000)+1;
		if(auxProb%10 == 0){
			mutation(cross);
		}
		//Teste a taxa de crossover
		//Selecione os pais
		//Troque os genes entre os pais
    }

	return cross;
}

	//Realiza mutação em um indivíduo
	private void mutation(ArrayList<Integer> filho){
		// Teste a taxa de mutação
		// Altere o cromossomo
		for(int i = 0; i < filho.size(); i++){
			if(filho.get(i) < 3){
				filho.set(i,r.nextInt(2)+1);
    			filho.set(i+1,r.nextInt(300)+101);
			}else if(filho.get(i) > 2){
				filho.set(i,r.nextInt(6)+1);
    			filho.set(i+1,r.nextInt(270)+46);			
    		}
			i++;
		}
    }
	
	//Aqui acabam as funções do algoritmo evolutivo
	
	
	//Função principal do seu algoritmo evolutivo
		public void run() {
        setColors(null, Color.WHITE, Color.BLUE, null, new Color(150, 0, 150)); 
		//Inicializa população inicial
		initPop();
		//Executa o RoboCode
        execute();
		//Executa o robô até acabar todos os rounds do campeonato
		for(int i = 0; true; i++) {
			//Decida aqui onde verificar fitness, chamar crossover, mutação, torneio, predação, etc :)
			//Executa o Robocode
		    selection(matrizRobos);

		    execute();
		}
	}

	//Aqui são comandos para verificar diferentes ações que acontecem durante o jogo. Escolha como pontuar cada uma :)
	
	/**
	 * onScannedRobot: Alvo avistado. Quais as suas ordens, mestre?
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if(hitByBullet){
		    hitByBullet = false;
	    } else {
		    if (e.getDistance() > 200 || getEnergy() < 15) {
			    fire(1);
		    }else if((e.getDistance() > 50)&&(getEnergy() < 10)){
			    fire(3);
		    }else if(getEnergy() > 20){
			    fire(2);
		    }
        }
	}

	/**
	 * onHitByBullet: Você foi atingido. Do a barrel roll!
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		hitByBullet = true;
		hit -= 5; 
	}
	/**
	 * onBulletHit: Seu tiro acertou outro robô, parabéns :)
	 */
    public void onBulletHit(BulletHitEvent event) {
		hit = hit + 3;
    }
	/**
	 * onBulletHitBullet: Seu tiro acertou outro tiro. Você é um ninja?
	 */
    public void onBulletHitBullet(BulletHitBulletEvent event){
    	hit += 0;
    }
	/**
	 * onBulletMissed: Você errou o tiro... Precisa de óculos?
	 */
    public void onBulletMissed(BulletMissedEvent event){
        hit -= 2;
    }
	/**
	 * onHitRobot: Você bateu em outro robô... Acho que não é assim que você dá dano.
	 */
    public void onHitRobot(HitRobotEvent event){
        hit -= 3;
        back(50);
    }
	/**
	 * onHitWall: Você bateu na parede... Como anda sua carteira de motorista?
	 */
    public void onHitWall(HitWallEvent event){
        hit -= 4;
        back(100);
        ahead(50);
    }  
}
