import Vaca
import Ovelha
import Gato

public class Zoo {
	public static void main(String[] args) {
		Animal animal = new Animal("Bicho", 8);
		Vaca vaca = new Vaca("Mimosa",4,"Redondo");
		Gato gato = new Gato("Garfield",4,7);
		Ovelha ovelha = new Ovelha("Dolly",4,"Rosa");
		Animal bichos[] = {animal, vaca, gato, ovelha};

		if(vaca instanceof Animal)
			System.out.println("vaca e Animal");
		if(gato instanceof Animal)
			System.out.println("gato e Animal");
		if(ovelha instanceof Animal)
			System.out.println("ovelha e Animal");

		System.out.println("-----------\n");
		for(int i=0 ; i < bichos.length ; i++) {
			System.out.print(bichos[i].nomeAnimal);

			if(bichos[i] instanceof Vaca)
				System.out.print(" e uma vaca\n");

			if(bichos[i] instanceof Gato)
				System.out.print(" e um gato\n");
			if(bichos[i] instanceof Ovelha){
				System.out.print(" eh uma ovelha");
				System.out.print(", tem " + bichos[i].numeroPatas + " patas e emite o som: ");
				bichos[i].emitirSom();
				System.out.println("\n");
			}
			if(bichos[i] instanceof Animal)
				System.out.println(bichos[i].toString());
			
			System.out.println();
		}
	}
}