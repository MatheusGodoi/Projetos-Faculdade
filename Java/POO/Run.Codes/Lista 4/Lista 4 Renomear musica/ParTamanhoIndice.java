package exercicio2;

import java.util.Comparator;

public class ParTamanhoIndice implements Comparator<ParTamanhoIndice>, Comparable<ParTamanhoIndice>{
    Integer indice;
    Long tamanho;
    
    public ParTamanhoIndice(){
    }
    
    public ParTamanhoIndice(int i, Long t){
        this.indice = i;
        this.tamanho = t;
    }
    
    public void getValue(){
        System.out.println(this.indice + this.tamanho);
    }

    public int getIndice(){
        return this.indice;
    }
    
    public String getIndiceAsString(){
        return this.indice.toString();
    }
    
    public double getTamanho(){
        return this.tamanho;
    }
    
    @Override
    public int compare(ParTamanhoIndice o1, ParTamanhoIndice o2) {
        return (int) (o1.tamanho - o2.tamanho);
    }

    @Override
    public int compareTo(ParTamanhoIndice o) {
        return (this.tamanho).compareTo(o.tamanho);
    }
}
