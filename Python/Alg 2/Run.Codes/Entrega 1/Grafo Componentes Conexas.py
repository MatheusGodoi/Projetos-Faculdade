from collections import defaultdict

class Grafo:
	def __init__(self):
		self.maiorNo = None
		self.nosGrafo = {}

	def addAresta(self, noV, noW):
		if not self.nosGrafo.get(noV):
			self.nosGrafo[noV] = []
		if noW not in self.nosGrafo[noV]:
			self.nosGrafo[noV].append(noW)
		#Caso de Grafo, adcionar refletido
		if not self.nosGrafo.get(noW):
			self.nosGrafo[noW] = []
		if noV not in self.nosGrafo[noW]:
			self.nosGrafo[noW].append(noV)

	def dfs(self, no, visitados):
		indice = ord(no)
		indice -= 65
		visitados.insert(indice, no)
		for noW in self.nosGrafo[no]:
			if noW not in visitados:
				self.dfs(noW,visitados)

	def contarConexos(self):
		visitados = []	
		conexos = 0
		for no in self.nosGrafo:
			if no not in visitados:
				self.dfs(no, visitados)
				conexos += 1
		return conexos

def main():
	numGrafos = int(input())
	aux = input()
	numGrafos = range(numGrafos)
	listaGrafos = [Grafo() for indice in numGrafos]

	resultConexos = []
	for grafoI in listaGrafos:
		grafoI.maiorNo = input()
		noVnoW = input()
		while len(noVnoW) > 0:
			grafoI.addAresta(noVnoW[0], noVnoW[1])
			try:
				noVnoW = input()
			except Exception:
				break
		resultConexos.append(grafoI.contarConexos())
	
	for resultados in resultConexos:
		print(resultados)


if __name__ == '__main__':
	main()