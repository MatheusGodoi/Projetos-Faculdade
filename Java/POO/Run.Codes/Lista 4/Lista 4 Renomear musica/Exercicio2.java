package exercicio2;

import java.util.Scanner;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
        
public class Exercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String path;
        int i;
        
        path = sc.nextLine();

        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().endsWith(".mp3");
            }
        };
        
        File dir = new File(path);
        File[] pasta = dir.listFiles(filter);
        
        List<ParTamanhoIndice> list = new ArrayList<>();
        ParTamanhoIndice aux;
        String nomeMusica;
        i = 0;
        
        System.out.println(path);
        
        for(File arquivos : pasta){
            nomeMusica = arquivos.getName();
            System.out.println(nomeMusica);
            aux = new ParTamanhoIndice(i,arquivos.length());
            list.add(aux);
            i++;
        }
        
        for(int j = 0; j < list.size(); j++){
            for(int k = 0; k < list.size()-1; k++){
                if(list.get(k).compareTo(list.get(k+1)) > 0){
                    aux = list.get(k+1);
                    list.remove(k+1);
                    list.add(k+1,list.get(k));
                    list.remove(k);
                    list.add(k,aux);
                }
            }
        }

        String indiceAuxString;
        int indiceAux;
        char c;
        for(Integer j = 0; j < list.size(); j++){
            String novoNomeMusica = new String();
            indiceAuxString = list.get(j).getIndiceAsString();
            indiceAux = list.get(j).getIndice();
            
            nomeMusica = pasta[indiceAux].getName();
            
            for(i = 4; i > j.toString().length(); i--){
                novoNomeMusica = novoNomeMusica + '0';
            }
            novoNomeMusica = novoNomeMusica + j + '_';

            for(i = 0; i < nomeMusica.length()-4; i++){
                c = nomeMusica.charAt(i);
                if((!Character.isDigit(c))&&(c != '_')&&(c != '-')){
                    novoNomeMusica = novoNomeMusica + c;
                }   
            }
                     
            pasta[indiceAux].renameTo(new File(path + '\\' + novoNomeMusica + ".mp3"));
        }
    }
}
