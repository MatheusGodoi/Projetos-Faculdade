#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void leArquivo(int* cont1,int* cont2,char pal1[],char pal2[]);

int main () {
    char pal1[20], pal2[20];
    int cont1, cont2;

    scanf(" %s",pal1);
    scanf(" %s",pal2);

    leArquivo(&cont1,&cont2,pal1,pal2);

    printf("%s %d %s %d",pal1, cont1, pal2, cont2);

    return 0;
}

void leArquivo(int* cont1,int* cont2,char pal1[],char pal2[]){
    int p1=0, p2=0;
    char palaux[20];

    FILE*arq = fopen("QuePaiseEste.txt","r");
    if (arq == NULL) {
       printf ("Houve um erro ao abrir o arquivo\n");
       exit(0);
    }

    while(1){

        strcpy(palaux,"feof");

        fscanf(arq, " %s", palaux);

        if(!strcmp(palaux,pal1)){
            p1++;
        }else if(!strcmp(palaux,pal2)){
            p2++;
        }

        if(!strcmp(palaux,"feof")){
            break;
        }
    }

    fclose(arq);

    (*cont1) = p1;
    (*cont2) = p2;

    return;
}

