import sys
import csv 

def main(empresa):
	#Leitura do CSV, jogando todos valores em uma lista de usuarios da empresa em questao
	with open('/home/matheus-godoi/Downloads/flask/users.csv') as csvfile:
		userCSV = csv.reader(csvfile, delimiter=',')
		listaUser = []
		for row in userCSV:
			if row[1] == empresa:
				listaUser.append(row)

	#Leitura do CSV de voo, jogando valores em uma lista de voos.
	with open('/home/matheus-godoi/Downloads/flask/flights.csv') as csvfile:
		flightCSV = csv.reader(csvfile, delimiter=',')
		listaVoos = []
		for row in flightCSV:
			if row[0] != 'travelCode':
				listaVoos.append(row)

	#Soma da quantidade de pessoas por agencia de voo e escrita em um arquivo
	agencias = [0, 0, 0]
	for voo in listaVoos:
			if voo[8] == 'Rainbow':
				agencias[0] = agencias[0] + 1
			elif voo[8] == 'CloudFy':
				agencias[1] = agencias[1] + 1
			elif voo[8] == 'FlyingDrops':
				agencias[2] = agencias[2] + 1

	#Limpeza do arquivo para eliminar dados passados
	open('/home/matheus-godoi/Downloads/flask/static/qtdeXagencia.txt', 'w').close()
	#Escrita dos valores
	f = open("/home/matheus-godoi/Downloads/flask/static/qtdeXagencia.txt", "a+")
	f.write("Rainbow," + str(agencias[0]) + "\n")
	f.write("CloudFy," + str(agencias[1]) + "\n")
	f.write("FlyingDrops," + str(agencias[2]) + "\n")
	f.close()	


if __name__ == '__main__':
	main(sys.argv[1])
