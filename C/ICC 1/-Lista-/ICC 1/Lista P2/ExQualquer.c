#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void adicionarnumero(int* seg,int vet[]);
void retirarnumero(int* seg,int vet[]);
void consultarnumero(int* seg,int vet[]);
void trocanumero(int* seg,int* ter,int vet[]);

int cnt = 1; //tamanho do vetor

int main () {

    int qtnd=0;
    int i=0;
    int *vet;
    int pri,seg,ter;

    enum { ADICIONAR = 1, REMOVER, CONSULTAR, ALTERAR };

    vet = (int*)malloc(qtnd*sizeof(int));

    scanf("%d",&qtnd);

    for(;i<qtnd;i++){
        scanf("%d %d",&pri,&seg);
        if(pri==4){
            scanf("%d",&ter);
        }
        //printf("%d %d %d",pri,seg,ter);

        switch(pri){
            case ADICIONAR: cnt++; adicionarnumero(&seg,vet); break;
            case REMOVER: retirarnumero(&seg,vet); break;
            case CONSULTAR: consultarnumero(&seg,vet); printf("\n"); break;
            case ALTERAR: trocanumero(&seg,&ter,vet); break;
        }

    }

    for(i=0;i<cnt-1;i++){
        if(vet[i]=='x'){
            continue;
        }else{
            printf("%d ",vet[i]);
        }
    }

    return 0;
}

void adicionarnumero(int* seg,int vet[]){
    vet[cnt-2] = *seg;
    //printf("%d\n",vet[cnt-2]);
}

void retirarnumero(int* seg,int vet[]){
    int i=0, tam=cnt-1, aux;
    int achou = 0;     //flag para saber se achou o numero

    for(;i<tam-achou;i++){
        if(vet[i]==*seg){
            achou++;

            //aux = vet[i];
            vet[i] = vet[tam-achou];
            vet[tam-achou] = 'x';
        }
    }

    cnt-=achou;

    if(!achou){
        printf("-1\n");
    }
}

void consultarnumero(int* seg,int vet[]){
    int tam=cnt-1, i=0, achou=0; //flag para ver se o numero existe
    int ex = 0;     //para saber quantos numeros excluidos ele passou

    for(;i<tam;i++){
        if(vet[i]==*seg){
            printf("%d ",i-ex);
            achou = 1;
        }
        if(vet[i]=='x'){
            ex++;
        }
    }

    if(!achou){
        printf("-1");
    }
}

void trocanumero(int* seg,int* ter,int vet[]){
    int i=0, tam = cnt-1, achou=0;

    for(;i<tam;i++){
        if(vet[i]==*seg){
            achou = 1;
            vet[i] = *ter;
        }
    }

    if(!achou){
        printf("-1\n");
    }

}
