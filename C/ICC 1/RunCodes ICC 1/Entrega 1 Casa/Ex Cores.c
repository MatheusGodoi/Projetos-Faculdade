#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void lemat(int (*mat)[][3]);
void mistura(int (*mat)[][3],int cord[3]);
void printfinal(int mat[][3]);

int main(){
    int cord[3],paleta[6][3],paleta2[6][3],i=0,j=0;

    for (;i<3;i++){
        scanf("%d",&cord[i]);
    }

    lemat(&paleta);

    for (i=0;i<6;i++){
        for (j=0;j<3;j++){
        paleta2[i][j]=paleta[i][j];
        }
    }
    mistura(&paleta2,cord);
    printf("Start:\n");
    printfinal(paleta);
    printf("\n");
    printf("Result:\n");
    printfinal(paleta2);
    return 0;
}


void lemat(int (*mat)[6][3]){
    int i,j;
    for (i=0;i<6;i++){
        for (j=0;j<3;j++){
            scanf("%d",&mat[0][i][j]);
        }
    }
}

void mistura(int (*mat)[6][3],int cord[3]){
    int i=0;
    for (;i<3;i++){
        mat[0][cord[2]][i]=mat[0][cord[2]][i]+mat[0][cord[0]][i]/2+mat[0][cord[1]][i]/2;
    }
}

void printfinal(int mat[6][3]){
    int i=0;
    for (;i<6;i++){
        printf("Color(%d): [\t%d\t%d\t%d\t]\n", i, mat[i][0],mat[i][1],mat[i][2]);
    }

}
