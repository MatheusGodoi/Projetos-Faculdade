#include <stdio.h>
#include <stdlib.h>

void criaVetor(int** vetor, int tam){
	int i;
	int* vetorTemporario;

	vetorTemporario = (int*)malloc((tam * sizeof(int)));
	if(vetorTemporario == NULL){
		printf("Erro ao alocar vetorTemporario!\n");
		exit(1);
	}

	for(i = 0; i < tam; i++){
		vetorTemporario[i] = i;
	}

	*vetor = vetorTemporario;
}

int main (){
	int *vetor;
	int tam;
	int i;

	vetor = NULL;
	tam = 5;

	criaVetor(&vetor,tam);

	for(i = 0; i < tam; i++){
		printf("%d ",vetor[i]);
	}
	printf("\n");
	return 0;
}