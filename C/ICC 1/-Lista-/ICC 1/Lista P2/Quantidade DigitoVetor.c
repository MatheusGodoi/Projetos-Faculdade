#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 30

int cnt = 0;
void contadigito(char vet[],char *digito,int* tam);

int main () {
    char vet[max], digito;
    int tam;
    char* dig;              //ponteiro do digito
    int* ptr;               //ponteiro do tamanho

    scanf("%s %c",vet,&digito);
    tam = strlen(vet);
    tam--;

    dig = &digito;
    ptr = &tam;

    contadigito(vet,dig,ptr);

    printf("%d",cnt);
    return 0;
}

void contadigito(char vet[],char *digito,int* tam){
    int tamanho = *tam;

    if(tamanho<0){
        return;
    }

    if(vet[tamanho]==*digito){
        cnt++;
    }

    tamanho--;
    contadigito(vet,digito,&tamanho);

    return;
}

