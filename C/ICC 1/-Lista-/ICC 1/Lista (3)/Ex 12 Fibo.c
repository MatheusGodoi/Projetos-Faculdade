#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {

int termo, fibo1=1, fibo2=1, proximo, cont;

	scanf("%d",&termo);

    if (termo<=2){
        printf ("1");
        return 0;
    }

for(cont=3;cont<=termo;cont++){

    proximo = fibo1;
    fibo1 += fibo2;
    fibo2 = proximo;
}

printf ("%d",fibo1);

//system ("pause");
return 0;
}
