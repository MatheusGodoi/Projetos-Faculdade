# Projetos-Faculdade

Compilação de todos os código desenvolvidos durante o curso de Sistemas de Informação; separados por linguagem, matéria e tipo do projeto. ( Listas, run.codes ou trabalhos )

Devido a correção automática do run.codes e listas de exercícios, tais códigos estão faltando comentário e alguns estão com nomes não sugestivos ( como Ex2 ), porém, todas funções e variáveis tem um nome que procura facilitar a compreensão da sua funcionalidade e do que o código procura realizar.

Todas as pastas contem um arquivo "test. " que é o arquivo que uso para programar o exercicio que estou no momento, portanto, muitos estão inacabados sem funcionar.

Dois arquivos existem juntos com as pastas: Compile.doc e Pylint config.doc
Compile.doc é somente uma cola para fazer a execução dos codigos pelo terminal;
Pylint config.doc guarda as modificações que eu fiz no arquivo de configuração do Pylinter para utiliza-lo junto com o sublime text 3 para programar em python;

Na pasta inicial, existem ainda um arquivo com o nome "Git Cheat Sheet.pdf" que se trata apenas de um pdf com comandos para auxiliar no uso do git; e um "Comandos em Arquivos.cpp", um código em c++ com algumas funções de arquivos comentadas com suas funcionalidades para ajudar no uso.
